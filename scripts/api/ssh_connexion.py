######################################################
#
# Default value used for exit()
#
EXIT_SUCCESS = 0
EXIT_FAILURE = 1

######################################################
#
# Import Library
#
try:
    import paramiko
except ImportError as importError:
    print("Error import [install_ansible] paramiko")
    print(importError)
    exit(EXIT_FAILURE)

######################################################
#
# Functions
#
def ssh_connexion(host_username, host_password, host_ip_address, host_port=22) -> paramiko.SSHClient():
        try:
            sshClient = paramiko.SSHClient()
            sshClient.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            sshClient.connect(hostname=host_ip_address, port=host_port,
                              username=host_username, password=host_password)

            return sshClient

        except paramiko.AuthenticationException as e:
            print(
                "[install_ansible - ssh_connexion] - Authentication issue during the SSH connection")
        except paramiko.BadHostKeyException as e:
            print(
                "[install_ansible - ssh_connexion] - Bad Host Key issue during the SSH connection")
        except paramiko.ChannelException as e:
            print(
                "[install_ansible - ssh_connexion] - Channel issue during the SSH connection")
        except paramiko.SSHException as e:
            print(
                "[install_ansible - ssh_connexion] - SSH issue during the SSH connection")
        except TimeoutError as e:
            print(
                "[install_ansible - ssh_connexion] - Timeout during the SSH conenction")
