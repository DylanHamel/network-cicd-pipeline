#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

"""
Description ...

"""

__author__ = "Dylan Hamel"
__maintainer__ = "Dylan Hamel"
__version__ = "1.0"
__email__ = "dylan.hamel@protonmail.com"
__status__ = "Production"
__copyright__ = "Copyright 2019"
__license__ = "MIT"

######################################################
#
# Default value used for exit()
#
EXIT_SUCCESS = 0
EXIT_FAILURE = 1

######################################################
#
# Import Library
#
try:
    import netmiko
except ImportError as importError:
    print("Error import [install_ansible] netmiko")
    print(importError)
    exit(EXIT_FAILURE)

try: 
    import ansible
except ImportError as importError:
    print("Error import [install_ansible] ansible")
    print(importError)
    exit(EXIT_FAILURE)

try: 
    import sys
except ImportError as importError:
    print("Error import [install_ansible] sys")
    print(importError)
    exit(EXIT_FAILURE)

try: 
    import time
except ImportError as importError:
    print("Error import [install_ansible] time")
    print(importError)
    exit(EXIT_FAILURE)

try: 
    import paramiko
    from paramiko import SSHClient
except ImportError as importError:
    print("Error import [install_ansible] paramiko")
    print(importError)
    exit(EXIT_FAILURE)

try: 
    import os
except ImportError as importError:
    print("Error import [install_ansible] os")
    print(importError)
    exit(EXIT_FAILURE)

try: 
    from multiprocessing import Process
except ImportError as importError:
    print("Error import [install_ansible] os")
    print(importError)
    exit(EXIT_FAILURE)

    
######################################################
#
# Constantes
#

######################################################
#
# Variables
#



######################################################
#
# Functions
#
#! /usr/bin/env python

# This script installs the puppet agent on the hosts specified in the command
# line arguments. This script assumes that the 'cumulus' user has passwordless
# sudo enabled on the target devices.

def go(host):
    expect = paramiko.SSHClient()
    expect.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    expect.connect(host, username="cumulus", password="CumulusLinux!")
    
    commands_install_ansible = ["sudo apt-get install -y software-properties-common",
            "sudo add-apt-repository -y ppa:ansible/ansible",
            "sudo apt-get update",
            "sudo apt-get install -y ansible"
            ]

    
    for line in commands:
        stdin, stdout, stderr = expect.exec_command(line, get_pty=True)
        stdout.channel.recv_exit_status()
        print("%s: %s"%(host, line))

    os.system('sudo /opt/puppetlabs/bin/puppet cert sign %s.simulation'%host)
    
    for line in ['sudo /opt/puppetlabs/bin/puppet agent --test',
                 'sudo service puppet start']:
        stdin, stdout, stderr = expect.exec_command(line, get_pty=True)
        stdout.channel.recv_exit_status()
        print("%s: %s"%(host, line))

    expect.close()


if __name__ == "__main__":
    try:
        hostnames = sys.argv[1].split(',')
    except:
        print("Usage: install-puppet-agents.py [leaf01,leaf02,etc]")
        sys.exit(-1)

    processes = []
    for host in hostnames:
        p = Process(target=go, args=(host,))
        p.start()
        processes.append(p)
    for p in processes:
        p.join()
