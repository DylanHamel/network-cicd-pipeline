#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

"""
Description ...

"""

__author__ = "Dylan Hamel"
__maintainer__ = "Dylan Hamel"
__version__ = "1.0"
__email__ = "dylan.hamel@protonmail.com"
__status__ = "Production"
__copyright__ = "Copyright 2019"
__license__ = "MIT"


######################################################
#
# Default value used for exit()
#
EXIT_SUCCESS = 0
EXIT_FAILURE = 1


######################################################
#
# Import Library
#
try:
    import click
except ImportError as importError:
    print("Error import [eveng-api] click")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import requests
except ImportError as importError:
    print("Error import requests")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import json
except ImportError as importError:
    print("Error import json")
    print(importError)
    exit(EXIT_FAILURE)

######################################################
#
# Functions
#
def getGNS3ProjectIDByName(ip, port, project_name):
    """
    This function will return a string that contains project_id according to the project_name given in parameter

    Args:
        param1 (str): GNS3 VM IPv4 address.
        param2 (str): GNS3 VM port.
        param3 (str): GNS3 Project name.

    Returns:
        string: string that contains project_id.
    """

    result = requests.get("http://" + ip + ":" + port + '/v2/projects')
    content = json.loads(result.text)
    exceptionGNS3Get200(result.status_code, content)

    project_id = ""
    for project in content:
        if project["filename"] in project_name:
            project_id = project["project_id"]
            break

    return project_id


def startGNS3NodesByProjectName(ip, port, project_name):
    """
    This function will start all nodes from project_name.

    Args:
        param1 (str): GNS3 VM IPv4 address.
        param2 (str): GNS3 VM port.
        param3 (str): GNS3 Project Name.

    """

    startGNS3NodesByProjectID(
        ip, port, getGNS3ProjectIDByName(ip, port, project_name))


def startGNS3NodesByProjectID(ip, port, project_id):
    """
    This function will start all nodes from project_id.

    Args:
        param1 (str): GNS3 VM IPv4 address.
        param2 (str): GNS3 VM port.
        param3 (str): GNS3 Project ID.

    """

    requests.post(
        f"http://{ip}:{port}/v2/projects/{project_id}/nodes/start", json={})


def stopGNS3NodesByProjectID(ip, port, project_name):
    """
    This function will start all nodes from project_name.

    Args:
        param1 (str): GNS3 VM IPv4 address.
        param2 (str): GNS3 VM port.
        param3 (str): GNS3 Project Name.

    """
    stopGNS3NodesByProjectID(
        ip, port, getGNS3ProjectIDByName(ip, port, project_name))


def stopGNS3NodesByProjectID(ip, port, project_id):
    """
    This function will start all nodes from project_id.

    Args:
        param1 (str): GNS3 VM IPv4 address.
        param2 (str): GNS3 VM port.
        param3 (str): GNS3 Project ID.

    """
    requests.post(
        f"http://{ip}:{port}/v2/projects/{project_id}/nodes/stop", json={})


def exceptionGNS3Get200(status_code, error_msg):
    """
    This function will raise an Exception if request status_code is not 200

    Args:
        param1 (str): HTTP request status_code.
    """
    if status_code is 403:
        raise Exception("Error "+str(status_code) +
                        " in HTTP GET  \n The project is not opened \n " + error_msg["message"])
    elif status_code is 405:
        raise Exception("Error "+str(status_code) +
                        " in HTTP GET  \n The Method Not Allowed \n " + error_msg["message"])
    elif status_code is not 200:
        raise Exception("Error "+str(status_code) +
                        " in HTTP GET \n " + error_msg["message"])
                            
# -----------------------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------------------
@click.command()
@click.option('--start', default="#", help='Start the lab.')
@click.option('--stop', default="#", help='Stop the lab.')
@click.option('--ip', default="127.0.0.1", help='GNS3 VM IPv4 address.')
@click.option('--port', default="3080", help='GNS3 VM tcp port.')
def main(start, stop, ip, port):
    
    if start != "#":
        print(f"Start GNS3 (Production) lab {start}...")
        startGNS3NodesByProjectName(ip, port, start)
        return(EXIT_SUCCESS)

    if stop != "#":
        print(f"Stop GNS3 (Production) lab {stop}...")
        startGNS3NodesByProjectName(ip, port, stop)
        return(EXIT_SUCCESS)


if __name__ == "__main__":
    main()
