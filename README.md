# Network CI/CD Pipeline

Example of a network CI/CD pipeline with Cumulus Network devices.
This pipeline is triggered by a "git push"

A virtal network will be created with EVE-NG.
Network configuration will be applied with Ansible :)
Network will be tested with Nornir (Python) scripts that will be based on "reference files ./tests/topology/*"

### Tools

* Docker
* Jenkins
* Gitlab
* EVE-NG



### Libraries

* Ansible
* Nornir
* Netmiko
* Jinja2
* BatFish (is coming)
* ...


No human intervention is necessary !!!!
After a git push if there is no issue on the network modifications will be automatically applied in producation !!!

READ => **./docs/SETUP.md**

![pipeline.png](./images/pipeline.png)

>
>
>

![schema_principes.png](./images/schema_principes.png)





![cd.png](./images/cd.png)

![cd_tests.png](./images/cd_tests.png)

![tests.png](./images/tests.png)

### Jenkins task output

```bash
Started by GitLab push by Dylan Hamel
Obtained jenkins/Jenkinsfile from git git@172.17.0.3:dhamel/network-config.git
Running in Durability level: MAX_SURVIVABILITY
[Pipeline] Start of Pipeline
[Pipeline] node
Running on Jenkins in /var/jenkins_home/workspace/network-pipeline
[Pipeline] {
[Pipeline] stage
[Pipeline] { (Declarative: Checkout SCM)
[Pipeline] checkout
using credential jenkins_gitlab
 > git rev-parse --is-inside-work-tree # timeout=10
Fetching changes from the remote Git repository
 > git config remote.origin.url git@172.17.0.3:dhamel/network-config.git # timeout=10
Fetching upstream changes from git@172.17.0.3:dhamel/network-config.git
 > git --version # timeout=10
using GIT_SSH to set credentials GitLab_Jenkins
 > git fetch --tags --progress git@172.17.0.3:dhamel/network-config.git +refs/heads/*:refs/remotes/origin/*
skipping resolution of commit remotes/origin/staging, since it originates from another repository
 > git rev-parse refs/remotes/origin/staging^{commit} # timeout=10
 > git rev-parse refs/remotes/origin/origin/staging^{commit} # timeout=10
Checking out Revision 6a8fcf63e501722b6eb83db1c2b200df537bfb3b (refs/remotes/origin/staging)
 > git config core.sparsecheckout # timeout=10
 > git checkout -f 6a8fcf63e501722b6eb83db1c2b200df537bfb3b
Commit message: "Add sleep afer playbook and Add ping error msg"
 > git rev-list --no-walk 72b9724f2291fbc2f3d8cc689f83456872e30e6c # timeout=10
[Pipeline] }
[Pipeline] // stage
[Pipeline] withEnv
[Pipeline] {
[Pipeline] stage
[Pipeline] { ([Step 00] - Prepare_Git_Environnement)
[Pipeline] git
using credential jenkins_gitlab
 > git rev-parse --is-inside-work-tree # timeout=10
Fetching changes from the remote Git repository
 > git config remote.origin.url git@172.17.0.3:dhamel/network-config.git # timeout=10
Fetching upstream changes from git@172.17.0.3:dhamel/network-config.git
 > git --version # timeout=10
using GIT_SSH to set credentials GitLab_Jenkins
 > git fetch --tags --progress git@172.17.0.3:dhamel/network-config.git +refs/heads/*:refs/remotes/origin/*
skipping resolution of commit remotes/origin/staging, since it originates from another repository
 > git rev-parse refs/remotes/origin/master^{commit} # timeout=10
 > git rev-parse refs/remotes/origin/origin/master^{commit} # timeout=10
Checking out Revision eae4e3fee44e145a75e90fe4b4808768c27704cb (refs/remotes/origin/master)
 > git config core.sparsecheckout # timeout=10
 > git checkout -f eae4e3fee44e145a75e90fe4b4808768c27704cb
 > git branch -a -v --no-abbrev # timeout=10
 > git branch -D master # timeout=10
 > git checkout -b master eae4e3fee44e145a75e90fe4b4808768c27704cb
Commit message: "Update"
 > git rev-list --no-walk c6d52ad3f147a604725e38eee7bdf5ca95b3ba68 # timeout=10
[Pipeline] sh
+ rm -rf network-config
[Pipeline] sh
+ git clone http://oauth2:XAfxBX-fasvdDUzCgrM5@172.17.0.3/dhamel/network-config.git
Cloning into 'network-config'...
[Pipeline] }
[Pipeline] // stage
[Pipeline] stage
[Pipeline] { ([Step 01] - Check_If_EVENG_Reachable)
[Pipeline] dir
Running in /var/jenkins_home/workspace/network-pipeline/network-config/scripts/vm
[Pipeline] {
[Pipeline] sh
+ chmod 777 ./test_port.sh
[Pipeline] sh
+ ./test_port.sh
[Pipeline] }
[Pipeline] // dir
Post stage
[Pipeline] echo
[Step 01] - Test connexion to EVE-NG ...
[Pipeline] echo
[Step 01] - Connexion to EVE-NG is ok.
[Pipeline] }
[Pipeline] // stage
[Pipeline] stage
[Pipeline] { ([Step 02] - Deploy_Virutal_Network)
[Pipeline] dir
Running in /var/jenkins_home/workspace/network-pipeline/network-config/scripts
[Pipeline] {
[Pipeline] sh
+ chmod 777 ./eveng-api.py
[Pipeline] sh
+ ./eveng-api.py --deploy=./../topology/topology.yml --force=True
[PyEVENG - login] ...
[PyEVENG - login] (200) logged !
[PyEVENG - deleteLab] - data-center-cumulus.unl is deleting...
[PyEVENG stopLabNode] - data-center-cumulus.unl spine01 is stopping...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/1/stop/stopmode=3
[PyEVENG stopLabNode] - data-center-cumulus.unl spine01 is stopped !
[PyEVENG stopLabNode] - data-center-cumulus.unl spine02 is stopping...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/2/stop/stopmode=3
[PyEVENG stopLabNode] - data-center-cumulus.unl spine02 is stopped !
[PyEVENG stopLabNode] - data-center-cumulus.unl leaf01 is stopping...
[PyEVENG stopLabNode] - data-center-cumulus.unl leaf02 is stopping...
[PyEVENG stopLabNode] - data-center-cumulus.unl leaf03 is stopping...
[PyEVENG stopLabNode] - data-center-cumulus.unl leaf04 is stopping...
[PyEVENG stopLabNode] - data-center-cumulus.unl exit01 is stopping...
[PyEVENG stopLabNode] - data-center-cumulus.unl exit02 is stopping...
[PyEVENG stopLabNode] - data-center-cumulus.unl core01 is stopping...
[PyEVENG stopLabNode] - data-center-cumulus.unl core02 is stopping...
[PyEVENG - remove_remote_connexion_file] - remove root/.eveng/connexion_data-center-cumulus.unl ...
[PyEVENG - remove_remote_connexion_file] - remove root/.eveng/connexion_data-center-cumulus.unl OK !
[PyEVENG - remove_remote_connexion_file] - remove root/.eveng/data-center-cumulus.unl ...
[PyEVENG - remove_remote_connexion_file] - remove root/.eveng/data-center-cumulus.unl OK !
[PyEVENG createLab] - data-center-cumulus.unl has been deleted...
[eveng-api - deploy_all] - labdata-center-cumulus.unl has been removed !
[eveng-api - deploy_all] - deploy projects
[PyEVENG createLab] - data-center-cumulus is creating...
[PyEVENG createLab] - data-center-cumulus has been created...
[eveng-api - deploy_all] - deploy devices
[PyEVENG addNodeToLab] - spine01 is deploying...
('{"type": "qemu", "template": "cumulus", "config": "Unconfigured", "delay": '
 '0, "icon": "router.png", "image": "cumulus-vx-3.7.5", "name": "spine01", '
 '"left": "45%", "top": "20%", "ram": 512, "console": "telnet", "cpu": 1, '
 '"ethernet": 8, "uuid": "641a4800-1b19-427c-ae87-5555590b7790"}')
[PyEVENG addNodeToLab] - spine01 has been deployed!
[PyEVENG addNodeToLab] - spine02 is deploying...
('{"type": "qemu", "template": "cumulus", "config": "Unconfigured", "delay": '
 '0, "icon": "router.png", "image": "cumulus-vx-3.7.5", "name": "spine02", '
 '"left": "55%", "top": "20%", "ram": 1024, "console": "telnet", "cpu": 1, '
 '"ethernet": 8, "uuid": "641a4800-1b19-427c-ae87-6666690b7790"}')
[PyEVENG addNodeToLab] - spine02 has been deployed!
[PyEVENG addNodeToLab] - leaf01 is deploying...
('{"type": "qemu", "template": "cumulus", "config": "Unconfigured", "delay": '
 '0, "icon": "router.png", "image": "cumulus-vx-3.7.5", "name": "leaf01", '
 '"left": "35%", "top": "35%", "ram": 512, "console": "telnet", "cpu": 1, '
 '"ethernet": 8, "uuid": "641a4800-1b19-427c-ae87-1111190b7790"}')
[PyEVENG addNodeToLab] - leaf01 has been deployed!
[PyEVENG addNodeToLab] - leaf02 is deploying...
('{"type": "qemu", "template": "cumulus", "config": "Unconfigured", "delay": '
 '0, "icon": "router.png", "image": "cumulus-vx-3.7.5", "name": "leaf02", '
 '"left": "45%", "top": "35%", "ram": 512, "console": "telnet", "cpu": 1, '
 '"ethernet": 8, "uuid": "641a4800-1b19-427c-ae87-2222290b7790"}')
[PyEVENG addNodeToLab] - leaf02 has been deployed!
[PyEVENG addNodeToLab] - leaf03 is deploying...
('{"type": "qemu", "template": "cumulus", "config": "Unconfigured", "delay": '
 '0, "icon": "router.png", "image": "cumulus-vx-3.7.5", "name": "leaf03", '
 '"left": "55%", "top": "35%", "ram": 512, "console": "telnet", "cpu": 1, '
 '"ethernet": 8, "uuid": "641a4800-1b19-427c-ae87-3333390b7790"}')
[PyEVENG addNodeToLab] - leaf03 has been deployed!
[PyEVENG addNodeToLab] - leaf04 is deploying...
('{"type": "qemu", "template": "cumulus", "config": "Unconfigured", "delay": '
 '0, "icon": "router.png", "image": "cumulus-vx-3.7.5", "name": "leaf04", '
 '"left": "65%", "top": "35%", "ram": 512, "console": "telnet", "cpu": 1, '
 '"ethernet": 8, "uuid": "641a4800-1b19-427c-ae87-4444490b7790"}')
[PyEVENG addNodeToLab] - leaf04 has been deployed!
[PyEVENG addNodeToLab] - exit01 is deploying...
('{"type": "qemu", "template": "cumulus", "config": "Unconfigured", "delay": '
 '0, "icon": "router.png", "image": "cumulus-vx-3.7.5", "name": "exit01", '
 '"left": "65%", "top": "35%", "ram": 512, "console": "telnet", "cpu": 1, '
 '"ethernet": 11, "uuid": "641a4800-1b19-427c-ae87-4444490b7791"}')
[PyEVENG addNodeToLab] - exit01 has been deployed!
[PyEVENG addNodeToLab] - exit02 is deploying...
('{"type": "qemu", "template": "cumulus", "config": "Unconfigured", "delay": '
 '0, "icon": "router.png", "image": "cumulus-vx-3.7.5", "name": "exit02", '
 '"left": "65%", "top": "35%", "ram": 512, "console": "telnet", "cpu": 1, '
 '"ethernet": 11, "uuid": "641a4800-1b19-427c-ae87-4444490b7792"}')
[PyEVENG addNodeToLab] - exit02 has been deployed!
[PyEVENG addNodeToLab] - core01 is deploying...
('{"type": "qemu", "template": "extremexos", "config": "Unconfigured", '
 '"delay": 0, "icon": "Switch L3.png", "image": "extremexos-voss-7.1", "name": '
 '"core01", "left": "45%", "top": "20%", "ram": 2048, "console": "telnet", '
 '"cpu": 1, "ethernet": 8, "uuid": "641a4800-1b19-427c-ae87-5555590b77963"}')
[PyEVENG addNodeToLab] - core01 has been deployed!
[PyEVENG addNodeToLab] - core02 is deploying...
('{"type": "qemu", "template": "extremexos", "config": "Unconfigured", '
 '"delay": 0, "icon": "Switch L3.png", "image": "extremexos-voss-7.1", "name": '
 '"core02", "left": "45%", "top": "20%", "ram": 2048, "console": "telnet", '
 '"cpu": 1, "ethernet": 8, "uuid": "641a4800-1b19-427c-ae87-5555590b77964"}')
[PyEVENG addNodeToLab] - core02 has been deployed!
[PyEVENG addNodesToLab] - all nodes have been deployed!
[eveng-api - deploy_all] - deploy links
[PyEVENG addNetworkToLab] - spine01(swp1)--leaf01(swp1) is deploying...
[PyEVENG addNetworkToLab] - spine01(swp1)--leaf01(swp1) ( 201 ) has been deployed!
[PyEVENG addNetworkToLab] - spine01(swp2)--leaf02(swp1) is deploying...
[PyEVENG addNetworkToLab] - spine01(swp2)--leaf02(swp1) ( 201 ) has been deployed!
[PyEVENG addNetworkToLab] - spine01(swp3)--leaf03(swp1) is deploying...
[PyEVENG addNetworkToLab] - spine01(swp3)--leaf03(swp1) ( 201 ) has been deployed!
[PyEVENG addNetworkToLab] - spine01(swp4)--leaf04(swp1) is deploying...
[PyEVENG addNetworkToLab] - spine01(swp4)--leaf04(swp1) ( 201 ) has been deployed!
[PyEVENG addNetworkToLab] - spine01(swp5)--exit01(swp1) is deploying...
[PyEVENG addNetworkToLab] - spine01(swp5)--exit01(swp1) ( 201 ) has been deployed!
[PyEVENG addNetworkToLab] - spine01(swp6)--exit02(swp1) is deploying...
[PyEVENG addNetworkToLab] - spine01(swp6)--exit02(swp1) ( 201 ) has been deployed!
[PyEVENG addNetworkToLab] - spine02(swp4)--leaf04(swp2) is deploying...
[PyEVENG addNetworkToLab] - spine02(swp4)--leaf04(swp2) ( 201 ) has been deployed!
[PyEVENG addNetworkToLab] - spine02(swp3)--leaf03(swp2) is deploying...
[PyEVENG addNetworkToLab] - spine02(swp3)--leaf03(swp2) ( 201 ) has been deployed!
[PyEVENG addNetworkToLab] - spine02(swp2)--leaf02(swp2) is deploying...
[PyEVENG addNetworkToLab] - spine02(swp2)--leaf02(swp2) ( 201 ) has been deployed!
[PyEVENG addNetworkToLab] - spine02(swp1)--leaf01(swp2) is deploying...
[PyEVENG addNetworkToLab] - spine02(swp1)--leaf01(swp2) ( 201 ) has been deployed!
[PyEVENG addNetworkToLab] - spine02(swp5)--exit01(swp2) is deploying...
[PyEVENG addNetworkToLab] - spine02(swp5)--exit01(swp2) ( 201 ) has been deployed!
[PyEVENG addNetworkToLab] - spine02(swp6)--exit02(swp2) is deploying...
[PyEVENG addNetworkToLab] - spine02(swp6)--exit02(swp2) ( 201 ) has been deployed!
[PyEVENG addNetworkToLab] - leaf01(swp6)--leaf02(swp6) is deploying...
[PyEVENG addNetworkToLab] - leaf01(swp6)--leaf02(swp6) ( 201 ) has been deployed!
[PyEVENG addNetworkToLab] - leaf01(swp7)--leaf02(swp7) is deploying...
[PyEVENG addNetworkToLab] - leaf01(swp7)--leaf02(swp7) ( 201 ) has been deployed!
[PyEVENG addNetworkToLab] - leaf03(swp6)--leaf04(swp6) is deploying...
[PyEVENG addNetworkToLab] - leaf03(swp6)--leaf04(swp6) ( 201 ) has been deployed!
[PyEVENG addNetworkToLab] - leaf03(swp7)--leaf04(swp7) is deploying...
[PyEVENG addNetworkToLab] - leaf03(swp7)--leaf04(swp7) ( 201 ) has been deployed!
[PyEVENG addNetworkToLab] - exit01(swp6)--exit02(swp6) is deploying...
[PyEVENG addNetworkToLab] - exit01(swp6)--exit02(swp6) ( 201 ) has been deployed!
[PyEVENG addNetworkToLab] - exit01(swp7)--exit02(swp7) is deploying...
[PyEVENG addNetworkToLab] - exit01(swp7)--exit02(swp7) ( 201 ) has been deployed!
[PyEVENG addNetworkToLab] - exit01(swp8)--core01(port1) is deploying...
[PyEVENG addNetworkToLab] - exit01(swp8)--core01(port1) ( 201 ) has been deployed!
[PyEVENG addNetworkToLab] - exit01(swp9)--core02(port1) is deploying...
[PyEVENG addNetworkToLab] - exit01(swp9)--core02(port1) ( 201 ) has been deployed!
[PyEVENG addNetworkToLab] - exit02(swp9)--core02(port2) is deploying...
[PyEVENG addNetworkToLab] - exit02(swp9)--core02(port2) ( 201 ) has been deployed!
[PyEVENG addNetworkToLab] - exit02(swp8)--core01(port2) is deploying...
[PyEVENG addNetworkToLab] - exit02(swp8)--core01(port2) ( 201 ) has been deployed!
[PyEVENG addNetworkToLab] - core01(port3)--core02(port3) is deploying...
[PyEVENG addNetworkToLab] - core01(port3)--core02(port3) ( 201 ) has been deployed!
[PyEVENG addNetworkToLab] - core01(port4)--core02(port4) is deploying...
[PyEVENG addNetworkToLab] - core01(port4)--core02(port4) ( 201 ) has been deployed!
[PyEVENG addNetworkToLab] - OOB-NETWORK is deploying...
[PyEVENG addNetworkToLab] - OOB-NETWORK ( 201 ) has been deployed!
[PyEVENG addLinkToLab] - 1 1 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/1/interfaces - data={"1":"1"}
[PyEVENG addLinkToLab] - 3 1 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/3/interfaces - data={"1":"1"}
[PyEVENG addLinkToLab] - 1 2 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/1/interfaces - data={"2":"2"}
[PyEVENG addLinkToLab] - 4 1 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/4/interfaces - data={"1":"2"}
[PyEVENG addLinkToLab] - 1 3 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/1/interfaces - data={"3":"3"}
[PyEVENG addLinkToLab] - 5 1 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/5/interfaces - data={"1":"3"}
[PyEVENG addLinkToLab] - 1 4 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/1/interfaces - data={"4":"4"}
[PyEVENG addLinkToLab] - 6 1 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/6/interfaces - data={"1":"4"}
[PyEVENG addLinkToLab] - 1 5 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/1/interfaces - data={"5":"5"}
[PyEVENG addLinkToLab] - 7 1 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/7/interfaces - data={"1":"5"}
[PyEVENG addLinkToLab] - 1 6 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/1/interfaces - data={"6":"6"}
[PyEVENG addLinkToLab] - 8 1 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/8/interfaces - data={"1":"6"}
[PyEVENG addLinkToLab] - 2 4 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/2/interfaces - data={"4":"7"}
[PyEVENG addLinkToLab] - 6 2 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/6/interfaces - data={"2":"7"}
[PyEVENG addLinkToLab] - 2 3 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/2/interfaces - data={"3":"8"}
[PyEVENG addLinkToLab] - 5 2 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/5/interfaces - data={"2":"8"}
[PyEVENG addLinkToLab] - 2 2 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/2/interfaces - data={"2":"9"}
[PyEVENG addLinkToLab] - 4 2 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/4/interfaces - data={"2":"9"}
[PyEVENG addLinkToLab] - 2 1 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/2/interfaces - data={"1":"10"}
[PyEVENG addLinkToLab] - 3 2 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/3/interfaces - data={"2":"10"}
[PyEVENG addLinkToLab] - 2 5 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/2/interfaces - data={"5":"11"}
[PyEVENG addLinkToLab] - 7 2 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/7/interfaces - data={"2":"11"}
[PyEVENG addLinkToLab] - 2 6 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/2/interfaces - data={"6":"12"}
[PyEVENG addLinkToLab] - 8 2 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/8/interfaces - data={"2":"12"}
[PyEVENG addLinkToLab] - 3 6 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/3/interfaces - data={"6":"13"}
[PyEVENG addLinkToLab] - 4 6 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/4/interfaces - data={"6":"13"}
[PyEVENG addLinkToLab] - 3 7 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/3/interfaces - data={"7":"14"}
[PyEVENG addLinkToLab] - 4 7 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/4/interfaces - data={"7":"14"}
[PyEVENG addLinkToLab] - 5 6 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/5/interfaces - data={"6":"15"}
[PyEVENG addLinkToLab] - 6 6 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/6/interfaces - data={"6":"15"}
[PyEVENG addLinkToLab] - 5 7 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/5/interfaces - data={"7":"16"}
[PyEVENG addLinkToLab] - 6 7 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/6/interfaces - data={"7":"16"}
[PyEVENG addLinkToLab] - 7 6 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/7/interfaces - data={"6":"17"}
[PyEVENG addLinkToLab] - 8 6 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/8/interfaces - data={"6":"17"}
[PyEVENG addLinkToLab] - 7 7 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/7/interfaces - data={"7":"18"}
[PyEVENG addLinkToLab] - 8 7 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/8/interfaces - data={"7":"18"}
[PyEVENG addLinkToLab] - 7 8 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/7/interfaces - data={"8":"19"}
[PyEVENG addLinkToLab] - 9 1 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/9/interfaces - data={"1":"19"}
[PyEVENG addLinkToLab] - 7 9 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/7/interfaces - data={"9":"20"}
[PyEVENG addLinkToLab] - 10 1 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/10/interfaces - data={"1":"20"}
[PyEVENG addLinkToLab] - 8 9 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/8/interfaces - data={"9":"21"}
[PyEVENG addLinkToLab] - 10 2 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/10/interfaces - data={"2":"21"}
[PyEVENG addLinkToLab] - 8 8 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/8/interfaces - data={"8":"22"}
[PyEVENG addLinkToLab] - 9 2 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/9/interfaces - data={"2":"22"}
[PyEVENG addLinkToLab] - 9 3 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/9/interfaces - data={"3":"23"}
[PyEVENG addLinkToLab] - 10 3 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/10/interfaces - data={"3":"23"}
[PyEVENG addLinkToLab] - 9 4 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/9/interfaces - data={"4":"24"}
[PyEVENG addLinkToLab] - 10 4 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/10/interfaces - data={"4":"24"}
[ip - convertNetmaskToCIDR] Receive -  24 |
[PyEVENG - addLinksToLab] - sudo ifconfig pnet5 up && sudo ifconfig pnet5 10.0.5.1 netmask 255.255.255.0
[PyEVENG - create_iptables_nat] - create file data-center-cumulus.unl
[PyEVENG - create_iptables_nat] - iptables -A PREROUTING -t nat -i $(ip route show | grep $(hostname -I | awk '{print $1}') | awk '{print $3}') -p tcp --dport 22101 -j DNAT --to 10.0.5.101:22
[PyEVENG - create_iptables_nat] - iptables -A FORWARD -p tcp -d $(hostname -I | awk '{print $1}') --dport 22101 -j ACCEPT
[PyEVENG - create_iptables_nat] - iptables -A POSTROUTING -t nat -o pnet5 -p tcp --dport 22 -j SNAT --to 10.0.5.1
[PyEVENG addLinkToLab] - 1 0 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/1/interfaces - data={"0":"25"}
[PyEVENG - create_iptables_nat] - create file data-center-cumulus.unl
[PyEVENG - create_iptables_nat] - iptables -A PREROUTING -t nat -i $(ip route show | grep $(hostname -I | awk '{print $1}') | awk '{print $3}') -p tcp --dport 22102 -j DNAT --to 10.0.5.102:22
[PyEVENG - create_iptables_nat] - iptables -A FORWARD -p tcp -d $(hostname -I | awk '{print $1}') --dport 22102 -j ACCEPT
[PyEVENG - create_iptables_nat] - iptables -A POSTROUTING -t nat -o pnet5 -p tcp --dport 22 -j SNAT --to 10.0.5.1
[PyEVENG addLinkToLab] - 2 0 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/2/interfaces - data={"0":"25"}
[PyEVENG - create_iptables_nat] - create file data-center-cumulus.unl
[PyEVENG - create_iptables_nat] - iptables -A PREROUTING -t nat -i $(ip route show | grep $(hostname -I | awk '{print $1}') | awk '{print $3}') -p tcp --dport 22201 -j DNAT --to 10.0.5.201:22
[PyEVENG - create_iptables_nat] - iptables -A FORWARD -p tcp -d $(hostname -I | awk '{print $1}') --dport 22201 -j ACCEPT
[PyEVENG - create_iptables_nat] - iptables -A POSTROUTING -t nat -o pnet5 -p tcp --dport 22 -j SNAT --to 10.0.5.1
[PyEVENG addLinkToLab] - 3 0 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/3/interfaces - data={"0":"25"}
[PyEVENG - create_iptables_nat] - create file data-center-cumulus.unl
[PyEVENG - create_iptables_nat] - iptables -A PREROUTING -t nat -i $(ip route show | grep $(hostname -I | awk '{print $1}') | awk '{print $3}') -p tcp --dport 22202 -j DNAT --to 10.0.5.202:22
[PyEVENG - create_iptables_nat] - iptables -A FORWARD -p tcp -d $(hostname -I | awk '{print $1}') --dport 22202 -j ACCEPT
[PyEVENG - create_iptables_nat] - iptables -A POSTROUTING -t nat -o pnet5 -p tcp --dport 22 -j SNAT --to 10.0.5.1
[PyEVENG addLinkToLab] - 4 0 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/4/interfaces - data={"0":"25"}
[PyEVENG - create_iptables_nat] - create file data-center-cumulus.unl
[PyEVENG - create_iptables_nat] - iptables -A PREROUTING -t nat -i $(ip route show | grep $(hostname -I | awk '{print $1}') | awk '{print $3}') -p tcp --dport 22203 -j DNAT --to 10.0.5.203:22
[PyEVENG - create_iptables_nat] - iptables -A FORWARD -p tcp -d $(hostname -I | awk '{print $1}') --dport 22203 -j ACCEPT
[PyEVENG - create_iptables_nat] - iptables -A POSTROUTING -t nat -o pnet5 -p tcp --dport 22 -j SNAT --to 10.0.5.1
[PyEVENG addLinkToLab] - 5 0 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/5/interfaces - data={"0":"25"}
[PyEVENG - create_iptables_nat] - create file data-center-cumulus.unl
[PyEVENG - create_iptables_nat] - iptables -A PREROUTING -t nat -i $(ip route show | grep $(hostname -I | awk '{print $1}') | awk '{print $3}') -p tcp --dport 22204 -j DNAT --to 10.0.5.204:22
[PyEVENG - create_iptables_nat] - iptables -A FORWARD -p tcp -d $(hostname -I | awk '{print $1}') --dport 22204 -j ACCEPT
[PyEVENG - create_iptables_nat] - iptables -A POSTROUTING -t nat -o pnet5 -p tcp --dport 22 -j SNAT --to 10.0.5.1
[PyEVENG addLinkToLab] - 6 0 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/6/interfaces - data={"0":"25"}
[PyEVENG - create_iptables_nat] - create file data-center-cumulus.unl
[PyEVENG - create_iptables_nat] - iptables -A PREROUTING -t nat -i $(ip route show | grep $(hostname -I | awk '{print $1}') | awk '{print $3}') -p tcp --dport 22151 -j DNAT --to 10.0.5.151:22
[PyEVENG - create_iptables_nat] - iptables -A FORWARD -p tcp -d $(hostname -I | awk '{print $1}') --dport 22151 -j ACCEPT
[PyEVENG - create_iptables_nat] - iptables -A POSTROUTING -t nat -o pnet5 -p tcp --dport 22 -j SNAT --to 10.0.5.1
[PyEVENG addLinkToLab] - 7 0 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/7/interfaces - data={"0":"25"}
[PyEVENG - create_iptables_nat] - create file data-center-cumulus.unl
[PyEVENG - create_iptables_nat] - iptables -A PREROUTING -t nat -i $(ip route show | grep $(hostname -I | awk '{print $1}') | awk '{print $3}') -p tcp --dport 22152 -j DNAT --to 10.0.5.152:22
[PyEVENG - create_iptables_nat] - iptables -A FORWARD -p tcp -d $(hostname -I | awk '{print $1}') --dport 22152 -j ACCEPT
[PyEVENG - create_iptables_nat] - iptables -A POSTROUTING -t nat -o pnet5 -p tcp --dport 22 -j SNAT --to 10.0.5.1
[PyEVENG addLinkToLab] - 8 0 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/8/interfaces - data={"0":"25"}
[PyEVENG - create_iptables_nat] - create file data-center-cumulus.unl
[PyEVENG - create_iptables_nat] - iptables -A PREROUTING -t nat -i $(ip route show | grep $(hostname -I | awk '{print $1}') | awk '{print $3}') -p tcp --dport 22051 -j DNAT --to 10.0.5.51:22
[PyEVENG - create_iptables_nat] - iptables -A FORWARD -p tcp -d $(hostname -I | awk '{print $1}') --dport 22051 -j ACCEPT
[PyEVENG - create_iptables_nat] - iptables -A POSTROUTING -t nat -o pnet5 -p tcp --dport 22 -j SNAT --to 10.0.5.1
[PyEVENG addLinkToLab] - 9 0 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/9/interfaces - data={"0":"25"}
[PyEVENG - create_iptables_nat] - create file data-center-cumulus.unl
[PyEVENG - create_iptables_nat] - iptables -A PREROUTING -t nat -i $(ip route show | grep $(hostname -I | awk '{print $1}') | awk '{print $3}') -p tcp --dport 22052 -j DNAT --to 10.0.5.52:22
[PyEVENG - create_iptables_nat] - iptables -A FORWARD -p tcp -d $(hostname -I | awk '{print $1}') --dport 22052 -j ACCEPT
[PyEVENG - create_iptables_nat] - iptables -A POSTROUTING -t nat -o pnet5 -p tcp --dport 22 -j SNAT --to 10.0.5.1
[PyEVENG addLinkToLab] - 10 0 is deploying...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/10/interfaces - data={"0":"25"}
[PyEVENG - write_in_remote_file] - create new files /root/.eveng/connexion_data-center-cumulus.unl ...
[PyEVENG - write_in_remote_file] - create new files OK ! 
[PyEVENG startLabNode] - data-center-cumulus.unl spine01 is starting...
[PyEVENG startLabNode] - data-center-cumulus.unl spine01 is started !
[PyEVENG startLabNode] - data-center-cumulus.unl spine02 is starting...
[PyEVENG startLabNode] - data-center-cumulus.unl spine02 is started !
[PyEVENG startLabNode] - data-center-cumulus.unl leaf01 is starting...
[PyEVENG startLabNode] - data-center-cumulus.unl leaf01 is started !
[PyEVENG startLabNode] - data-center-cumulus.unl leaf02 is starting...
[PyEVENG startLabNode] - data-center-cumulus.unl leaf02 is started !
[PyEVENG startLabNode] - data-center-cumulus.unl leaf03 is starting...
[PyEVENG startLabNode] - data-center-cumulus.unl leaf03 is started !
[PyEVENG startLabNode] - data-center-cumulus.unl leaf04 is starting...
[PyEVENG startLabNode] - data-center-cumulus.unl leaf04 is started !
[PyEVENG startLabNode] - data-center-cumulus.unl exit01 is starting...
[PyEVENG startLabNode] - data-center-cumulus.unl exit01 is started !
[PyEVENG startLabNode] - data-center-cumulus.unl exit02 is starting...
[PyEVENG startLabNode] - data-center-cumulus.unl exit02 is started !
[PyEVENG startLabNode] - data-center-cumulus.unl core01 is starting...
[PyEVENG startLabNode] - data-center-cumulus.unl core01 is started !
[PyEVENG startLabNode] - data-center-cumulus.unl core02 is starting...
[PyEVENG startLabNode] - data-center-cumulus.unl core02 is started !
[PyEVENG stopLabNode] - data-center-cumulus.unl spine01 is stopping...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/1/stop/stopmode=3
[PyEVENG stopLabNode] - data-center-cumulus.unl spine01 is stopped !
[PyEVENG stopLabNode] - data-center-cumulus.unl spine02 is stopping...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/2/stop/stopmode=3
[PyEVENG stopLabNode] - data-center-cumulus.unl spine02 is stopped !
[PyEVENG stopLabNode] - data-center-cumulus.unl leaf01 is stopping...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/3/stop/stopmode=3
[PyEVENG stopLabNode] - data-center-cumulus.unl leaf01 is stopped !
[PyEVENG stopLabNode] - data-center-cumulus.unl leaf02 is stopping...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/4/stop/stopmode=3
[PyEVENG stopLabNode] - data-center-cumulus.unl leaf02 is stopped !
[PyEVENG stopLabNode] - data-center-cumulus.unl leaf03 is stopping...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/5/stop/stopmode=3
[PyEVENG stopLabNode] - data-center-cumulus.unl leaf03 is stopped !
[PyEVENG stopLabNode] - data-center-cumulus.unl leaf04 is stopping...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/6/stop/stopmode=3
[PyEVENG stopLabNode] - data-center-cumulus.unl leaf04 is stopped !
[PyEVENG stopLabNode] - data-center-cumulus.unl exit01 is stopping...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/7/stop/stopmode=3
[PyEVENG stopLabNode] - data-center-cumulus.unl exit01 is stopped !
[PyEVENG stopLabNode] - data-center-cumulus.unl exit02 is stopping...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/8/stop/stopmode=3
[PyEVENG stopLabNode] - data-center-cumulus.unl exit02 is stopped !
[PyEVENG stopLabNode] - data-center-cumulus.unl core01 is stopping...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/9/stop/stopmode=3
[PyEVENG stopLabNode] - data-center-cumulus.unl core01 is stopped !
[PyEVENG stopLabNode] - data-center-cumulus.unl core02 is stopping...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/10/stop/stopmode=3
[PyEVENG stopLabNode] - data-center-cumulus.unl core02 is stopped !
[eveng-api - deploy_all] - push configs
[EVE-NG shell mount] sudo modprobe nbd max_part=8
[EVE-NG shell mount] sudo qemu-nbd -c /dev/nbd0 /opt/unetlab/tmp/0/dcef936a-8aed-4807-a4b2-d0bc05f3f04d/1/virtioa.qcow2
[EVE-NG shell mount] sudo partx -a /dev/nbd0
[EVE-NG shell mount] sudo mkdir -p /mnt/disk
[EVE-NG shell mount] sudo mount /dev/nbd0p4 /mnt/disk/
[CumulusDevice - pushConfig] copy ./../topology/configs/spine01/interfaces to /mnt/disk/etc/network/interfaces
[EVE-NG shell mount] sudo modprobe nbd max_part=8
[EVE-NG shell mount] sudo qemu-nbd -c /dev/nbd0 /opt/unetlab/tmp/0/dcef936a-8aed-4807-a4b2-d0bc05f3f04d/2/virtioa.qcow2
[EVE-NG shell mount] sudo partx -a /dev/nbd0
[EVE-NG shell mount] sudo mkdir -p /mnt/disk
[EVE-NG shell mount] sudo mount /dev/nbd0p4 /mnt/disk/
[CumulusDevice - pushConfig] copy ./../topology/configs/spine02/interfaces to /mnt/disk/etc/network/interfaces
[EVE-NG shell mount] sudo modprobe nbd max_part=8
[EVE-NG shell mount] sudo qemu-nbd -c /dev/nbd0 /opt/unetlab/tmp/0/dcef936a-8aed-4807-a4b2-d0bc05f3f04d/3/virtioa.qcow2
[EVE-NG shell mount] sudo partx -a /dev/nbd0
[EVE-NG shell mount] sudo mkdir -p /mnt/disk
[EVE-NG shell mount] sudo mount /dev/nbd0p4 /mnt/disk/
[CumulusDevice - pushConfig] copy ./../topology/configs/leaf01/interfaces to /mnt/disk/etc/network/interfaces
[EVE-NG shell mount] sudo modprobe nbd max_part=8
[EVE-NG shell mount] sudo qemu-nbd -c /dev/nbd0 /opt/unetlab/tmp/0/dcef936a-8aed-4807-a4b2-d0bc05f3f04d/4/virtioa.qcow2
[EVE-NG shell mount] sudo partx -a /dev/nbd0
[EVE-NG shell mount] sudo mkdir -p /mnt/disk
[EVE-NG shell mount] sudo mount /dev/nbd0p4 /mnt/disk/
[CumulusDevice - pushConfig] copy ./../topology/configs/leaf02/interfaces to /mnt/disk/etc/network/interfaces
[EVE-NG shell mount] sudo modprobe nbd max_part=8
[EVE-NG shell mount] sudo qemu-nbd -c /dev/nbd0 /opt/unetlab/tmp/0/dcef936a-8aed-4807-a4b2-d0bc05f3f04d/5/virtioa.qcow2
[EVE-NG shell mount] sudo partx -a /dev/nbd0
[EVE-NG shell mount] sudo mkdir -p /mnt/disk
[EVE-NG shell mount] sudo mount /dev/nbd0p4 /mnt/disk/
[CumulusDevice - pushConfig] copy ./../topology/configs/leaf03/interfaces to /mnt/disk/etc/network/interfaces
[EVE-NG shell mount] sudo modprobe nbd max_part=8
[EVE-NG shell mount] sudo qemu-nbd -c /dev/nbd0 /opt/unetlab/tmp/0/dcef936a-8aed-4807-a4b2-d0bc05f3f04d/6/virtioa.qcow2
[EVE-NG shell mount] sudo partx -a /dev/nbd0
[EVE-NG shell mount] sudo mkdir -p /mnt/disk
[EVE-NG shell mount] sudo mount /dev/nbd0p4 /mnt/disk/
[CumulusDevice - pushConfig] copy ./../topology/configs/leaf04/interfaces to /mnt/disk/etc/network/interfaces
[EVE-NG shell mount] sudo modprobe nbd max_part=8
[EVE-NG shell mount] sudo qemu-nbd -c /dev/nbd0 /opt/unetlab/tmp/0/dcef936a-8aed-4807-a4b2-d0bc05f3f04d/7/virtioa.qcow2
[EVE-NG shell mount] sudo partx -a /dev/nbd0
[EVE-NG shell mount] sudo mkdir -p /mnt/disk
[EVE-NG shell mount] sudo mount /dev/nbd0p4 /mnt/disk/
[CumulusDevice - pushConfig] copy ./../topology/configs/exit01/interfaces to /mnt/disk/etc/network/interfaces
[EVE-NG shell mount] sudo modprobe nbd max_part=8
[EVE-NG shell mount] sudo qemu-nbd -c /dev/nbd0 /opt/unetlab/tmp/0/dcef936a-8aed-4807-a4b2-d0bc05f3f04d/8/virtioa.qcow2
[EVE-NG shell mount] sudo partx -a /dev/nbd0
[EVE-NG shell mount] sudo mkdir -p /mnt/disk
[EVE-NG shell mount] sudo mount /dev/nbd0p4 /mnt/disk/
[CumulusDevice - pushConfig] copy ./../topology/configs/exit02/interfaces to /mnt/disk/etc/network/interfaces
[EVE-NG shell mount] sudo modprobe nbd max_part=8
[EVE-NG shell mount] sudo qemu-nbd -c /dev/nbd0 /opt/unetlab/tmp/0/dcef936a-8aed-4807-a4b2-d0bc05f3f04d/9/hda.qcow2
[EVE-NG shell mount] sudo partx -a /dev/nbd0
[EVE-NG shell mount] sudo mkdir -p /mnt/disk
[EVE-NG shell mount] sudo mount /dev/nbd0p2 /mnt/disk/
[ExtremeDevice - pushConfig] copy ./../topology/configs/core01/config.cfg to /mnt/disk/config.cfg
[EVE-NG shell mount] sudo modprobe nbd max_part=8
[EVE-NG shell mount] sudo qemu-nbd -c /dev/nbd0 /opt/unetlab/tmp/0/dcef936a-8aed-4807-a4b2-d0bc05f3f04d/10/hda.qcow2
[EVE-NG shell mount] sudo partx -a /dev/nbd0
[EVE-NG shell mount] sudo mkdir -p /mnt/disk
[EVE-NG shell mount] sudo mount /dev/nbd0p2 /mnt/disk/
[ExtremeDevice - pushConfig] copy ./../topology/configs/core02/config.cfg to /mnt/disk/config.cfg
[PyEVENG startLabNode] - data-center-cumulus.unl spine01 is starting...
[PyEVENG startLabNode] - data-center-cumulus.unl spine01 is started !
[PyEVENG startLabNode] - data-center-cumulus.unl spine02 is starting...
[PyEVENG startLabNode] - data-center-cumulus.unl spine02 is started !
[PyEVENG startLabNode] - data-center-cumulus.unl leaf01 is starting...
[PyEVENG startLabNode] - data-center-cumulus.unl leaf01 is started !
[PyEVENG startLabNode] - data-center-cumulus.unl leaf02 is starting...
[PyEVENG startLabNode] - data-center-cumulus.unl leaf02 is started !
[PyEVENG startLabNode] - data-center-cumulus.unl leaf03 is starting...
[PyEVENG startLabNode] - data-center-cumulus.unl leaf03 is started !
[PyEVENG startLabNode] - data-center-cumulus.unl leaf04 is starting...
[PyEVENG startLabNode] - data-center-cumulus.unl leaf04 is started !
[PyEVENG startLabNode] - data-center-cumulus.unl exit01 is starting...
[PyEVENG startLabNode] - data-center-cumulus.unl exit01 is started !
[PyEVENG startLabNode] - data-center-cumulus.unl exit02 is starting...
[PyEVENG startLabNode] - data-center-cumulus.unl exit02 is started !
[PyEVENG startLabNode] - data-center-cumulus.unl core01 is starting...
[PyEVENG startLabNode] - data-center-cumulus.unl core01 is started !
[PyEVENG startLabNode] - data-center-cumulus.unl core02 is starting...
[PyEVENG startLabNode] - data-center-cumulus.unl core02 is started !
[PyEVENG startLabAllNodes] - no shutdown interfaces ...
[PyEVENG startLabAllNodes] - no shutdown interfaces done !/usr/local/lib/python3.7/site-packages/paramiko/kex_ecdh_nist.py:39: CryptographyDeprecationWarning: encode_point has been deprecated on EllipticCurvePublicNumbers and will be removed in a future version. Please use EllipticCurvePublicKey.public_bytes to obtain both compressed and uncompressed point encoding.
  m.add_string(self.Q_C.public_numbers().encode_point())
/usr/local/lib/python3.7/site-packages/paramiko/kex_ecdh_nist.py:96: CryptographyDeprecationWarning: Support for unsafe construction of public numbers from encoded data will be removed in a future version. Please use EllipticCurvePublicKey.from_encoded_point
  self.curve, Q_S_bytes
/usr/local/lib/python3.7/site-packages/paramiko/kex_ecdh_nist.py:111: CryptographyDeprecationWarning: encode_point has been deprecated on EllipticCurvePublicNumbers and will be removed in a future version. Please use EllipticCurvePublicKey.public_bytes to obtain both compressed and uncompressed point encoding.
  hm.add_string(self.Q_C.public_numbers().encode_point())

[PyEVENG - logout] ...
[PyEVENG - logout] (200) EVE-NG says Byyye :) !
[generate_hosts - generate] Your dynamic hosts file is being created ...
[generate_hosts - generate] Your dynamic hosts file has been created ...


[eveng-api - exit_success] - Did you love this tool ?
Give a STAR https://gitlab.com/DylanHamel/python-eveng-api 


[Pipeline] }
[Pipeline] // dir
Post stage
[Pipeline] echo
[Step 02] - Network deployment is finished ...
[Pipeline] echo
[Step 02] - Network is correctly deployed
[Pipeline] }
[Pipeline] // stage
[Pipeline] stage
[Pipeline] { ([Step 03] - Configure_Virutal_Network)
[Pipeline] dir
Running in /var/jenkins_home/workspace/network-pipeline/network-config
[Pipeline] {
[Pipeline] sleep
Sleeping for 30 sec
[Pipeline] sh
+ cat ./hosts_virtual



[leaf]

leaf01 ansible_host=172.16.194.239 ansible_port=22201 

leaf02 ansible_host=172.16.194.239 ansible_port=22202 

leaf03 ansible_host=172.16.194.239 ansible_port=22203 

leaf04 ansible_host=172.16.194.239 ansible_port=22204 


[spine]

spine01 ansible_host=172.16.194.239 ansible_port=22101 

spine02 ansible_host=172.16.194.239 ansible_port=22102 


[exit]

exit01 ansible_host=172.16.194.239 ansible_port=22151 

exit02 ansible_host=172.16.194.239 ansible_port=22152 


[core]

core01 ansible_host=172.16.194.239 ansible_port=22051 

core02 ansible_host=172.16.194.239 ansible_port=22052 





#
# Static variables
#
[all:vars]
ansible_user=cumulus
ansible_ssh_pass=CumulusLinux!
ansible_sudo_pass=CumulusLinux![Pipeline] ansiblePlaybook
[network-config] $ /usr/bin/ansible-playbook ./deploy_fabric.yml -i ./hosts_virtual

PLAY [exit] ********************************************************************

TASK [Gathering Facts] *********************************************************
Friday 12 July 2019  21:38:40 +0000 (0:00:00.047)       0:00:00.047 *********** 
ok: [exit01]
ok: [exit02]

TASK [deploy_leaf : Create ./backup folder] ************************************
Friday 12 July 2019  21:38:43 +0000 (0:00:02.616)       0:00:02.664 *********** 
ok: [exit01 -> localhost]

TASK [deploy_leaf : Copy FRR daemons file] *************************************
Friday 12 July 2019  21:38:43 +0000 (0:00:00.471)       0:00:03.136 *********** 
changed: [exit01]
changed: [exit02]

TASK [deploy_leaf : L4 hash policy] ********************************************
Friday 12 July 2019  21:38:44 +0000 (0:00:01.158)       0:00:04.295 *********** 
 [WARNING]: The value 1 (type int) in a string field was converted to u'1'
(type string). If this does not look like what you expect, quote the entire
value to ensure it does not change.
changed: [exit01]
changed: [exit02]

TASK [deploy_leaf : Get hostname] **********************************************
Friday 12 July 2019  21:38:45 +0000 (0:00:00.677)       0:00:04.972 *********** 
changed: [exit02]
changed: [exit01]

TASK [deploy_leaf : Print inventory_hostname] **********************************
Friday 12 July 2019  21:38:46 +0000 (0:00:00.597)       0:00:05.569 *********** 
ok: [exit01] => {
    "msg": "cumulus"
}
ok: [exit02] => {
    "msg": "cumulus"
}

TASK [deploy_leaf : Modify hostname] *******************************************
Friday 12 July 2019  21:38:46 +0000 (0:00:00.194)       0:00:05.764 *********** 
changed: [exit01]
changed: [exit02]

TASK [deploy_leaf : Copy interfaces configuration] *****************************
Friday 12 July 2019  21:38:49 +0000 (0:00:03.093)       0:00:08.858 *********** 
changed: [exit01]
changed: [exit02]

TASK [deploy_leaf : Backup interfaces configuration] ***************************
Friday 12 July 2019  21:38:50 +0000 (0:00:01.563)       0:00:10.421 *********** 
changed: [exit01 -> localhost]
changed: [exit02 -> localhost]

TASK [deploy_leaf : Copy FRR configuration] ************************************
Friday 12 July 2019  21:38:51 +0000 (0:00:00.624)       0:00:11.046 *********** 
changed: [exit01]
changed: [exit02]

TASK [deploy_leaf : Backup FRR configuration] **********************************
Friday 12 July 2019  21:38:52 +0000 (0:00:01.125)       0:00:12.171 *********** 
changed: [exit01 -> localhost]
changed: [exit02 -> localhost]

RUNNING HANDLER [deploy_leaf : reload networking] ******************************
Friday 12 July 2019  21:38:53 +0000 (0:00:00.591)       0:00:12.762 *********** 
changed: [exit02]
changed: [exit01]

RUNNING HANDLER [deploy_leaf : restart frr with nohup] *************************
Friday 12 July 2019  21:38:54 +0000 (0:00:00.958)       0:00:13.721 *********** 
changed: [exit01]
changed: [exit02]

RUNNING HANDLER [deploy_leaf : restart lldp with nohup] ************************
Friday 12 July 2019  21:38:55 +0000 (0:00:01.150)       0:00:14.872 *********** 
changed: [exit01]
changed: [exit02]

PLAY [leaf] ********************************************************************

TASK [Gathering Facts] *********************************************************
Friday 12 July 2019  21:38:57 +0000 (0:00:01.837)       0:00:16.709 *********** 
ok: [leaf01]
ok: [leaf04]
ok: [leaf02]
ok: [leaf03]

TASK [deploy_leaf : Create ./backup folder] ************************************
Friday 12 July 2019  21:39:02 +0000 (0:00:04.839)       0:00:21.548 *********** 
ok: [leaf01 -> localhost]

TASK [deploy_leaf : Copy FRR daemons file] *************************************
Friday 12 July 2019  21:39:02 +0000 (0:00:00.352)       0:00:21.901 *********** 
changed: [leaf02]
changed: [leaf01]
changed: [leaf04]
changed: [leaf03]

TASK [deploy_leaf : L4 hash policy] ********************************************
Friday 12 July 2019  21:39:04 +0000 (0:00:01.640)       0:00:23.541 *********** 
changed: [leaf01]
changed: [leaf02]
changed: [leaf03]
changed: [leaf04]

TASK [deploy_leaf : Get hostname] **********************************************
Friday 12 July 2019  21:39:05 +0000 (0:00:01.109)       0:00:24.651 *********** 
changed: [leaf01]
changed: [leaf02]
changed: [leaf03]
changed: [leaf04]

TASK [deploy_leaf : Print inventory_hostname] **********************************
Friday 12 July 2019  21:39:06 +0000 (0:00:01.118)       0:00:25.769 *********** 
ok: [leaf01] => {
    "msg": "cumulus"
}
ok: [leaf02] => {
    "msg": "cumulus"
}
ok: [leaf03] => {
    "msg": "cumulus"
}
ok: [leaf04] => {
    "msg": "cumulus"
}

TASK [deploy_leaf : Modify hostname] *******************************************
Friday 12 July 2019  21:39:06 +0000 (0:00:00.514)       0:00:26.284 *********** 
changed: [leaf01]
changed: [leaf03]
changed: [leaf04]
changed: [leaf02]

TASK [deploy_leaf : Copy interfaces configuration] *****************************
Friday 12 July 2019  21:39:11 +0000 (0:00:04.626)       0:00:30.911 *********** 
changed: [leaf01]
changed: [leaf02]
changed: [leaf03]
changed: [leaf04]

TASK [deploy_leaf : Backup interfaces configuration] ***************************
Friday 12 July 2019  21:39:13 +0000 (0:00:01.798)       0:00:32.709 *********** 
changed: [leaf01 -> localhost]
changed: [leaf02 -> localhost]
changed: [leaf03 -> localhost]
changed: [leaf04 -> localhost]

TASK [deploy_leaf : Copy FRR configuration] ************************************
Friday 12 July 2019  21:39:14 +0000 (0:00:01.037)       0:00:33.747 *********** 
changed: [leaf03]
changed: [leaf02]
changed: [leaf01]
changed: [leaf04]

TASK [deploy_leaf : Backup FRR configuration] **********************************
Friday 12 July 2019  21:39:15 +0000 (0:00:01.373)       0:00:35.122 *********** 
changed: [leaf01 -> localhost]
changed: [leaf02 -> localhost]
changed: [leaf03 -> localhost]
changed: [leaf04 -> localhost]

RUNNING HANDLER [deploy_leaf : reload networking] ******************************
Friday 12 July 2019  21:39:17 +0000 (0:00:01.651)       0:00:36.773 *********** 
changed: [leaf01]
changed: [leaf02]
changed: [leaf03]
changed: [leaf04]

RUNNING HANDLER [deploy_leaf : restart frr with nohup] *************************
Friday 12 July 2019  21:39:18 +0000 (0:00:01.189)       0:00:37.963 *********** 
changed: [leaf01]
changed: [leaf02]
changed: [leaf04]
changed: [leaf03]

RUNNING HANDLER [deploy_leaf : restart lldp with nohup] ************************
Friday 12 July 2019  21:39:19 +0000 (0:00:01.178)       0:00:39.141 *********** 
changed: [leaf01]
changed: [leaf03]
changed: [leaf04]
changed: [leaf02]

PLAY [spine] *******************************************************************

TASK [Gathering Facts] *********************************************************
Friday 12 July 2019  21:39:20 +0000 (0:00:01.051)       0:00:40.193 *********** 
ok: [spine01]
ok: [spine02]

TASK [deploy_spine : Create ./backup folder] ***********************************
Friday 12 July 2019  21:39:23 +0000 (0:00:02.773)       0:00:42.967 *********** 
ok: [spine01 -> localhost]

TASK [deploy_spine : Copy FRR daemons file] ************************************
Friday 12 July 2019  21:39:23 +0000 (0:00:00.372)       0:00:43.340 *********** 
changed: [spine01]
changed: [spine02]

TASK [deploy_spine : L4 hash policy] *******************************************
Friday 12 July 2019  21:39:25 +0000 (0:00:01.661)       0:00:45.001 *********** 
changed: [spine01]
changed: [spine02]

TASK [deploy_spine : Get hostname] *********************************************
Friday 12 July 2019  21:39:26 +0000 (0:00:01.225)       0:00:46.227 *********** 
changed: [spine01]
changed: [spine02]

TASK [deploy_spine : Print inventory_hostname] *********************************
Friday 12 July 2019  21:39:27 +0000 (0:00:00.905)       0:00:47.133 *********** 
ok: [spine01] => {
    "msg": "cumulus"
}
ok: [spine02] => {
    "msg": "cumulus"
}

TASK [deploy_spine : Modify hostname] ******************************************
Friday 12 July 2019  21:39:27 +0000 (0:00:00.282)       0:00:47.415 *********** 
changed: [spine01]
changed: [spine02]

TASK [deploy_spine : Copy interfaces configuration] ****************************
Friday 12 July 2019  21:39:31 +0000 (0:00:03.554)       0:00:50.969 *********** 
changed: [spine01]
changed: [spine02]

TASK [deploy_spine : Backup interfaces configuration] **************************
Friday 12 July 2019  21:39:32 +0000 (0:00:01.308)       0:00:52.278 *********** 
changed: [spine01 -> localhost]
changed: [spine02 -> localhost]

TASK [deploy_spine : Copy FRR configuration] ***********************************
Friday 12 July 2019  21:39:33 +0000 (0:00:00.670)       0:00:52.949 *********** 
changed: [spine01]
changed: [spine02]

TASK [deploy_spine : Backup FRR configuration] *********************************
Friday 12 July 2019  21:39:34 +0000 (0:00:01.156)       0:00:54.105 *********** 
changed: [spine01 -> localhost]
changed: [spine02 -> localhost]

RUNNING HANDLER [deploy_spine : reload networking] *****************************
Friday 12 July 2019  21:39:35 +0000 (0:00:00.614)       0:00:54.720 *********** 
changed: [spine01]
changed: [spine02]

RUNNING HANDLER [deploy_spine : restart frr with nohup] ************************
Friday 12 July 2019  21:39:36 +0000 (0:00:00.745)       0:00:55.465 *********** 
changed: [spine01]
changed: [spine02]

RUNNING HANDLER [deploy_spine : restart lldp with nohup] ***********************
Friday 12 July 2019  21:39:36 +0000 (0:00:00.977)       0:00:56.443 *********** 
changed: [spine01]
changed: [spine02]

PLAY RECAP *********************************************************************
exit01                     : ok=14   changed=11   unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
exit02                     : ok=13   changed=11   unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
leaf01                     : ok=14   changed=11   unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
leaf02                     : ok=13   changed=11   unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
leaf03                     : ok=13   changed=11   unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
leaf04                     : ok=13   changed=11   unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
spine01                    : ok=14   changed=11   unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
spine02                    : ok=13   changed=11   unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   

Friday 12 July 2019  21:39:37 +0000 (0:00:00.839)       0:00:57.283 *********** 
=============================================================================== 
Gathering Facts --------------------------------------------------------- 4.84s
deploy_leaf : Modify hostname ------------------------------------------- 4.63s
deploy_spine : Modify hostname ------------------------------------------ 3.55s
deploy_leaf : Modify hostname ------------------------------------------- 3.09s
Gathering Facts --------------------------------------------------------- 2.77s
Gathering Facts --------------------------------------------------------- 2.62s
deploy_leaf : restart lldp with nohup ----------------------------------- 1.84s
deploy_leaf : Copy interfaces configuration ----------------------------- 1.80s
deploy_spine : Copy FRR daemons file ------------------------------------ 1.66s
deploy_leaf : Backup FRR configuration ---------------------------------- 1.65s
deploy_leaf : Copy FRR daemons file ------------------------------------- 1.64s
deploy_leaf : Copy interfaces configuration ----------------------------- 1.56s
deploy_leaf : Copy FRR configuration ------------------------------------ 1.37s
deploy_spine : Copy interfaces configuration ---------------------------- 1.31s
deploy_spine : L4 hash policy ------------------------------------------- 1.23s
deploy_leaf : reload networking ----------------------------------------- 1.19s
deploy_leaf : restart frr with nohup ------------------------------------ 1.18s
deploy_leaf : Copy FRR daemons file ------------------------------------- 1.16s
deploy_spine : Copy FRR configuration ----------------------------------- 1.16s
deploy_leaf : restart frr with nohup ------------------------------------ 1.15s
[Pipeline] }
[Pipeline] // dir
Post stage
[Pipeline] echo
[Step 03] - Network configuration is finished ...
[Pipeline] echo
[Step 03] - Network is correctly configured
[Pipeline] }
[Pipeline] // stage
[Pipeline] stage
[Pipeline] { ([Step 04] - Test_Virutal_Network)
[Pipeline] sleep
Sleeping for 10 sec
[Pipeline] dir
Running in /var/jenkins_home/workspace/network-pipeline/network-config
[Pipeline] {
[Pipeline] sh
+ chmod 777 ./tests/network-tests.py
[Pipeline] sh
+ ./tests/network-tests.py
/usr/local/lib/python3.7/site-packages/paramiko/kex_ecdh_nist.py:39: CryptographyDeprecationWarning: encode_point has been deprecated on EllipticCurvePublicNumbers and will be removed in a future version. Please use EllipticCurvePublicKey.public_bytes to obtain both compressed and uncompressed point encoding.
  m.add_string(self.Q_C.public_numbers().encode_point())
[0m/usr/local/lib/python3.7/site-packages/paramiko/kex_ecdh_nist.py:96: CryptographyDeprecationWarning: Support for unsafe construction of public numbers from encoded data will be removed in a future version. Please use EllipticCurvePublicKey.from_encoded_point
  self.curve, Q_S_bytes
[0m/usr/local/lib/python3.7/site-packages/paramiko/kex_ecdh_nist.py:111: CryptographyDeprecationWarning: encode_point has been deprecated on EllipticCurvePublicNumbers and will be removed in a future version. Please use EllipticCurvePublicKey.public_bytes to obtain both compressed and uncompressed point encoding.
  hm.add_string(self.Q_C.public_numbers().encode_point())
[network-tests.py - main] pings are True[0m
[0m[network-tests.py - main] BGP sessions established are True[0m
[0m[network-tests.py - main] BGP sessions are True[0m
[0m[network-tests.py - main] LLDP neighbors are True[0m
[0m[0m[Pipeline] }
[Pipeline] // dir
Post stage
[Pipeline] echo
[Step 04] - Network tests is finished ...
[Pipeline] echo
[Step 04] - Network has been successfully tested
[Pipeline] }
[Pipeline] // stage
[Pipeline] stage
[Pipeline] { ([Step 05] - Shutdown_Virtual_Network)
[Pipeline] dir
Running in /var/jenkins_home/workspace/network-pipeline/network-config/scripts
[Pipeline] {
[Pipeline] sh
+ chmod 777 ./eveng-api.py
[Pipeline] sh
+ ./eveng-api.py --stop=data-center-cumulus.unl
[PyEVENG - login] ...
[PyEVENG - login] (200) logged !
[PyEVENG stopLabNode] - data-center-cumulus.unl spine01 is stopping...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/1/stop/stopmode=3
[PyEVENG stopLabNode] - data-center-cumulus.unl spine01 is stopped !
[PyEVENG stopLabNode] - data-center-cumulus.unl spine02 is stopping...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/2/stop/stopmode=3
[PyEVENG stopLabNode] - data-center-cumulus.unl spine02 is stopped !
[PyEVENG stopLabNode] - data-center-cumulus.unl leaf01 is stopping...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/3/stop/stopmode=3
[PyEVENG stopLabNode] - data-center-cumulus.unl leaf01 is stopped !
[PyEVENG stopLabNode] - data-center-cumulus.unl leaf02 is stopping...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/4/stop/stopmode=3
[PyEVENG stopLabNode] - data-center-cumulus.unl leaf02 is stopped !
[PyEVENG stopLabNode] - data-center-cumulus.unl leaf03 is stopping...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/5/stop/stopmode=3
[PyEVENG stopLabNode] - data-center-cumulus.unl leaf03 is stopped !
[PyEVENG stopLabNode] - data-center-cumulus.unl leaf04 is stopping...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/6/stop/stopmode=3
[PyEVENG stopLabNode] - data-center-cumulus.unl leaf04 is stopped !
[PyEVENG stopLabNode] - data-center-cumulus.unl exit01 is stopping...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/7/stop/stopmode=3
[PyEVENG stopLabNode] - data-center-cumulus.unl exit01 is stopped !
[PyEVENG stopLabNode] - data-center-cumulus.unl exit02 is stopping...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/8/stop/stopmode=3
[PyEVENG stopLabNode] - data-center-cumulus.unl exit02 is stopped !
[PyEVENG stopLabNode] - data-center-cumulus.unl core01 is stopping...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/9/stop/stopmode=3
[PyEVENG stopLabNode] - data-center-cumulus.unl core01 is stopped !
[PyEVENG stopLabNode] - data-center-cumulus.unl core02 is stopping...
https://172.16.194.239/api/labs/Users/data-center-cumulus.unl/nodes/10/stop/stopmode=3
[PyEVENG stopLabNode] - data-center-cumulus.unl core02 is stopped !
[PyEVENG - logout] ...
[PyEVENG - logout] (200) EVE-NG says Byyye :) !


[eveng-api - exit_success] - Did you love this tool ?
Give a STAR https://gitlab.com/DylanHamel/python-eveng-api 


[Pipeline] }
[Pipeline] // dir
Post stage
[Pipeline] echo
[Step 05] - Shutdown network ...
[Pipeline] echo
[Step 05] - Shutdown has been successfully done
[Pipeline] telegramSend
[Step 05] - Shutdown has been successfully done for <network-pipeline #74> !
[Pipeline] }
[Pipeline] // stage
[Pipeline] stage
[Pipeline] { ([Step 06] - Push_Staging_To_Master)
[Pipeline] sh
+ exit 100
```

