docker pull batfish/allinone:latest
docker run -v batfish-data:/data -p 8888:8888 -p 9997:9997 -p 9996:9996 batfish/allinone

python3 -m pip install --upgrade git+https://github.com/batfish/pybatfish.git