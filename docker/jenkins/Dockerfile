FROM jenkins/jenkins:latest

MAINTAINER <dylan.hamel@protonmail.com>

USER root

ENV JAVA_OPTS="-Djenkins.install.runSetupWizard=false"

RUN apt update -y
RUN apt install -y \
	software-properties-common build-essential zlib1g-dev libncurses5-dev \
	libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev wget vim nano netcat

RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367
RUN apt-add-repository "deb http://ppa.launchpad.net/ansible/ansible/ubuntu bionic main"
RUN apt update -y
RUN apt install -y ansible

RUN curl -O https://www.python.org/ftp/python/3.7.3/Python-3.7.3.tar.xz
RUN tar -xf Python-3.7.3.tar.xz && \
	cd Python-3.7.3 && \
	./configure --enable-optimizations && \
	make -j 8 && \
	make altinstall

RUN python3.7 -m pip install pip

COPY ./files/requirements.txt /tmp/requirements.txt
RUN pip3.7 install -r /tmp/requirements.txt

COPY ./files/security.groovy /usr/share/jenkins/ref/init.groovy.d/security.groovy

COPY ./files/jenkins-cli.jar /var/jenkins_home/jenkins-cli.jar
RUN cd /var/jenkins_home/ && ls -lah && chmod 777 jenkins-cli.jar

COPY ./files/plugins.txt /usr/share/jenkins/ref/plugins.txt
RUN /usr/local/bin/install-plugins.sh < /usr/share/jenkins/ref/plugins.txt

COPY --chown=jenkins:jenkins ./files/jobs /var/jenkins_home/jobs

RUN wget https://s3.us-east-1.amazonaws.com/butlercli/1.0.0/linux/butler && \
	 chmod 777 butler && \
	 mv butler /usr/local/bin/


USER jenkins
