# Change Gitlab system name

If you set a name to your Gitlab server as ``gitlab.dh.local``. You will not able to copy paste link to clone the repo. If you want to change this value :

1. Connect to Docker container with ``root`` account

```shell
» docker exec -it -u root gitlab /bin/bash
```



2. Edit ``/etc/gitlab/gitlab.rb`` 

```shell
root@gitlab:/# vi /etc/gitlab/gitlab.rb
```



3. Add the following line

```bash
## GitLab configuration settings
##! This file is generated during initial installation and **is not** modified
##! during upgrades.
##! Check out the latest version of this file to know about the different
##! settings that can be configured by this file, which may be found at:
##! https://gitlab.com/gitlab-org/omnibus-gitlab/raw/master/files/gitlab-config-template/gitlab.rb.template


## GitLab URL
##! URL on which GitLab will be reachable.
##! For more details on configuring external_url see:
##! https://docs.gitlab.com/omnibus/settings/configuration.html#configuring-the-external-url-for-gitlab
# external_url 'GENERATED_EXTERNAL_URL'
external_url "http://172.17.0.2"					# <<<<<=====
```



4. Restart Gitlab installation

```shell
root@gitlab:/# gitlab-ctl reconfigure
```





