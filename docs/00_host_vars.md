# Specify ``password:`` and ``platform:``

For each device your have to specify this two parameters.
For the moment Nornir can't retrieve ``password:`` from Ansible and have not platform:``



### Put these values in ``host_vars/{inventory_hostname}.yml``

```yaml
password: CumulusLinux!
platform: linux
```



### Put these values in ``group_vars/{group_name}.yml``

If these values are the same for all devices, you can put they in ``group_vars`` folder

* ``all.yml``
* ``leaf.yml`` + ``spine.yml`` + ``exit.yml``

