# Linux Ubuntu - Set interface in DHCP mode

Use the following command :

```shell
ifconfig pnet3 0.0.0.0 0.0.0.0 && dhclient
```

If you don't configure IP and mask 0.0.0.0 the DHCP request will not be send for all network.

