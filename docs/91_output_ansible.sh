 » ansible-playbook deploy_fabric.yml -i hosts_virtual

PLAY [exit] ********************************************************************************************************************

TASK [Gathering Facts] *********************************************************************************************************
Friday 02 August 2019  08:58:08 +0200 (0:00:00.061)       0:00:00.061 *********
fatal: [exit01]: UNREACHABLE! => {"changed": false, "msg": "Failed to connect to the host via ssh: ssh: connect to host 172.16.194.239 port 22151: Network is unreachable", "unreachable": true}
fatal: [exit02]: UNREACHABLE! => {"changed": false, "msg": "Failed to connect to the host via ssh: ssh: connect to host 172.16.194.239 port 22152: Network is unreachable", "unreachable": true}

PLAY RECAP *********************************************************************************************************************
exit01                     : ok=0    changed=0    unreachable=1    failed=0    skipped=0    rescued=0    ignored=0
exit02                     : ok=0    changed=0    unreachable=1    failed=0    skipped=0    rescued=0    ignored=0

Friday 02 August 2019  08:58:12 +0200 (0:00:03.152)       0:00:03.213 *********
===============================================================================
Gathering Facts --------------------------------------------------------------------------------------------------------- 3.15s
------------------------------------------------------------
/Volumes/Data/gitlab/network-cicd-pipeline(master*) » ansible-playbook deploy_fabric.yml -i hosts_virtual

PLAY [exit] ********************************************************************************************************************

TASK [Gathering Facts] *********************************************************************************************************
Friday 02 August 2019  08:58:41 +0200 (0:00:00.052)       0:00:00.052 *********
ok: [exit02]
ok: [exit01]

TASK [deploy_leaf : Create ./backup folder] ************************************************************************************
Friday 02 August 2019  08:58:43 +0200 (0:00:02.348)       0:00:02.401 *********
ok: [exit01 -> localhost]

TASK [deploy_leaf : Copy FRR daemons file] *************************************************************************************
Friday 02 August 2019  08:58:44 +0200 (0:00:00.411)       0:00:02.813 *********
changed: [exit01]
changed: [exit02]

TASK [deploy_leaf : L4 hash policy] ********************************************************************************************
Friday 02 August 2019  08:58:46 +0200 (0:00:02.645)       0:00:05.458 *********
 [WARNING]: The value 1 (type int) in a string field was converted to u'1' (type string). If this does not look like what you
expect, quote the entire value to ensure it does not change.

changed: [exit02]
changed: [exit01]

TASK [deploy_leaf : Get hostname] **********************************************************************************************
Friday 02 August 2019  08:58:47 +0200 (0:00:00.988)       0:00:06.447 *********
changed: [exit01]
changed: [exit02]

TASK [deploy_leaf : Print inventory_hostname] **********************************************************************************
Friday 02 August 2019  08:58:48 +0200 (0:00:00.730)       0:00:07.178 *********
ok: [exit01] => {
    "msg": "cumulus"
}
ok: [exit02] => {
    "msg": "cumulus"
}

TASK [deploy_leaf : Modify hostname] *******************************************************************************************
Friday 02 August 2019  08:58:48 +0200 (0:00:00.092)       0:00:07.271 *********
changed: [exit01]
changed: [exit02]

TASK [deploy_leaf : Copy interfaces configuration] *****************************************************************************
Friday 02 August 2019  08:58:50 +0200 (0:00:01.996)       0:00:09.267 *********
changed: [exit01]
changed: [exit02]

TASK [deploy_leaf : Backup interfaces configuration] ***************************************************************************
Friday 02 August 2019  08:58:51 +0200 (0:00:00.715)       0:00:09.983 *********
ok: [exit01 -> localhost]
ok: [exit02 -> localhost]

TASK [deploy_leaf : Copy FRR configuration] ************************************************************************************
Friday 02 August 2019  08:58:51 +0200 (0:00:00.549)       0:00:10.532 *********
changed: [exit01]
changed: [exit02]

TASK [deploy_leaf : Backup FRR configuration] **********************************************************************************
Friday 02 August 2019  08:58:52 +0200 (0:00:00.721)       0:00:11.254 *********
ok: [exit01 -> localhost]
ok: [exit02 -> localhost]

RUNNING HANDLER [deploy_leaf : reload networking] ******************************************************************************
Friday 02 August 2019  08:58:52 +0200 (0:00:00.497)       0:00:11.751 *********
changed: [exit01]
changed: [exit02]

RUNNING HANDLER [deploy_leaf : restart frr with nohup] *************************************************************************
Friday 02 August 2019  08:58:53 +0200 (0:00:00.561)       0:00:12.313 *********
changed: [exit01]
changed: [exit02]

RUNNING HANDLER [deploy_leaf : restart lldp with nohup] ************************************************************************
Friday 02 August 2019  08:58:54 +0200 (0:00:00.496)       0:00:12.809 *********
changed: [exit01]
changed: [exit02]

PLAY [leaf] ********************************************************************************************************************

TASK [Gathering Facts] *********************************************************************************************************
Friday 02 August 2019  08:58:54 +0200 (0:00:00.666)       0:00:13.475 *********
ok: [leaf01]
ok: [leaf03]
ok: [leaf02]
ok: [leaf04]

TASK [deploy_leaf : Create ./backup folder] ************************************************************************************
Friday 02 August 2019  08:58:56 +0200 (0:00:02.253)       0:00:15.729 *********
ok: [leaf01 -> localhost]

TASK [deploy_leaf : Copy FRR daemons file] *************************************************************************************
Friday 02 August 2019  08:58:57 +0200 (0:00:00.248)       0:00:15.978 *********
changed: [leaf01]
changed: [leaf02]
changed: [leaf04]
changed: [leaf03]

TASK [deploy_leaf : L4 hash policy] ********************************************************************************************
Friday 02 August 2019  08:58:58 +0200 (0:00:00.808)       0:00:16.786 *********
changed: [leaf01]
changed: [leaf02]
changed: [leaf03]
changed: [leaf04]

TASK [deploy_leaf : Get hostname] **********************************************************************************************
Friday 02 August 2019  08:58:58 +0200 (0:00:00.457)       0:00:17.244 *********
changed: [leaf02]
changed: [leaf03]
changed: [leaf01]
changed: [leaf04]

TASK [deploy_leaf : Print inventory_hostname] **********************************************************************************
Friday 02 August 2019  08:58:59 +0200 (0:00:00.590)       0:00:17.835 *********
ok: [leaf01] => {
    "msg": "cumulus"
}
ok: [leaf02] => {
    "msg": "cumulus"
}
ok: [leaf03] => {
    "msg": "cumulus"
}
ok: [leaf04] => {
    "msg": "cumulus"
}

TASK [deploy_leaf : Modify hostname] *******************************************************************************************
Friday 02 August 2019  08:58:59 +0200 (0:00:00.200)       0:00:18.035 *********
changed: [leaf02]
changed: [leaf04]
changed: [leaf03]
changed: [leaf01]

TASK [deploy_leaf : Copy interfaces configuration] *****************************************************************************
Friday 02 August 2019  08:59:02 +0200 (0:00:03.373)       0:00:21.408 *********
changed: [leaf02]
changed: [leaf03]
changed: [leaf01]
changed: [leaf04]

TASK [deploy_leaf : Backup interfaces configuration] ***************************************************************************
Friday 02 August 2019  08:59:03 +0200 (0:00:01.260)       0:00:22.669 *********
ok: [leaf01 -> localhost]
ok: [leaf02 -> localhost]
ok: [leaf03 -> localhost]
ok: [leaf04 -> localhost]

TASK [deploy_leaf : Copy FRR configuration] ************************************************************************************
Friday 02 August 2019  08:59:04 +0200 (0:00:00.932)       0:00:23.602 *********
changed: [leaf02]
changed: [leaf01]
changed: [leaf03]
changed: [leaf04]

TASK [deploy_leaf : Backup FRR configuration] **********************************************************************************
Friday 02 August 2019  08:59:05 +0200 (0:00:01.058)       0:00:24.661 *********
ok: [leaf01 -> localhost]
ok: [leaf02 -> localhost]
ok: [leaf03 -> localhost]
ok: [leaf04 -> localhost]

RUNNING HANDLER [deploy_leaf : reload networking] ******************************************************************************
Friday 02 August 2019  08:59:06 +0200 (0:00:00.793)       0:00:25.454 *********
changed: [leaf02]
changed: [leaf01]
changed: [leaf04]
changed: [leaf03]

RUNNING HANDLER [deploy_leaf : restart frr with nohup] *************************************************************************
Friday 02 August 2019  08:59:07 +0200 (0:00:00.791)       0:00:26.245 *********
changed: [leaf02]
changed: [leaf03]
changed: [leaf01]
changed: [leaf04]

RUNNING HANDLER [deploy_leaf : restart lldp with nohup] ************************************************************************
Friday 02 August 2019  08:59:08 +0200 (0:00:00.975)       0:00:27.220 *********
changed: [leaf04]
changed: [leaf01]
changed: [leaf02]
changed: [leaf03]

PLAY [spine] *******************************************************************************************************************

TASK [Gathering Facts] *********************************************************************************************************
Friday 02 August 2019  08:59:09 +0200 (0:00:01.152)       0:00:28.373 *********
ok: [spine02]
ok: [spine01]

TASK [deploy_spine : Create ./backup folder] ***********************************************************************************
Friday 02 August 2019  08:59:11 +0200 (0:00:01.881)       0:00:30.255 *********
ok: [spine01 -> localhost]

TASK [deploy_spine : Copy FRR daemons file] ************************************************************************************
Friday 02 August 2019  08:59:11 +0200 (0:00:00.244)       0:00:30.499 *********
changed: [spine01]
changed: [spine02]

TASK [deploy_spine : L4 hash policy] *******************************************************************************************
Friday 02 August 2019  08:59:12 +0200 (0:00:00.605)       0:00:31.105 *********
changed: [spine01]
changed: [spine02]

TASK [deploy_spine : Get hostname] *********************************************************************************************
Friday 02 August 2019  08:59:12 +0200 (0:00:00.633)       0:00:31.739 *********
changed: [spine01]
changed: [spine02]

TASK [deploy_spine : Print inventory_hostname] *********************************************************************************
Friday 02 August 2019  08:59:13 +0200 (0:00:00.623)       0:00:32.363 *********
ok: [spine01] => {
    "msg": "cumulus"
}
ok: [spine02] => {
    "msg": "cumulus"
}

TASK [deploy_spine : Modify hostname] ******************************************************************************************
Friday 02 August 2019  08:59:13 +0200 (0:00:00.132)       0:00:32.496 *********
changed: [spine01]
changed: [spine02]

TASK [deploy_spine : Copy interfaces configuration] ****************************************************************************
Friday 02 August 2019  08:59:17 +0200 (0:00:04.009)       0:00:36.505 *********
changed: [spine02]
changed: [spine01]

TASK [deploy_spine : Backup interfaces configuration] **************************************************************************
Friday 02 August 2019  08:59:19 +0200 (0:00:01.378)       0:00:37.884 *********
ok: [spine01 -> localhost]
ok: [spine02 -> localhost]

TASK [deploy_spine : Copy FRR configuration] ***********************************************************************************
Friday 02 August 2019  08:59:20 +0200 (0:00:00.967)       0:00:38.851 *********
changed: [spine02]
changed: [spine01]

TASK [deploy_spine : Backup FRR configuration] *********************************************************************************
Friday 02 August 2019  08:59:21 +0200 (0:00:01.559)       0:00:40.411 *********
ok: [spine01 -> localhost]
ok: [spine02 -> localhost]

RUNNING HANDLER [deploy_spine : reload networking] *****************************************************************************
Friday 02 August 2019  08:59:22 +0200 (0:00:00.673)       0:00:41.084 *********
changed: [spine02]
changed: [spine01]

RUNNING HANDLER [deploy_spine : restart frr with nohup] ************************************************************************
Friday 02 August 2019  08:59:23 +0200 (0:00:00.811)       0:00:41.896 *********
changed: [spine02]
changed: [spine01]

RUNNING HANDLER [deploy_spine : restart lldp with nohup] ***********************************************************************
Friday 02 August 2019  08:59:24 +0200 (0:00:01.234)       0:00:43.131 *********
changed: [spine02]
changed: [spine01]

PLAY RECAP *********************************************************************************************************************
exit01                     : ok=14   changed=9    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
exit02                     : ok=13   changed=9    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
leaf01                     : ok=14   changed=9    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
leaf02                     : ok=13   changed=9    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
leaf03                     : ok=13   changed=9    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
leaf04                     : ok=13   changed=9    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
spine01                    : ok=14   changed=9    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
spine02                    : ok=13   changed=9    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0

Friday 02 August 2019  08:59:25 +0200 (0:00:00.764)       0:00:43.896 *********
===============================================================================
deploy_spine : Modify hostname ------------------------------------------------------------------------------------------ 4.01s
deploy_leaf : Modify hostname ------------------------------------------------------------------------------------------- 3.37s
deploy_leaf : Copy FRR daemons file ------------------------------------------------------------------------------------- 2.65s
Gathering Facts --------------------------------------------------------------------------------------------------------- 2.35s
Gathering Facts --------------------------------------------------------------------------------------------------------- 2.25s
deploy_leaf : Modify hostname ------------------------------------------------------------------------------------------- 2.00s
Gathering Facts --------------------------------------------------------------------------------------------------------- 1.88s
deploy_spine : Copy FRR configuration ----------------------------------------------------------------------------------- 1.56s
deploy_spine : Copy interfaces configuration ---------------------------------------------------------------------------- 1.38s
deploy_leaf : Copy interfaces configuration ----------------------------------------------------------------------------- 1.26s
deploy_spine : restart frr with nohup ----------------------------------------------------------------------------------- 1.23s
deploy_leaf : restart lldp with nohup ----------------------------------------------------------------------------------- 1.15s
deploy_leaf : Copy FRR configuration ------------------------------------------------------------------------------------ 1.06s
deploy_leaf : L4 hash policy -------------------------------------------------------------------------------------------- 0.99s
deploy_leaf : restart frr with nohup ------------------------------------------------------------------------------------ 0.97s
deploy_spine : Backup interfaces configuration -------------------------------------------------------------------------- 0.97s
deploy_leaf : Backup interfaces configuration --------------------------------------------------------------------------- 0.93s
deploy_spine : reload networking ---------------------------------------------------------------------------------------- 0.81s
deploy_leaf : Copy FRR daemons file ------------------------------------------------------------------------------------- 0.81s
deploy_leaf : Backup FRR configuration ---------------------------------------------------------------------------------- 0.79s