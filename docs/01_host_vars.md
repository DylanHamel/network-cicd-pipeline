# Always use relative path from ``.``
Example :
Path in ``./tests/script.py`` for retrieve data in file ``./tests/inventory/data/common.yml`` has to be :

```python
PATH_TO_COMMON_DATA = "./tests/inventory/data/common.yml"
```

The reason is that all scripts are run from ``./``