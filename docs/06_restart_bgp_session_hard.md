# Restart Cumulus BGP sessions

```
» ansible -i hosts exit,leaf,spine -m shell -a "service frr restart" --become
```

