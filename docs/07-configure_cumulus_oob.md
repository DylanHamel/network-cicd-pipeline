# Configure Cumulus OOB network (``eth0``)

On each device execute :

* spine01
```shell
net add interface eth0 ip address 10.0.4.101/24
net commit
```

* spine02
```shell
net add interface eth0 ip address 10.0.4.102/24
net commit
```


* leaf01
```shell
net add interface eth0 ip address 10.0.4.201/24
net commit
```

* leaf02
```shell
net add interface eth0 ip address 10.0.4.202/24
net commit
```

* leaf03
```shell
net add interface eth0 ip address 10.0.4.203/24
net commit
```

* leaf04
```shell
net add interface eth0 ip address 10.0.4.204/24
net commit
```

* exit01
```shell
net add interface eth0 ip address 10.0.4.151/24
net commit
```

* exit02
```shell
net add interface eth0 ip address 10.0.4.152/24
net commit
```