» ./tests/network-tests.py
./tests/nornir/config_virtual.yml
[network-tests.py - main] All BGP sessions are established => True
[network-tests.py - main] All BGP sessions are UP regarding YAML file (./tests/topology/bgp_sessions.yml) => True
[network-tests.py - main] All LLDP neighbors are UP regarding YAMl file (./tests/topology/lldp_links.yml) True
[network-tests.py - main] All interfaces IPv4 addresses are correct regarding YAMl file (./tests/topology/ipv4_addresses.yml)  True
[network-tests.py - main] All PINGS work regarding YAMl file (./tests/topology/ping_network.yml) True
[network-tests.py - main] All VLAN and VXLAN are present regarding YAMl file (./tests/topology/interfaces_vlan.yml) True
[network-tests.py - main] All MLAG are UP regarding YAMl file (./tests/topology/mlag.yml) True
[network-tests.py - main] All VRF VNI are present regarding YAMl file (./tests/topology/interfaces_vlan.yml) True
[network-tests.py - main] All EVPN (VNI) sessions are UP regarding YAMl file (./tests/topology/evpn.yml) True