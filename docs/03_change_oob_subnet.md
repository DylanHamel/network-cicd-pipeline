# Which files do you have to change to move your OOB Network

Change :

1. IP on out-of-band interface in ``host_vars/{{inventory_hostname}}.yml``

```yaml
port: 22151

loopback: 10.10.0.151/32
mtu: 9000

oob:
  ip_addr: 10.0.4.151     # <<<<<======
  netmask: 24
  alias: Out-Of-Band
  default: 10.0.4.1       # <<<<<======
  vrf: mgmt
```

2. Change IP in your ``topology/topology.yml``
You have to check if ``network: pnet`` is already correct

```yaml
- id: 25
  network: pnet5
  src:
    - host: spine01
      port: eth0
      ip_mgmt: 10.0.4.101       # <<<<<======
      ssh: 22
      nat: 22101
    - host: spine02
```


3. Change in backup files ``topology/config/hostname/interfaces``

```shell
# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).

source /etc/network/interfaces.d/*.intf

# The primary network interface
auto eth0
iface eth0 inet
  address 10.0.4.201/24       # <<<<<======
  alias Out-Of-Band
  gateway 10.0.4.1            # <<<<<======
  vrf mgmt

```
