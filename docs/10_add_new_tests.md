# Add a new network test

###### Dylan Hamel - <dylan.hamel@protonmail.com> - 2019



## Introduction

All tests are written with Nornir (Python).

Actually there are tests for :

* Check if all BGP sessions are UP.
* BGP sessions based on YAML file ``./tests/topology/bgp_sessions.yml``
* MLAG based on YAML file `./tests/topology/mlag.yml`
* VRF-VNI based on YAML file `./tests/topology/vrf_vni.yml`
* LLDP based on YAML file `./tests/topology/lldp_links.yml`
  * This file is a copy of ``links:`` from your ``./topology/topology.yml``
* IPv4 interfaces based on YAML file `./tests/topology/ipv4_addresses.yml`
* EVPN interfaces based on YAML file `./tests/topology/evpn.yml`
* Check if devices can reach other with ping based on YAML file `./tests/topology/ping_network.yml`



For each tests there are a Nornir task in ``./tests/network-tests.py``.

```python
def check_cumulus_mlag(nr: Nornir):

    # No MLAG ON SPINE !!!
    devices = nr.filter(
        F(groups__contains="leaf") |
        F(groups__contains="exit")
    )

    if len(devices.inventory.hosts) == 0:
        raise Exception(f"[check_cumulus_mlag] no device selected.")

    path_url = os.environ['tests_path']+MLAG_TO_CHECK

    data = devices.run(
        task=netests.mlag._cumulus_get_mlag_infos,
        on_failed=True,
        num_workers=10
    )
    #print_result(data)
    if data.failed is True:
        return False

    netests.mlag._cumulus_mlag_infos_converter(devices)
    content = open_file(path_url)

    return (netests.mlag.compare_mlag(content, devices))
```



There are a file to retrieve output from devices and compare with YAML file.
YAML files are the REFERENCE !
All theses files are stored in ``./tests/netests``

To compare devices outputs and YAML file a Python Class has been created for each test.
All these classes are stored in  `./tests/netests/networkCheck`

```python
class BGPSession:

    src_hostname: str
    dst_hostname: str
    src_interface: str

    # ------------------------------------------------------------
    #
    #
    def __init__(self, src_hostname: str(),  dst_hostname: str(), src_interface: str()):
        self.src_hostname = src_hostname
        self.dst_hostname = dst_hostname
        self.src_interface = src_interface

    # ------------------------------------------------------------
    #
    #
    def __eq__(self, other):
        if not isinstance(other, BGPSession):
            return NotImplemented
    
        return ((self.src_hostname == other.src_hostname and
                self.dst_hostname == other.dst_hostname) and
                self.src_interface == other.src_interface)

    # ------------------------------------------------------------
    #
    #
    def __repr__(self):
        return f"<BGPSession src_hostname={self.src_hostname} dst_hostname=		 {self.dst_hostname} src_interface={self.src_interface}>\n"
```



## Add a new tests

1. Create a new reference file in ``./tests/topology/your_tests.yml``

2. In ``./tests/network-tests.py`` add information about this file

   ```python
   ######################################################
   #
   # Constantes
   #
   EXCEPTIONS_HEADER = "Error import [network-tests]"
   PRINT_HEADER = "[network-tests.py - main]"
   VLAN_VXLAN_TO_CHECK = "interfaces_vlan.yml"
   MLAG_TO_CHECK = "mlag.yml"
   YOUR_TO_CHECK = "your_tests.yml"						# <<<<<=====
   ```

3. Create files for retrive production devices outputs and compare with YAML file.

4. Add a Nornir function ``./tests/network-tests.py``. This function has to return a ``bool``.

5. Add a call to your Nornir task in ``./tests/network-tests.py``

   ```python
   		# ''''''''''''''''''''''''''''''''''''''''''''
       # 9. Check YOUR
       # ''''''''''''''''''''''''''''''''''''''''''''
       your = your_cumulus(nr)
       print(f"{PRINT_HEADER} YOUR are UP regarding YAML file ({YOUR_TO_CHECK}) {your}")
   ```

6. Add Nornir return value in main function return.

   ```python
   return (ping_works and \
           all_bgp_session_established and \
           check_cumulus_bgp and \
           lldp_neighbors and \
           ipv4_adresses and \
           vlan_vxlan_exist and \
           mlag_works and \
           vrf_vni_works and \
           evpn_works and \
           your)
   ```

7. In the pipeline add git commands to retrieve previous file for test network before change. At step 
   ``(Test_Production_Network_Before_Changes)``

   ```groovy
   def restore_ping_tests = sh returnStatus: true, script: 'git --no-pager show HEAD^:tests/topology/your_tests.yml > tests/topology/backup/your_tests.yml'
   ```

   



























