# Run Jenkins & Gitlab 

```shell
» docker run --detach \
  --hostname gitlab.dh.local \
  --publish 44443:443 --publish 4480:80 --publish 4422:22 \
  --name gitlab \
  --restart always \
  --volume config:/etc/gitlab \
  --volume logs:/var/log/gitlab \
  --volume data:/var/opt/gitlab \
  gitlab/gitlab-ce:latest
```


```shell
docker run –detach \
--name jenkins \ 
--publish 8080:8080 \ --publish 50000:50000 \ 
--volume ./home:/home \
dh/jenkins
```