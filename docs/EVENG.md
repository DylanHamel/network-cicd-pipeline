# Setup Network CI/CD pipeline from scratch

###### Dylan Hamel - Engineering School Project - <dylan.hamel@protonmail.com> - August 2019



## Prerequisites

You need to have Python3.7, Ansible, Git and Docker installed on your machine.

EVE-NG installation is not described in this documentation

```shell
» docker version
Client: Docker Engine - Community
 Version:           19.03.1
 API version:       1.40
 Go version:        go1.12.5
```

```shell
» python3.7 --version
Python 3.7.0
```

```shell
» ansible --version
ansible 2.8.1
```

```shell
» git --version
git version 2.20.1 (Apple Git-117)
```



## Download files from Gitlab

Go on my Gitlab and clone ``network-cicd-pipeline`` repo.

```shell
» git clone https://gitlab.com/DylanHamel/network-cicd-pipeline
» cd network-cicd-pipeline
```

You should retrieve all the following files

```shell
» ll
-rw-r--r--@  1 dylan.hamel  admin  56963 14 jul 12:44 [1]  README.md
-rw-r--r--   1 dylan.hamel  admin    487  7 jul 22:03 [2]  ansible.cfg
drwxr-xr-x   5 dylan.hamel  admin    170  4 aoû 19:26 [3]  backup
drwxr-xr-x   5 dylan.hamel  admin    170 30 jul 22:24 [4]  batfish
drwxr-xr-x  12 dylan.hamel  admin    408 14 jul 00:54 [5]  config
drwxr-xr-x   5 dylan.hamel  admin    170 30 jui 17:39 [6]  debug
-rw-r--r--   1 dylan.hamel  admin     74 11 jul 14:41 [7]  deploy_default.yml
-rw-r--r--   1 dylan.hamel  admin     59  1 jul 20:56 [8]  deploy_exit.yml
-rw-r--r--   1 dylan.hamel  admin    176  1 jul 20:56 [9]  deploy_fabric.yml
-rw-r--r--   1 dylan.hamel  admin    191  4 aoû 20:09 [10] deploy_fabric_virt.yml
-rw-r--r--   1 dylan.hamel  admin     60  1 jul 20:56 [11] deploy_leaf.yml
-rw-r--r--   1 dylan.hamel  admin     61  1 jul 20:56 [12] deploy_spine.yml
drwxr-xr-x   6 dylan.hamel  admin    204 27 aoû 15:38 [13] docker
drwxr-xr-x  18 dylan.hamel  admin    612 27 aoû 16:01 [14] docs
drwxr-xr-x   8 dylan.hamel  admin    272 12 jul 17:25 [15] group_vars
drwxr-xr-x  10 dylan.hamel  admin    340 20 jul 22:42 [16] host_vars
-rw-r--r--@  1 dylan.hamel  admin    601 21 jul 14:45 [17] hosts
-rw-r--r--   1 dylan.hamel  admin    722  2 aoû 10:21 [18] hosts_virtual
drwxr-xr-x   3 dylan.hamel  admin    102 14 jul 12:43 [19] images
drwxr-xr-x  10 dylan.hamel  admin    340  4 aoû 20:07 [20] roles
-rw-r--r--@  1 dylan.hamel  admin    111 14 jul 01:08 [21] run-demo.yml
drwxr-xr-x  11 dylan.hamel  admin    374 30 jul 21:26 [22] scripts
drwxr-xr-x   4 dylan.hamel  admin    136 22 jui 15:09 [23] secure
drwxr-xr-x   9 dylan.hamel  admin    306  4 aoû 11:53 [24] tests
drwxr-xr-x   5 dylan.hamel  admin    170  4 aoû 19:22 [25] tools
drwxr-xr-x   6 dylan.hamel  admin    204 29 jul 21:07 [26] topology
```



## Run Jenkins and Gitlan in a Container

Go into the ``docker`` folder.

```shell
» cd docker
```

Use ``docker-compose`` to build your environnement

```shell
» docker-compose up -d
```

If ``docker-compose`` doesn't work you can build container manually.

* Gitlab

```shell
» cd gitlab && docker build -t dh/gitlab .
» docker run --detach \  
						--publish 44443:443 \
						--publish 4480:80 \
						--publish 4422:22 \
            --name gitlab \
            --restart always \
            --volume ./config:/etc/gitlab \
            --volume ./logs:/var/log/gitlab \
            --volume ./data:/var/opt/gitlab \
						dh/gitlab
#						gitlab/gitlab-ce:latest
# There are not important command in the Dockerfile. You can also directly use gitlab
# container
```

* Jenkins

```shell
» cd jenkins && docker build -t dh/jenkins .
» » docker run --detach \
							--name myjenkins \
							-p 8080:8080 \
							-p 50000:50000 \
							dh/jenkins
```

It will take some time. Go to drink a coffee :).
Python3.7, Ansible will be installed.

When Jenkins is up, generate a new SSH keys.

```shell
» docker exec -it -u root jenkins /bin/bash
root@f61eac9c1d09:/#
# Yet you are logged with root account.
root@f61eac9c1d09:/# ssh-keygen -t rsa -C "jenkins@dh.local"
# ADD A PASSPHRASE !!!!

root@f61eac9c1d09:/# ls -lh /root/.ssh/
total 8.0K
-rw------- 1 root root 1.7K Aug 27 17:36 id_rsa
-rw-r--r-- 1 root root  398 Aug 27 17:36 id_rsa.pub

# You can also create a jenkins_deploy account to avoid to use the root account...
```

Check jenkins and gitlab IP addresses :

```shell
» docker inspect gitlab | grep -i "ipaddr" && docker inspect jenkins | grep -i "ipaddr"
            "SecondaryIPAddresses": null,
            "IPAddress": "172.17.0.3",
                    "IPAddress": "172.17.0.3",
            "SecondaryIPAddresses": null,
            "IPAddress": "172.17.0.2",
                    "IPAddress": "172.17.0.2",
```



## Gitlab

When container is up, you need to create 3 users :

1. ``root`` account to manager Gitlab
2. ``jenkins_deploy`` system account, repository ``network-config`` owner.
3. ``dylan.hamel`` your own account.

![01_gitlab](.//01_gitlab.png)

Signin with ``Jenkins_deploy`` account.

Create a new project (``network-config``). This project will contain all network configurations files.

* Ansible playbooks and roles
* Devices variables (``host_vars/``, ``group_vars/``, etc.)
* Scripts to deploy network in a virtual environnement
* Scripts to test your network
* Network configuration backups

![02_gitlab](./images/setup/02_gitlab.png)

Add your account as member for this repository.

On the left: "Setting -->> Members"

![26_gitlab.png](./images/setup/26_gitlab.png)

**Add to project**

Now you can login with your own account ``dylan.hamel``. Normally you will see the repository.

![03_gitlab](./images/setup/03_gitlab.png)

Add a SSH key to your Gitlab.

Go to your settings

![04_gitlab](./images/setup/04_gitlab.png)

Go to "SSH Keys"

![05_gitlab](./docs/jenkins_gitlab_images/05_gitlab.png)

Generate a SSH key for Gitlab.

```shell
» ssh-keygen -o -t rsa -b 4096 -C "dylan.hamel@protonmail.com"
Generating public/private rsa key pair.
Enter file in which to save the key (/Users/dylan.hamel/.ssh/id_rsa): /Users/dylan.hamel/.ssh/gitlab_local_id_rsa
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /Users/dylan.hamel/.ssh/gitlab_local_id_rsa.
Your public key has been saved in /Users/dylan.hamel/.ssh/gitlab_local_id_rsa.pub.


» ls ~/.ssh | grep gitlab                                                         
gitlab_local_id_rsa
gitlab_local_id_rsa.pub
```

Copy-Paste your public keys into Gitlab.

![06_gitlab](./images/setup/06_gitlab.png)

**Add key**

Configure your laptop ssh settings to use this key with this URL.

```shell
» vim ~/.ssh/config
```

Add the following lines.

```shell
Host localhost
  Hostname localhost
  Port 4422
  User dylan.hamel
  AddKeysToAgent yes
  UseKeychain yes
  IdentityFile ~/.ssh/gitlab_local_id_rsa
```

With these lines each time you execute a ``git clone`` or some others commands you will automatically use the good key.



Clone your repo in a folder on your laptop.

![07_gitlab](./images/setup/07_gitlab.png)

```shell
» pwd
/Users/dylan.hamel/Desktop

» git@172.17.0.2:jenkins_deploy/network-config.git
Cloning into 'network-config'...

```

If you only copy-past the link that you find in gitlab you see that you will be not able to download the file. This is totally normal. You are trying to use the IPv4 172.17.0.2 but this IP is a Docker network and is unreachable from your laptop. When you run the container you had done a port-forwarding to port tcp:22 of Gitlab container (``--publish 4422:22`` ) you need to use these informations.

```shell
» git clone git@localhost:/jenkins_deploy/network-config.git
Cloning into 'network-config'...
warning: You appear to have cloned an empty repository.
```

Try to push a file in the repositor.

```shell
» cd network-config
(master)» touch myfile && echo "HELLO WORLD" > myfile
```

Push all files into the Gitlab repository

```shell
» git add .
» git commit -m "First commit with all files"
» git push

# ERRRROR
```

Actually the repository is empty. So you can't push some files if you are not a maintainer.

With ``jenkins_deploy`` account create a file.

![08_gitlab](./images/setup/08_gitlab.png)

This file will be removed.

![09_gitlab](./images/setup/09_gitlab.png)

With you laptop retrieve ``useless.coooooool`` file.

```shell
» git pull                                                                              dylan.hamel@MacBook-Pro-de-Dylan
warning: no common commits
remote: Enumerating objects: 3, done.
remote: Counting objects: 100% (3/3), done.
remote: Total 3 (delta 0), reused 0 (delta 0)
Unpacking objects: 100% (3/3), done.
From localhost:/jenkins_deploy/network-config
 * [new branch]      master     -> origin/master
fatal: refusing to merge unrelated histories
```

Your ``git pull`` has been refused ... !!

**BY DEFAULT IN GITLAB, MASTER BRANCH IS PROTECTED**.

In our case, master branch will be protected. You need to create an staging branch where **developper** will have access.

![10_gitlab](./images/setup/10_gitlab.png)

Create a new branch. Standard names are `develop` or ``staging``.

![11_gitlab](./images/setup/11_gitlab.png)

Create the new staging branch.

![12_gitlab](./images/setup/12_gitlab.png)

Create an other branch named ``history``.

![27_gitlab](./images/setup/27_gitlab.png)

![28_gitlab](./images/setup/28_gitlab.png)

The default branch for all users is ``staging`` only ``jenkins_deploy`` can use the master branch. In Gitlab set the ``staging`` branch as the default of the repository.

![13_gitlab](./images/setup/13_gitlab.png)

You see that the ``staging`` branch is not protected.

``history`` branch can be protected or not it's not very important.

To make easy remove completely the repository.

```shell
» rm -rf network-config
```

Clone another time the repository with the``useless.coooooool`` file

```shell
» git clone git@localhost:/jenkins_deploy/network-config.git
Cloning into 'network-config'...
remote: Enumerating objects: 3, done.
remote: Counting objects: 100% (3/3), done.
remote: Total 3 (delta 0), reused 0 (delta 0)
Receiving objects: 100% (3/3), done.

» cd network-config

(staging) » ls
usless.coooooool
```

You can see that now your are not on the ``master`` branch ! You don't see the ``master`` branch.

```shell
(staging) » git branch
* [1] staging
```

You can now copy all files that are in the public repository :

* https://gitlab.com/DylanHamel/network-cicd-pipeline

```shell
(staging) » cp -r /path/to/public/gitlab/repository/network-cicd-pipeline/* .
```

Push all file in your local gitlab.

```shell
(staging*) » git add .
(staging*) » git commit -m "First commit with all files"
(staging) » git push                                                                             dylan.hamel@MacBook-Pro-de-Dylan
Enumerating objects: 5585, done.
Counting objects: 100% (5585/5585), done.
Delta compression using up to 8 threads
Compressing objects: 100% (5483/5483), done.
Writing objects: 100% (5584/5584), 13.09 MiB | 5.46 MiB/s, done.
Total 5584 (delta 4778), reused 0 (delta 0)
remote: Resolving deltas: 100% (4778/4778), done.
To localhost:/jenkins_deploy/network-config.git
   fb680c5..36371e6  staging -> staging
```

All files are now on your own Gitlab on ``staging`` branch.

![14_gitlab](./images/setup/14_gitlab.png)

Files are not on the master branch !!! Nobody push files into the ``master`` branch. With ``jenkins_deploy`` user you can see that master branch is empty.

![15_gitlab](./images/setup/15_gitlab.png)



![16_gitlab](./images/setup/16_gitlab.png)

Only ``Jenkins_deploy`` can push some files into master. For the moment that is not important. ``Jenkins_deploy`` will be used into the CI/CD pipeline and will push data into ``master``.

You need to have the following branches

![29_gitlab](./images/setup/29_gitlab.png)

## Jenkins configuration

Click on "network-pipeline".

![00_jenkins](./images/setup/00_jenkins.png)

Click on "configuration"

![01_jenkins](./images/setup/01_jenkins.png)

Normally the job is imported. You need to add your private SSH key.

But you need to add new SSH key in the credentials

**--> Credentials --> Ajouter**

![02_jenkins](/Volumes/Data/gitlab/network-cicd-pipeline/docs/jenkins_gitlab_images/02_jenkins.png)

Create new Credentials. Add your private key that you generated before.

```shell
» docker exec -it -u root jenkins /bin/bash
root@f61eac9c1d09:/# cat ~/.ssh/id_rsa
```

![03_jenkins](./images/setup/03_jenkins.png)

You have to add public key in Gitlab too. Login with ``jenkins_deploy`` account.

**-->> Settings -->> SSH Keys** / Copy-Past your public key and click on **Add key**

```shell
» docker exec -it -u root jenkins /bin/bash
root@f61eac9c1d09:/# cat ~/.ssh/id_rsa.pub
```

![17_gitlab](./images/setup/17_gitlab.png)

Now you have information to retrieve files. On Jenkins you should not see error message.

![04_jenkins](./images/setup/04_jenkins.png)

Parameters to check :

1. Check that the IP adresse in the **Repository URL** is correct. You can see with the following command.

   ```shell
   » docker inspect gitlab | grep -i "ipaddr"
               "SecondaryIPAddresses": null,
               "IPAddress": "172.17.0.3",
                       "IPAddress": "172.17.0.3",
   ```

2. Check the repository name. If you create a user with a different name that ``Jenkins_deploy`` you need to rename the URL.

   ```Shell
   git@172.17.0.3:YOUR_USER_MASER/network-config.git
   ```

3. If you create your repository with a different name that ``network_config`` you have to rename the URL

   ```shell
   git@172.17.0.3:jenkins_deploy/REPOSITORY_NAME.git
   ```

4. Verify path to ``Jenkinsfile``. Normally it's ``./docker/jenkins/Jenkinsfile``. If you move this file. Change the **docker/jenkins/Jenkinsfile**.

5. If you create a branch with a different name that ``staging``, you have to change **Branch Specifier** parameter with your branch name.



For a continous integration and continuous deployement pipeline you want that when a user push some files on the ``staging`` branch the pipeline is automatically run.

For that you have to create a WebHook on Gitlab that will triger the pipeline if there is a push in the ``staging`` branch.

By default Gitlab don't authorize to execute a WebHook on a local IP address. To allow it log into Gitlab with the root account.

![18_gitlab](./images/setup/18_gitlab.png)

Go to **Settings -->> Network** and add the option.

![19_gitlab](./images/setup/19_gitlab.png)

**Save changes**

Now use the ``jenkins_deploy`` account. Go on the project and in **Settings -->> Integration**.

![20_gitlab](./images/setup/20_gitlab.png)

You need to give the Jenkins URL to the ``network-pipeline`` job. You can find this information directly on Jenkins.

![05_jenkins](./images/setup/05_jenkins.png)

To avoid that everybody can run the pipeline with a simple WEB call a Secret has to be given in the payload. Click on "Generate" to have a need Secret Token.

In Jenkins we define that only ``staging` branch can trigger the pipeline.

Add all these informations on Gitlab WebHook.

![21_gitlab](./images/setup/21_gitlab.png)

Replace ``http://localhost:8080`` with the Jenkins container IP address. One more time you can find this informations with the following command :

```shell
» docker inspect jenkins | grep -i "ipaddr"
            "SecondaryIPAddresses": null,
            "IPAddress": "172.17.0.2",
                    "IPAddress": "172.17.0.2",
```

**Save changes**

You can now test the WebHook.

![22_gitlab](./images/setup/22_gitlab.png)

If it works, you will have a "HTTP return code = 200"

![23_gitlab](./images/setup/23_gitlab.png)

And, on Jenkins, you can see that the pipeline will be started

![06_jenkins](./images/setup/06_jenkins.png)

You can have more information about this build with clicking on the **#4**.

**Console Output**

![07_jenkins](./images/setup/07_jenkins.png)



## Jenkinsfile

There are some modifications to do in ``Jenkinsfile``.

In the **Step 00**, change the Credentials.

```shell
stage ('[Step 00] - Prepare_Git_Environnement') {
	steps {
		git credentialsId: 'jenkins_gitlab', url: 'git@172.17.0.3:dhamel/network-config.git'
```

![03_jenkins](./images/setup/03_jenkins.png)

=> Change with your value

```groovy
stage ('[Step 00] - Prepare_Git_Environnement') {
	steps {
git credentialsId:'jenkins_deploy',url:'git@172.17.0.3:jenkins_deploy/network-config.git'
```

Change the URL.

```Shell
'git@172.17.0.3:jenkins_deploy/network-config.git'
```

Update with your user and repository name if there are different.



There was a problem with git if files were retrieved with SSH. To fix this issue, I use an **Personnal Access Token**.

To create a personnal access token to ``jenkins_deploy`` connect to Gitlab.

**Setting -->> Access Token**.

![24_gitlab](./images/setup/24_gitlab.png)

**Create peronal access token**

> Save your personnal token
>
> Warning !!!!! With this token an user can retrieve and modify all your repositories !!!!

![25_gitlab](./images/setup/25_gitlab.png)

Replace the token in the ``Jenkinsfile``.

```shell
stage ('[Step 00] - Prepare_Git_Environnement') {
 steps {
git credentialsId:'jenkins_deploy',url:'git@172.17.0.3:jenkins_deploy/network-config.git'
sh 'rm -rf network-config'
sh 'git clone http://oauth2:8P9pHyM-41g58t23ebsa@172.17.0.3/jenkins_deploy/network-config.git'
```





## Slack Integration

First you have to create a Slack Room.

https://slack.com/intl/fr-ch/

When your room is created, you can create a channel. In our case we will create a ``network-pipeline`` channel that will contains all informations about pipeline executions.

![06_slack](./images/setup/06_slack.png)

Give the name and a little description

![07_slack](./images/setup/07_slack.png)

Now you have your new channel. You can see that I had already create a channel te open some pipelines issues.

![00_slack](./images/setup/00_slack.png)

Aller ensuite dans les paramètres du channel.

**Administration -->> Gérer les applications**

![01_slack](./images/setup/01_slack.png)

Click on **Parcourir la liste des applications**

![02_slack](./images/setup/02_slack.png)

Search Jenkins plugin integration

![03_slack](./images/setup/03_slack.png)

Click on **Ajouter la configuration**

![04_slack](./images/setup/04_slack.png)

Define on which channel you would like to send pipeline execution informations.

![08_slack](./images/setup/08_slack.png)

You will find a guide to configure your Jenkins to send message to Slack. Juste follow the documentation.

![09_slack](./images/setup/09_slack.png)

Important thing is the Access Token

![10_slack](./images/setup/10_slack.png)

Go to your Jenkins

**Administrer Jenkins**

![17_jenkins](./images/setup/17_jenkins.png)

If you use the ``docker-compose`` or the ``jenkins/Dockerfile``, Slack plugins is automatically installed in your Jenkins server.

Otherwise you need to install it.

Click on **Gestion des plugins**.

![19_jenkins](./images/setup/19_jenkins.png)

Got to "Disponible" plugins and search "Slack". Click on an install with a restart.

![20_jenkins](./images/setup/20_jenkins.png)

Wait until the packet is installed and restart Jenkins with **Redémarrer Jenkins ...**

![21_jenkins](./images/setup/21_jenkins.png)

Wait until Jenkins restart.

![22_jenkins](./images/setup/22_jenkins.png)

Return in "Jenkins Administration" and click on **Manage system**.

![18_jenkins](./images/setup/18_jenkins.png)

Find the Slack configuration and add your Room and Access Token informations.

![23_jenkins](./images/setup/23_jenkins.png)

1. Insert your Slack Room name

3. Insert your Channel on which one you would like receive Jenkins messages.

Create credentials with API Token => Clik on **Ajouter**.

![24_jenkins](./images/setup/24_jenkins.png)

Enter your Access Toekn an click on **Ajouter**.

Select your new credentials and test the connection.

You will receive a message in your channel. If not there is an error with Access Token, channel name or room name.

![11_slack](./images/setup/11_slack.png)

**Save**



## Test

On your laptop verify that your repository is up to date.

```Shell
(staging) » git pull
Already up to date.
```

**Actually there are a bugs with Jenkins. My Jenkinsfile is too long an the pipeline failed ...**

To test the pipeline you can remove "Production steps". In fact, you don't have access to a production network :).

```groovy
/*
        stage('[Step 07] - Start_Production_Network') {
            when {
                expression {
                    return RUN_PIPELINE == "YES";
                }
            }

....
....
....

}
        */

        stage('[Step 11] - Pipeline_Finished_Sucessfully') {
            when {
```

To see your modifications you can run the following command.

```shell
(staging) » git status
On branch staging
Your branch is up to date with 'origin/staging'.

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   docker/jenkins/Jenkinsfile
```

In this case we will not use ``git add .``. This command will add all files that have changed. In this case you want only add ``Jenkinsfile``.

```shell
(staging*) » git add docker/jenkins/Jenkinsfile

(staging*) » git commit -m "Update Jenkinsfile - remove production steps"
[staging c9ca08f] Update Jenkinsfile - remove production steps
 1 file changed, 2 insertions(+), 1 deletion(-)

 (staging*) » git push
```

You can see that the pipeline has been triggred.

SCM, Step 00 and Step 01 have been done successfully.

There was a problem on the EVE-NG VM but it's not a Jenkins, Gitlab or pipeline issue !

![09_jenkins](./images/setup/09_jenkins.png)

#### TIPS

To test the pipeline you have to modify ``./topology/topology.yml`` file. The reason is that the network will be created on EVE-NG **ONLY** if this file changes !

If this file doesn't change network will only be started

For example, you cann add a new word in the project description ("Archi").

```yaml
---
project:
  path: /Users/
  name: data-center-cumulus
  version: 1
  author: DylanHamel
  description: Data Center Network with Cumulus Network devices
  body: HEIG-VD - TB - Archi
```

Then commit and push modifications.

```shell
(staging*)» git add topology/topology.yml
(staging*)» git commit -m "Test pipeline"
(staging*)» git push
```

![10_jenkins](./images/setup/10_jenkins.png)

You can see informations about the commit

Click on the pipeline ID (#13)

![11_jenkins](./images/setup/11_jenkins.png)

Then you can see who push a modification, the commit ID and more informations.

![12_jenkins](./images/setup/12_jenkins.png)

Click on **Changes** to see which files are changed

![13_jenkins](./images/setup/13_jenkins.png)

1. You can see the commit message ``git commit -m "Test pipeline"``
2. You can see which files changed

You can also see what files have been retrieved by Jenkins.

Click on **Workplace** and click on the job path.

![14_jenkins](./images/setup/14_jenkins.png)

You can see all files and which directories have been created and downloaded.

![15_jenkins](./images/setup/15_jenkins.png)

Clik on **network-config** to see all files.

![16_jenkins](./images/setup/16_jenkins.png)
