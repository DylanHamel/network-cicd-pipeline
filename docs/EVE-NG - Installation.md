# EVE-NG - Installation

###### Dylan Hamel - <dylan.hamel@protonmail.com> - Septembre 2019



## Outils et versions

MacOS Mojave Version ``10.14.6``.

VirtualBox Version ``6.0.12`` - ``VirtualBox-6.0.12-133076-OSX.dmg``.

* https://www.virtualbox.org/wiki/Downloads

EVE-NG community Version ``2.0.3-95`` - ``EVE Community VM.ova``. 

* https://www.eve-ng.net/downloads/eve-ng-2

Cumulus Networks VX Version ``3.7.9`` - ``cumulus-linux-3.7.9-vx-amd64-qemu.qcow2``

* https://cumulusnetworks.com/products/cumulus-vx/download/

Extreme Networks VOSS Version ``7.1.0.0`` - ``VOSSGNS3.7.1.0.0.qcow2``

* https://github.com/extremenetworks/Virtual_VOSS

## Installation de VMWare Fusion



## /!\ /!\ /!\ ......Ne fonctionnr pas actuellement...... /!\ /!\ /!\ Installation de VirtualBox 

Après avoir télécharger l'OVA d'EVE-NG l'importer dans VirtualBox.

![01_eveng](./images/eveng/01_eveng.png)

![02_eveng](./images/eveng/02_eveng.png)

Laisser les paramètres par défaut.

![03_eveng](./images/eveng/03_eveng.png)

Attendre la fin de l'importation.

![04_eveng](./images/eveng/04_eveng.png)

Aller dans configuration

![05_eveng](./images/eveng/05_eveng.png)

Aller dans la configuration réseau et définir l'``interface 2`` en mode ``réseau interne`` et cliquer sur OK. Dans l'état, l'adresse IPv4 de la machine EVE-NG sera "pontée" sur le réseau publique de la machine hôte, en l'occurrence, le MacBookPro.

![06_eveng](./images/eveng/06_eveng.png)

Certains réseaux publiques empêchent l'optention d'une adresse IPv4 via DHCP avec ce type de connexion. Pour résoudre ce problème, utiliser un accès naté.

![07_eveng](./images/eveng/07_eveng.png)

Dans l'exemple ci-dessous, la machine utilisera un réseau "ponté".

Démarrer la machine EVE-NG et se connecter avec l'utilisateur ``root`` et le mot de passe ``eve``.

![08_eveng](./images/eveng/08_eveng.png)

``Ctrl+C`` / ``Ctrl+L`` une fois connecté pour éviter de changer le mot de passe.

L'adresse IPv4 récupérée par la machine EVE-NG est ``192.168.1.173``. Depuis la machine hôte, il doit être possible de se connecter sur cette dernière via une connexion SSH.

```shell
» ping 192.168.1.173
PING 192.168.1.173 (192.168.1.173): 56 data bytes
64 bytes from 192.168.1.173: icmp_seq=0 ttl=64 time=0.280 ms

» ssh -l root 192.168.1.173
root@192.168.1.173's password:
```

Il est également possible de se connecter avec HTTP en utilisant les credentials ``admin`` / ``eve``.

![09_eveng](./images/eveng/09_eveng.png)

## Importer les images Cumulus et Extreme Networks

Depuis la machine hôte aller dans le dossier contenant les images Cumulus et Extreme Networks.

```shell
» cd ~/Downloads
```

Uploader avec SCP les images vers la machine EVE-NG.

```shell
» scp cumulus-linux-3.7.9-vx-amd64-qemu.qcow2 root@192.168.1.173:/tmp
» scp VOSSGNS3.7.1.0.0.qcow2 root@192.168.1.173:/tmp
```

Depuis la machine EVE-NG, aller dans le dossier ``/opt/unetlab/addons/qemu/``.

```shell
cd /opt/unetlab/addons/qemu/
```

Créer les deux dossiers pour Cumulus et Extreme Networks.

```shell
mkdir cumulus-vx-3.7.9
mkdir extremexos-voss-7.1
```

Aller dans le dossier ``/opt/unetlab/addons/qemu/cumulus-vx-3.7.9``.

```shell
cd cumulus-vx-3.7.9
```

Déplacer l'image Cumulus Networks (``cumulus-linux-3.7.9-vx-amd64-qemu.qcow2``) et la renommer ``virtioa.qcow2``.

```shell
mv /tmp/cumulus-linux-3.7.9-vx-amd64-qemu.qcow2 virtioa.qcow2
```

Aller dans le dossier ``/opt/unetlab/addons/qemu/extremexos-voss-7.1``.

```shell
cd ../extremexos-voss-7.1/
```

Déplacer l'image Extreme Networks (``VOSSGNS3.7.1.0.0.qcow2``) et la renommer ``hda.qcow2 ``.

```shell
mv /tmp/VOSSGNS3.7.1.0.0.qcow2 hda.qcow2
```

Fixer les permissions

```shell
/opt/unetlab/wrappers/unl_wrapper -a fixpermissions
```



## Vérifier les images

Se connecter sur la plateforme EVE-NG via HTTP et créer un nouveau ``lab``.

![10_eveng](./images/eveng/10_eveng.png)

Ajouter les descriptions du laboratoire (étape facultative).

![11_eveng](./images/eveng/11_eveng.png)

Ajouter les équipements avec le clique droit directement sur l'interface EVE-NG.

![12_eveng](./images/eveng/12_eveng.png)

Ajouter l'équipement Cumulus Networks qui doit être disponible suite aux manipulations du point précédent. De base, les équipements Cumulus Networks possèdent 256MB de RAM. Il est très fortement conseillé d'augementer cette valeur à minimum 512MB.

Le nombre de port ethernet peut également être augmentés.

**Changement la méthode d'administration. Ces équipements se configurent via telnet.**

![13_eveng](./images/eveng/13_eveng.png)

Idem pour Extreme Networks.

Normalement, aucun paramètre n'est à changer.

![14_eveng](./images/eveng/14_eveng.png)

Cliquer sur ``Save``.

Démarrer les deux types d'équipement afin de s'assurer qu'ils fonctionnent correctement.

![15_eveng](./images/eveng/15_eveng.png)