#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

from netests.l2vni import _hosts_l2vnis
from nornir import InitNornir
import yaml

data = dict()
NORNIR_CONFIG_FILE_VIRTUAL = "./tests/nornir/config_virtual.yml"
NORNIR_LOGS_FILE = "./tests/nornir/nornir.log"
NORNIR_DEBUG_MODE = "debug"

with open("tests/topology/l2vni.yml", 'r') as yamlFile:
    try:
        data = yaml.load(yamlFile)
    except yaml.yamlError as exc:
        print(exc)
        print(
            "[network-tests - open_file] Error with YAML file - Check if your syntax and your path are correct")

print(data)

nr = InitNornir(
    config_file=NORNIR_CONFIG_FILE_VIRTUAL,
    logging={"file": NORNIR_LOGS_FILE, "level": NORNIR_DEBUG_MODE}
)

print(nr.inventory.hosts)
print(_hosts_l2vnis(data, nr, "leaf01"))
