#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

"""
Description ...

"""

__author__ = "Dylan Hamel"
__maintainer__ = "Dylan Hamel"
__version__ = "0.1"
__email__ = "dylan.hamel@protonmail.com"
__status__ = "Prototype"
__copyright__ = "Copyright 2019"

######################################################
#
# Default value used for exit()
#
EXIT_SUCCESS = 0
EXIT_FAILURE = 1

######################################################
#
# Class
#

class VRFVNI:

    vrf: str
    vni: str

    # ------------------------------------------------------------
    #
    #
    def __init__(self, vrf: str(), vni: str()):
        self.vrf = vrf
        self.vni = vni

    # ------------------------------------------------------------
    #
    #
    def __eq__(self, other):
        if not isinstance(other, VRFVNI):
            raise NotImplemented

        return (self.vrf == other.vrf and
                self.vni == other.vni)

    # ------------------------------------------------------------
    #
    #
    def __repr__(self):
        return f"<VRFVNI vrf={self.vrf} vni={self.vni}>\n"




class ListVRFVNI:

    vrf_vnis: list

    # ------------------------------------------------------------
    #
    #
    def __init__(self, vrf_vnis: list()):
        self.vrf_vnis = vrf_vnis

    # ------------------------------------------------------------
    #
    #
    def __eq__(self, others):
        if not isinstance(others, ListVRFVNI):
            raise NotImplemented
        for vrf_vni in self.vrf_vnis:
            if vrf_vni not in others.vrf_vnis:
                print(
                    f"[ListVRFVNI - __eq__] - The following VRF-VNI is not in the list \n {vrf_vni}")
                print(f"[ListVRFVNI - __eq__] - List: \n {others.vrf_vnis}")
                return False

        for vrf_vni in others.vrf_vnis:
            if vrf_vni not in self.vrf_vnis:
                print(
                    f"[ListVRFVNI - __eq__] - The following VRF-VNI is not in the list \n {vrf_vni}")
                print(f"[ListVRFVNI - __eq__] - List: \n {self.vrf_vnis}")
                return False

        return True

    # ------------------------------------------------------------
    #
    #
    def __repr__(self):
        result = "<ListVRFVNI \n"
        for vrf_vni in self.vrf_vnis:
            result = result + f"{vrf_vni}"
        return result+"\n>"
