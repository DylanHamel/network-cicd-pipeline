#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

"""
Description ...

"""

__author__ = "Dylan Hamel"
__maintainer__ = "Dylan Hamel"
__version__ = "0.1"
__email__ = "dylan.hamel@protonmail.com"
__status__ = "Prototype"
__copyright__ = "Copyright 2019"

######################################################
#
# Default value used for exit()
#
EXIT_SUCCESS = 0
EXIT_FAILURE = 1

######################################################
#
# Class
#

class IPv4Address:

    hostname: str
    interface: str
    address: list

    # ------------------------------------------------------------
    #
    #
    def __init__(self, hostname: str(), interface: str(),  address: list()):
        self.hostname = hostname
        self.interface = interface
        self.address = address

    # ------------------------------------------------------------
    #
    #
    def __eq__(self, other):
        if not isinstance(other, IPv4Address):
            return NotImplemented

        return (self.hostname == other.hostname and
                self.interface == other.interface and
                self.address == other.address)

    # ------------------------------------------------------------
    #
    #
    def __repr__(self):
        return f"<IPv4Address hostname={self.hostname} interface={self.interface} addr={self.address}>\n"





class ListIPv4Address:

    lst_ipv4: list

    # ------------------------------------------------------------
    #
    #
    def __init__(self, lst_ipv4: list()):
        self.lst_ipv4 = lst_ipv4

    # ------------------------------------------------------------
    #
    #
    def __eq__(self, other):
        if not isinstance(other, ListIPv4Address):
            raise NotImplemented

        for ipv4 in self.lst_ipv4:
            if ipv4 not in other.lst_ipv4:
                print(
                    f"[ListIPv4Address - __eq__] - The following IPIv4 is not in the list \n {ipv4}")
                print(
                    f"[ListIPv4Address - __eq__] - List: \n {other.lst_ipv4}")
                return False
        
        for ipv4 in other.lst_ipv4:
            if ipv4 not in self.lst_ipv4:
                print(
                    f"[ListIPv4Address - __eq__] - The following IPIv4 is not in the list \n {ipv4}")
                print(f"[ListVRFVNI - __eq__] - List: \n {self.lst_ipv4}")
                return False

        return True

    # ------------------------------------------------------------
    #
    #
    def __repr__(self):
        result = "<ListIPv4Address \n"
        for ipv4 in self.lst_ipv4:
            result = result + f"{ipv4}"
        return result+"\n>"
