#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-


"""
Description ...

"""

__author__ = "Dylan Hamel"
__maintainer__ = "Dylan Hamel"
__version__ = "0.1"
__email__ = "dylan.hamel@protonmail.com"
__status__ = "Prototype"
__copyright__ = "Copyright 2019"

######################################################
#
# Default value used for exit()
#
EXIT_SUCCESS = 0
EXIT_FAILURE = 1

######################################################
#
# Class
#
class LLDPLink:

    src: str
    spt: str
    dst: str
    dpt: str

    # ------------------------------------------------------------
    #
    #
    def __init__(self, src: str, spt: str, dst: str, dpt: str):
        self.src = src
        self.spt = spt
        self.dst = dst
        self.dpt = dpt

    # ------------------------------------------------------------
    #
    #
    def __eq__(self, other):
        if not isinstance(other, LLDPLink):
            return NotImplemented
        #print(f"Comparing {self} == {other} == > Result={(self.src == other.src and self.spt == other.spt and self.dst == other.dst and self.dpt == other.dpt) or (self.src == other.dst and self.spt == other.dpt and self.dst == other.src and self.dpt == other.spt)}")

        return (self.src == other.src and
                self.spt == other.spt and
                self.dst == other.dst and
                self.dpt == other.dpt) or (
                self.src == other.dst and
                self.spt == other.dpt and
                self.dst == other.src and
                self.dpt == other.spt)

    # ------------------------------------------------------------
    #
    #
    def __repr__(self):
        return f"<LLDPLink src={self.src} spt={self.spt} dst={self.dst} dpt={self.dpt}>\n"
