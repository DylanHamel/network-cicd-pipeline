#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

"""
Description ...

"""

__author__ = "Dylan Hamel"
__maintainer__ = "Dylan Hamel"
__version__ = "0.1"
__email__ = "dylan.hamel@protonmail.com"
__status__ = "Prototype"
__copyright__ = "Copyright 2019"

######################################################
#
# Default value used for exit()
#
EXIT_SUCCESS = 0
EXIT_FAILURE = 1

######################################################
#
# Class
#


class MLAG:

    vnis: list
    peer_if: str
    peer_ip: str
    sys_mac: str

    # ------------------------------------------------------------
    #
    #
    def __init__(self, vnis: list(), peer_if: str(),  peer_ip: str(),  sys_mac: str()):
        self.vnis = vnis
        self.peer_if = peer_if
        self.peer_ip = peer_ip
        self.sys_mac = sys_mac

    # ------------------------------------------------------------
    #
    #
    def __eq__(self, other):
        if not isinstance(other, MLAG):
            raise NotImplemented

        return (sorted(self.vnis) == sorted(other.vnis) and
                self.peer_if == other.peer_if and
                self.peer_ip == other.peer_ip and
                self.sys_mac == other.sys_mac)

    # ------------------------------------------------------------
    #
    #
    def __repr__(self):
        return f"<MLAG vnis={self.vnis} peer_if={self.peer_if} peer_ip={self.peer_ip} sys_mac={self.sys_mac}>\n"
