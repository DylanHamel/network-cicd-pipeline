#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

"""
Description ...

"""

__author__ = "Dylan Hamel"
__maintainer__ = "Dylan Hamel"
__version__ = "0.1"
__email__ = "dylan.hamel@protonmail.com"
__status__ = "Prototype"
__copyright__ = "Copyright 2019"

######################################################
#
# Default value used for exit()
#
EXIT_SUCCESS = 0
EXIT_FAILURE = 1

######################################################
#
# Class
#


class L2VNI:

    vrf_name: str()
    vrf_name: str()

    # ------------------------------------------------------------
    #
    #
    def __init__(self, vni_id: str(),  vrf_name: str()):
        self.vni_id = vni_id
        self.vrf_name = vrf_name

    # ------------------------------------------------------------
    #
    #
    def __eq__(self, other):
        if not isinstance(other, L2VNI):
            return NotImplemented

        return (self.vni_id == other.vni_id and
                 self.vrf_name == other.vrf_name)

    # ------------------------------------------------------------
    #
    #
    def __repr__(self):
        return f"<L2VNI vni_id={self.vni_id} vrf_name={self.vrf_name}>\n"





class ListL2VNI:

    l2vnis: list

    # ------------------------------------------------------------
    #
    #
    def __init__(self, l2vnis: list()):
        self.l2vnis = l2vnis

    # ------------------------------------------------------------
    #
    #
    def __eq__(self, others):
        if not isinstance(others, ListL2VNI):
            raise NotImplemented

        for l2vni in self.l2vnis:
            if l2vni not in others.l2vnis:
                print(
                    f"[ListL2VNI - __eq__] - The following L2VNI is not in the list \n {l2vni}")
                print(
                    f"[ListL2VNI - __eq__] - List: \n {others.l2vnis}")
                return False

        for l2vni in others.l2vnis:
            if l2vni not in self.l2vnis:
                print(
                    f"[ListL2VNI - __eq__] - The following BGP sessions is not in the list \n {l2vni}")
                print(
                    f"[ListL2VNI - __eq__] - List: \n {self.l2vnis}")
                return False

        return True

    # ------------------------------------------------------------
    #
    #
    def __repr__(self):
        result = "<ListL2VNI \n"
        for l2vni in self.l2vnis:
            result = result + f"{l2vni}"
        return result+"\n>"
