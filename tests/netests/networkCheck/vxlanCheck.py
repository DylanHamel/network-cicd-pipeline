#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

"""
Description ...

"""

__author__ = "Dylan Hamel"
__maintainer__ = "Dylan Hamel"
__version__ = "0.1"
__email__ = "dylan.hamel@protonmail.com"
__status__ = "Prototype"
__copyright__ = "Copyright 2019"

######################################################
#
# Default value used for exit()
#
EXIT_SUCCESS = 0
EXIT_FAILURE = 1

######################################################
#
# Class
#


class VXLAN:

    name: str
    vid: str

    # ------------------------------------------------------------
    #
    #
    def __init__(self, name: str(), vid: str()):
        self.name = name
        self.vid = vid

    # ------------------------------------------------------------
    #
    #
    def __eq__(self, other):
        if not isinstance(other, VXLAN):
            raise NotImplemented

        return (self.name == other.name and
                self.vid == other.vid)

    # ------------------------------------------------------------
    #
    #
    def __repr__(self):
        return f"<VXLAN name={self.name} vid={self.vid}>\n"


class ListVXLAN:

    vxlans: list

    # ------------------------------------------------------------
    #
    #
    def __init__(self, vxlans: list()):
        self.vxlans = vxlans

    # ------------------------------------------------------------
    #
    #
    def __eq__(self, others):
        if not isinstance(others, ListVXLAN):
            raise NotImplemented
        for vxlan in self.vxlans:
            if vxlan not in others.vxlans:
                print(
                    f"[ListVXLAN - __eq__] - The following V(X)LAN is not in the list \n {vxlan}")
                print(f"[ListVXLAN - __eq__] - List: \n {others.vxlans}")
                return False
        
        for vxlan in others.vxlans:
            if vxlan not in self.vxlans:
                print(
                    f"[ListVXLAN - __eq__] - The following V(X)LAN is not in the list \n {vxlan}")
                print(f"[ListVXLAN - __eq__] - List: \n {self.vxlans}")
                return False

        return True

    # ------------------------------------------------------------
    #
    #
    def __repr__(self):
        result = ""
        for vxlan in self.vxlans:
            result = result + f"<VXLAN name={vxlan.name} vid={vxlan.vid}>\n"
        return result
         
