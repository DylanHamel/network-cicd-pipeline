#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-


"""
Description ...

Warning !! This function work only if you have only one BRIDGE and its name is "Bridge".
The second condition is that all VLAN are assigned to this bridge

You can change bridge name in constant section

Interface  VLAN  Flags                  VNI
---------  ----  ---------------------  ------
bridge       13
             24
            555
           4001

"""

__author__ = "Dylan Hamel"
__maintainer__ = "Dylan Hamel"
__version__ = "1.0"
__email__ = "dylan.hamel@protonmail.com"
__status__ = "Prototype"
__copyright__ = "Copyright 2019"

######################################################
#
# Default value used for exit()
#
EXIT_SUCCESS = 0
EXIT_FAILURE = 1

######################################################
#
# Import Library
#
try:
    from netests.networkCheck.vxlanCheck import VXLAN
    from netests.networkCheck.vxlanCheck import ListVXLAN
except ImportError as importError:
    print(importError.msg)
    print("Error import [netests] BGPSession")
    print(importError)
    exit(EXIT_FAILURE)

try:
    from nornir.plugins.tasks.networking import netmiko_send_command
    from nornir.core import Nornir
except ImportError as importError:
    print("Error import [bgp.py] nornir")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import json
except ImportError as importError:
    print("Error import [bgp.py] json")
    print(importError)
    exit(EXIT_FAILURE)


######################################################
#
# Constantes
#
YAML_FILE_KEY = "vlan_vxlan"
YAML_ALL_KEY = "all"
YAML_GROUPS_KEY = "groups"

INTERFACE_KEY_FOR_NORNIR_HOSTS = "int_addr"
VLAN_VXLAN_KEY_FOR_NORNIR_HOSTS = "vlan_vxlan"
VLAN_VXLAN_KEY_FOR_NORNIR_HOSTS_BRIEF = "vxlan_brief"

CUMULUS_COMMAND_VLAN_ID = "net show bridge vlan json"
CUMULUS_COMMAND_INTERFACE = "net show interface all json"

BRIDGE_NAME = "bridge"
PEERLINK_NAME ="peerlink"

VLAN_KEY_NAME = "vlan"
VNI_KEY_NAME = "vni"
VLAN_END_KEY_NAME = "vlanEnd"
# ====================================================
# VXLAN & VLAN PART
# ====================================================


def _cumulus_get_interfaces_infos(task):

    if VLAN_VXLAN_KEY_FOR_NORNIR_HOSTS not in task.host.keys():
        output = task.run(
            name=f"{CUMULUS_COMMAND_VLAN_ID}",
            task=netmiko_send_command,
            command_string=CUMULUS_COMMAND_VLAN_ID
        )
        task.host[VLAN_VXLAN_KEY_FOR_NORNIR_HOSTS] = output.result
    
    #if INTERFACE_KEY_FOR_NORNIR_HOSTS not in task.host.keys():
    #    output = task.run(
    #        name=f"{CUMULUS_COMMAND_INTERFACE}",
    #        task=netmiko_send_command,
    #        command_string=CUMULUS_COMMAND_INTERFACE
    #    )
    #    task.host[INTERFACE_KEY_FOR_NORNIR_HOSTS]=output.result


# ----------------------------------------------------
#
#
def _cumulus_vlan_vxlan_infos_converter(nornirObj):
    
    for host in nornirObj.inventory.hosts:
    
        ## Check that {CUMULUS_COMMAND} has been executed and
        ## Output has been stored in task.host[VLAN_VXLAN_KEY_FOR_NORNIR_HOSTS]
        if VLAN_VXLAN_KEY_FOR_NORNIR_HOSTS in nornirObj.inventory.hosts[host].keys():
            data = json.loads(
                nornirObj.inventory.hosts[host][VLAN_VXLAN_KEY_FOR_NORNIR_HOSTS])
        else:
            raise Exception(
                f"[_cumulus_vlan_vxlan_infos_converter] - error with {VLAN_VXLAN_KEY_FOR_NORNIR_HOSTS} key => check inventory hosts file")

        list_vxlan_id = dict()

        for interface, facts in data.items():
            if BRIDGE_NAME in interface or PEERLINK_NAME in interface:
                for element in facts:
                    if VLAN_END_KEY_NAME in element.keys():
                        for vlan_id in range(element[VLAN_KEY_NAME], int(element[VLAN_END_KEY_NAME])+1):
                            list_vxlan_id[f"vlan{vlan_id}"] = vlan_id
                    else:
                        list_vxlan_id[f"vlan{element[VLAN_KEY_NAME]}"] = element[VLAN_KEY_NAME]
            
            if VLAN_KEY_NAME in facts[0].keys() and VNI_KEY_NAME in facts[0].keys():
                list_vxlan_id[interface] = facts[0][VNI_KEY_NAME]

        nornirObj.inventory.hosts[host][VLAN_VXLAN_KEY_FOR_NORNIR_HOSTS_BRIEF] = list_vxlan_id


# ----------------------------------------------------
#
#
def compare_vxlan_vlan(topology: dict(), devices: Nornir) -> bool:
        
    for host in devices.inventory.hosts:
        lst_running_vlan_vxlan = list()
        lst_config_vlan_vxlan = list()
        for name, vid in devices.inventory.hosts[host][VLAN_VXLAN_KEY_FOR_NORNIR_HOSTS_BRIEF].items():
            lst_running_vlan_vxlan.append(VXLAN(name=name, vid=vid))

        lst_config_vlan_vxlan = _hosts_vlan_vxlan(topology, devices, host)

        running = ListVXLAN(lst_running_vlan_vxlan)
        yamlFile = ListVXLAN(lst_config_vlan_vxlan)

        if running != yamlFile:
            print(f"[vxlan - compare_vxlan_vlan] - Error with {host}")
            return False

    return True


# ----------------------------------------------------
#
#
def _hosts_vlan_vxlan(topology: dict(), devices: Nornir, host: str()) -> list():

    lst_config_vlan_vxlan = list()
    retrieve = False

    if YAML_GROUPS_KEY in topology[YAML_FILE_KEY].keys():
        for file_groups in topology[YAML_FILE_KEY][YAML_GROUPS_KEY].keys():
            for group in devices.inventory.hosts[host].groups:
                if "," in file_groups:
                    if group in file_groups.split(","):
                        for vlan_vxlan in topology[YAML_FILE_KEY][YAML_GROUPS_KEY][file_groups]:
                            lst_config_vlan_vxlan.append(
                                VXLAN(name=vlan_vxlan['name'], vid=vlan_vxlan['vid']))
                else:
                    if group in topology[YAML_FILE_KEY][YAML_GROUPS_KEY].keys():
                        for vlan_vxlan in topology[YAML_FILE_KEY][YAML_GROUPS_KEY][group]:
                            lst_config_vlan_vxlan.append(
                                VXLAN(name=vlan_vxlan['name'], vid=vlan_vxlan['vid']))

    if YAML_ALL_KEY in topology[YAML_FILE_KEY].keys():
        for vlan_vxlan in topology[YAML_FILE_KEY][YAML_ALL_KEY]:
            lst_config_vlan_vxlan.append(
                VXLAN(name=vlan_vxlan['name'], vid=vlan_vxlan['vid']))
    
    if host in topology[YAML_FILE_KEY].keys():
        for vlan_vxlan in topology[YAML_FILE_KEY][host]:
            lst_config_vlan_vxlan.append(
                VXLAN(name=vlan_vxlan['name'], vid=vlan_vxlan['vid']))

    return lst_config_vlan_vxlan
