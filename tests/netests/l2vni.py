#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-


"""
Description ...

"""

__author__ = "Dylan Hamel"
__maintainer__ = "Dylan Hamel"
__version__ = "1.0"
__email__ = "dylan.hamel@protonmail.com"
__status__ = "Prototype"
__copyright__ = "Copyright 2019"

######################################################
#
# Default value used for exit()
#
EXIT_SUCCESS = 0
EXIT_FAILURE = 1

######################################################
#
# Import Library
#
try:
    import pprint
    PP = pprint.PrettyPrinter(indent=4)
except ImportError as importError:
    print("{EXCEPTIONS_HEADER} pprint")
    print(importError)
    exit(EXIT_FAILURE)

try:
    from netests.networkCheck.l2vniCheck import L2VNI
    from netests.networkCheck.l2vniCheck import ListL2VNI
except ImportError as importError:
    print(importError.msg)
    print("Error import [netests] BGPSession")
    print(importError)
    exit(EXIT_FAILURE)

try:
    from nornir.plugins.tasks.networking import netmiko_send_command
    from nornir.core import Nornir
except ImportError as importError:
    print("Error import [bgp.py] nornir")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import json
except ImportError as importError:
    print("Error import [l2vni.py] json")
    print(importError)
    exit(EXIT_FAILURE)


######################################################
#
# Constantes
#
YAML_ALL_KEY = "all"
YAML_GROUPS_KEY = "groups"
L2VNI_YAML_KEY = "l2vnis"

L2VNI_OBJECT_VRF_NAME_KEY = "vrf_name"
L2VNI_OBJECT_VNI_ID_KEY = "vni_id"

L2VNI_KEY_FOR_NORNIR_HOSTS = "l2vni_all"
L2VNI_KEY_FOR_NORNIR_HOSTS_BRIEF = "l2vni_brief"

L2VNI_TYPE_OUTPUT_KEY = "type"
L2VNI_VRF_OUTPUT_KEY = "tenantVrf"
L2VNI_TYPE_NAME = "L2"

CUMULUS_COMMAND_L2VNI = "net show evpn vni json"

# ====================================================
# L2VNI PART
# ====================================================


def _cumulus_get_l2vni(task):
    
    if L2VNI_KEY_FOR_NORNIR_HOSTS not in task.host.keys():
        output = task.run(
            name=f"{L2VNI_KEY_FOR_NORNIR_HOSTS}",
            task=netmiko_send_command,
            command_string=L2VNI_KEY_FOR_NORNIR_HOSTS
        )

        task.host[VRF_KEY_FOR_NORNIR_HOSTS] = output.result


def _cumulus_l2vni_converter(nornirObj):

    for host in nornirObj.inventory.hosts:
        if L2VNI_KEY_FOR_NORNIR_HOSTS in nornirObj.inventory.hosts[host].keys():
            data = json.loads(
                nornirObj.inventory.hosts[host][L2VNI_KEY_FOR_NORNIR_HOSTS])
        else:
            raise Exception(
                f"[_cumulus_l2vni_converter] - error with {L2VNI_KEY_FOR_NORNIR_HOSTS} key => check inventory hosts file")

        result = result()

        for vni, facts in data.items():
            if facts[L2VNI_TYPE_OUTPUT_KEY] == L2VNI_TYPE_NAME:
                result[vni][L2VNI_OBJECT_VNI_ID_KEY] = vni
                result[vni][L2VNI_OBJECT_VRF_NAME_KEY] = facts[L2VNI_VRF_OUTPUT_KEY]
   
        nornirObj.inventory.hosts[host][L2VNI_KEY_FOR_NORNIR_HOSTS_BRIEF] = result



def compare_l2vni(topology: dict(), devices: Nornir) -> bool:

    for host in devices.inventory.hosts:
        lst_running_l2vni = list()
        lst_config_l2vni = list()
        for vni, facts in devices.inventory.hosts[host][L2VNI_KEY_FOR_NORNIR_HOSTS_BRIEF].items():
            lst_running_l2vni.append(
                L2VNI(vni_id=facts[L2VNI_OBJECT_VNI_ID_KEY],
                        vrf_name=facts[L2VNI_OBJECT_VRF_NAME_KEY]))

        lst_config_vnis = _hosts_l2vnis(topology, devices, host)

        running = ListL2VNI(lst_running_l2vni)
        yamlFile = ListL2VNI(lst_config_vnis)

        if running != yamlFile:
            print(f"[l2vni - compare_l2vni] - Error with {host}")
            return False

    return True

# ----------------------------------------------------
#
#
def _hosts_l2vnis(topology: dict(), devices: Nornir, host: str()) -> list():

    lst_config_l2vni= list()
    retrieve = False

    if YAML_GROUPS_KEY in topology[L2VNI_YAML_KEY].keys():
        for file_groups in topology[L2VNI_YAML_KEY][YAML_GROUPS_KEY].keys():
            for group in devices.inventory.hosts[host].groups:
                if "," in file_groups:
                    if group in file_groups.split(","):
                        for l2vni in topology[L2VNI_YAML_KEY][YAML_GROUPS_KEY][file_groups]:
                            lst_config_l2vni.append(
                                L2VNI(vni_id=l2vni[L2VNI_OBJECT_VNI_ID_KEY], 
                                      vrf_name=l2vni[L2VNI_OBJECT_VRF_NAME_KEY]))
                else:
                    if group in topology[L2VNI_YAML_KEY][YAML_GROUPS_KEY].keys():
                        for l2vni in topology[L2VNI_YAML_KEY][YAML_GROUPS_KEY][group]:
                            lst_config_l2vni.append(
                                L2VNI(vni_id=l2vni[L2VNI_OBJECT_VNI_ID_KEY],
                                         vrf_name=l2vni[L2VNI_OBJECT_VRF_NAME_KEY]))

    if YAML_ALL_KEY in topology[L2VNI_YAML_KEY].keys():
        for l2vni in topology[L2VNI_YAML_KEY][YAML_ALL_KEY]:
            lst_config_l2vni.append(
                L2VNI(vni_id=l2vni[L2VNI_OBJECT_VNI_ID_KEY], 
                        vrf_name=l2vni[L2VNI_OBJECT_VRF_NAME_KEY]))

    if host in topology[L2VNI_YAML_KEY].keys():
        for l2vni in topology[L2VNI_YAML_KEY][host]:
            lst_config_l2vni.append(
                L2VNI(vni_id=l2vni[L2VNI_OBJECT_VNI_ID_KEY], 
                        vrf_name=l2vni[L2VNI_OBJECT_VRF_NAME_KEY]))

    return lst_config_l2vni
