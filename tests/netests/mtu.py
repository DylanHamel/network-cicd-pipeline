#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-


"""
Description ...

"""

__author__ = "Dylan Hamel"
__maintainer__ = "Dylan Hamel"
__version__ = "1.0"
__email__ = "dylan.hamel@protonmail.com"
__status__ = "Prototype"
__copyright__ = "Copyright 2019"

######################################################
#
# Default value used for exit()
#
EXIT_SUCCESS = 0
EXIT_FAILURE = 1

######################################################
#
# Import Library
#
try:
    import pprint
    PP = pprint.PrettyPrinter(indent=4)
except ImportError as importError:
    print("{EXCEPTIONS_HEADER} pprint")
    print(importError)
    exit(EXIT_FAILURE)

try:
    from nornir.plugins.tasks.networking import netmiko_send_command
    from nornir.core import Nornir
except ImportError as importError:
    print("Error import [bgp.py] nornir")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import json
except ImportError as importError:
    print("Error import [bgp.py] json")
    print(importError)
    exit(EXIT_FAILURE)


######################################################
#
# Constantes
#
MTU_KEY_FOR_NORNIR_HOSTS = "mtu_cli"
MTU_KEY_FOR_NORNIR_HOSTS_BRIEF = "mtu_brief"

MTU_CUMULUS_OUTPUT_FIRST_KEY = "iface_obj"
MTU_CUMULUS_OUTPUT_MTU_KEY = "mtu"

MTU_YAML_KEY = "mtu"
DEVICE_NB_SWP_INT_KEY = "how_many_swp_interfaces"

CUMULUS_COMMAND_MTU = "net show interface swp1-{} detail json"

# ====================================================
# INTERFACES MTU PART
# ====================================================

def _cumulus_get_interfaces_mtu(task):

    if MTU_KEY_FOR_NORNIR_HOSTS not in task.host.keys():
        output = task.run(
            name=CUMULUS_COMMAND_MTU.format(task.host[DEVICE_NB_SWP_INT_KEY]),
            task=netmiko_send_command,
            command_string=CUMULUS_COMMAND_MTU.format(
                task.host[DEVICE_NB_SWP_INT_KEY])
        )

        task.host[MTU_KEY_FOR_NORNIR_HOSTS] = output.result


# ----------------------------------------------------
#
#
def _cumulus_interfaces_mtu_converter(nornirObj):

    for host in nornirObj.inventory.hosts:
        if MTU_KEY_FOR_NORNIR_HOSTS in nornirObj.inventory.hosts[host].keys():
            data = json.loads(
                nornirObj.inventory.hosts[host][MTU_KEY_FOR_NORNIR_HOSTS])
        else:
            raise Exception(
                f"[_cumulus_interfaces_mtu_converter] - error with {MTU_KEY_FOR_NORNIR_HOSTS} key => check inventory hosts file")

        result = dict()

        for int_name, facts in data.items():
            result[int_name] = facts[MTU_CUMULUS_OUTPUT_FIRST_KEY][MTU_CUMULUS_OUTPUT_MTU_KEY]

        nornirObj.inventory.hosts[host][MTU_KEY_FOR_NORNIR_HOSTS_BRIEF] = result


# ----------------------------------------------------
#
#
def compare_interfaces_mtu(topology: dict(), devices: Nornir) -> bool:

    mtu_size = topology[MTU_YAML_KEY]
    return_value = True

    for host in devices.inventory.hosts:
        for int_name, facts in devices.inventory.hosts[host][MTU_KEY_FOR_NORNIR_HOSTS_BRIEF].items():
            if facts != mtu_size:
                print(
                    f"[compare_interfaces_mtu] MTU are {facts} on interface {int_name} on host {host} and should be {mtu_size} !!")
                return_value = False

    return return_value
