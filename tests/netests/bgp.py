#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-


"""
Description ...

"""

__author__ = "Dylan Hamel"
__maintainer__ = "Dylan Hamel"
__version__ = "1.0"
__email__ = "dylan.hamel@protonmail.com"
__status__ = "Prototype"
__copyright__ = "Copyright 2019"

######################################################
#
# Default value used for exit()
#
EXIT_SUCCESS = 0
EXIT_FAILURE = 1

######################################################
#
# Import Library
#
try:
    import pprint
    PP = pprint.PrettyPrinter(indent=4)
except ImportError as importError:
    print("{EXCEPTIONS_HEADER} pprint")
    print(importError)
    exit(EXIT_FAILURE)

try:
    from nornir.plugins.tasks.networking import netmiko_send_command
    from nornir.core import Nornir
except ImportError as importError:
    print("Error import [bgp.py] nornir")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import json

except ImportError as importError:
    print("Error import [bgp.py] json")
    print(importError)
    exit(EXIT_FAILURE)

try:
    from netests.networkCheck.bgpCheck import BGPSession
    from netests.networkCheck.bgpCheck import ListBGPSessions
except ImportError as importError:
    print(importError.msg)
    print("Error import [netests] BGPSession/ListBGPSessions")
    print(importError)
    exit(EXIT_FAILURE)

######################################################
#
# Constantes
#
BGP_YAML_KEY = "bgp_sessions"

BGP_KEY_FOR_NORNIR_HOSTS = "bgp"
BGP_KEY_FOR_NORNIR_HOSTS_BRIEF = "bgp_brief"

BGP_KEY_FOR_ASN = "asn"
BGP_KEY_FOR_TOTAL_PEERS = "totalPeers"

BGP_SESSION_ESTABLISHED = "Established"

CUMULUS_COMMAND_BGP = "net show bgp summary json"
# ====================================================
# BGP PART
# ====================================================
 
# ----------------------------------------------------
#
#
def _cumulus_get_bgp_summary(task):

    output = task.run(
        name=f"{CUMULUS_COMMAND_BGP}",
        task=netmiko_send_command,
        command_string=CUMULUS_COMMAND_BGP
    )

    task.host[BGP_KEY_FOR_NORNIR_HOSTS] = output.result

# ----------------------------------------------------
#
#
def _cumulus_bgp_summary_converter(nornirObj):

    for host in nornirObj.inventory.hosts:
        resultBGP = dict()

        if BGP_KEY_FOR_NORNIR_HOSTS in nornirObj.inventory.hosts[host].keys():
            data = json.loads(
                nornirObj.inventory.hosts[host][BGP_KEY_FOR_NORNIR_HOSTS])
        else:
            raise Exception(
                "[_cumulus_bgp_summary_converter] - error with BGP key => check inventory hosts file")

        if "ipv4 unicast" in data.keys():
            if data['ipv4 unicast'] is None:
                resultBGP[host] = dict()
            else:
                resultBGP[host] = dict()
                resultBGP[host][BGP_KEY_FOR_ASN] = data['ipv4 unicast']['as']
                resultBGP[host]['routerId'] = data['ipv4 unicast']['routerId']
                resultBGP[host]['bestPath'] = "multiPathRelax " + \
                    data['ipv4 unicast']['bestPath']['multiPathRelax']

                resultBGP[host]['vrfName'] = data['ipv4 unicast']['vrfName']
                resultBGP[host][BGP_KEY_FOR_TOTAL_PEERS] = data['ipv4 unicast']['totalPeers']
                resultBGP[host]['interfaces'] = dict()
                for port, facts in data['ipv4 unicast']['peers'].items():
                    resultBGP[host]['interfaces'][port] = dict()
                    if 'hostname' in facts.keys():
                        resultBGP[host]['interfaces'][port]['bgp_neighbor'] = facts['hostname']
                    else:
                        resultBGP[host]['interfaces'][port]['bgp_neighbor'] = "None"
                    resultBGP[host]['interfaces'][port]['peerUptime'] = facts['peerUptime']
                    resultBGP[host]['interfaces'][port]['state'] = facts['state']

        nornirObj.inventory.hosts[host][BGP_KEY_FOR_NORNIR_HOSTS_BRIEF] = resultBGP[host]

# ----------------------------------------------------
#
#
def compare_topology_and_bgp_output(topology: dict(), devices: Nornir) -> bool:

    lst_nb_peers_not_equals = list()
    lst_nb_asn_error = list()
    # Check if nb peers is not equals
    for host in devices.inventory.hosts:
        if devices.inventory.hosts[host][BGP_KEY_FOR_NORNIR_HOSTS_BRIEF][BGP_KEY_FOR_TOTAL_PEERS] != len(topology[BGP_YAML_KEY][host]['neighbors']):
            lst_nb_peers_not_equals.append(host)

        if devices.inventory.hosts[host][BGP_KEY_FOR_NORNIR_HOSTS_BRIEF][BGP_KEY_FOR_ASN] != topology[BGP_YAML_KEY][host][BGP_KEY_FOR_ASN]:
            lst_nb_asn_error.append(host)


    if len(lst_nb_peers_not_equals) > 0:
        print(
            f"[bgp - compare_topology_and_bgp_output] - Error with {lst_nb_peers_not_equals} Nb BGP peers is not equals !")
        return False
    
    if len(lst_nb_asn_error) > 0:
        print(
            f"[bgp - compare_topology_and_bgp_output] - Error with {lst_nb_asn_error} ASN is not the same !")
        return False

    for host in devices.inventory.hosts:
        if host in topology[BGP_YAML_KEY]:

            local_file_bgp = list()
            remote_running_bgp = list()

            for bgp_session in topology[BGP_YAML_KEY][host]['neighbors']:
                local_file_bgp.append(
                    BGPSession(src_hostname=host,
                               dst_hostname=bgp_session['neighbor'],
                               src_interface=bgp_session['interface']
                    )
                )

            for port, facts in devices.inventory.hosts[host][BGP_KEY_FOR_NORNIR_HOSTS_BRIEF]['interfaces'].items():
                remote_running_bgp.append(
                    BGPSession(src_hostname=host,
                                dst_hostname=facts['bgp_neighbor'],
                                src_interface=port
                    )
                )

            remote = ListBGPSessions(remote_running_bgp)
            local = ListBGPSessions(local_file_bgp)

            if remote != local:
                print(
                    f"[compare_topology_and_bgp_output] The problem is on {host}!!")
                return False

    return True

# ----------------------------------------------------
#
#
def check_all_bgp_sessions_established(devices: Nornir) -> bool:

    lst_running_bgp_sessions = list()

    # Get running configuration
    for host in devices.inventory.hosts:
        for facts in devices.inventory.hosts[host][BGP_KEY_FOR_NORNIR_HOSTS_BRIEF]['interfaces'].values():
            if facts['state'] != BGP_SESSION_ESTABLISHED:
                return False

    return True
