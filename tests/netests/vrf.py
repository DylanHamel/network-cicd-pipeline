#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-


"""
Description ...

"""

__author__ = "Dylan Hamel"
__maintainer__ = "Dylan Hamel"
__version__ = "1.0"
__email__ = "dylan.hamel@protonmail.com"
__status__ = "Prototype"
__copyright__ = "Copyright 2019"

######################################################
#
# Default value used for exit()
#
EXIT_SUCCESS = 0
EXIT_FAILURE = 1

######################################################
#
# Import Library
#
try:
    import pprint
    PP = pprint.PrettyPrinter(indent=4)
except ImportError as importError:
    print("{EXCEPTIONS_HEADER} pprint")
    print(importError)
    exit(EXIT_FAILURE)

try:
    from netests.networkCheck.vrfCheck import VRFVNI, ListVRFVNI
except ImportError as importError:
    print(importError.msg)
    print("Error import [netests] BGPSession")
    print(importError)
    exit(EXIT_FAILURE)

try:
    from nornir.plugins.tasks.networking import netmiko_send_command
    from nornir.core import Nornir
except ImportError as importError:
    print("Error import [bgp.py] nornir")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import json
except ImportError as importError:
    print("Error import [bgp.py] json")
    print(importError)
    exit(EXIT_FAILURE)


######################################################
#
# Constantes
#
VRF_VNI_YAML_KEY = "vrf_vni"

VRF_KEY_FOR_NORNIR_HOSTS = "vrf_all"
VRF_KEY_FOR_NORNIR_HOSTS_BRIEF = "vrf_brief"

VRF_VNI_INTERFACE_KEY_JSON = "vrfs"

CUMULUS_COMMAND_VRF = "net show vrf vni json"

# ====================================================
# VRF VNI PART
# ====================================================


def _cumulus_get_vrf_vni_infos(task):

    if VRF_KEY_FOR_NORNIR_HOSTS not in task.host.keys():
        output = task.run(
            name=f"{CUMULUS_COMMAND_VRF}",
            task=netmiko_send_command,
            command_string=CUMULUS_COMMAND_VRF
        )

        task.host[VRF_KEY_FOR_NORNIR_HOSTS] = output.result


# ----------------------------------------------------
#
#
def _cumulus_vrf_vni_infos_converter(nornirObj):

    for host in nornirObj.inventory.hosts:
        if VRF_KEY_FOR_NORNIR_HOSTS in nornirObj.inventory.hosts[host].keys():
            data = json.loads(
                nornirObj.inventory.hosts[host][VRF_KEY_FOR_NORNIR_HOSTS])
        else:
            raise Exception(
                f"[_cumulus_vrf_infos_converter] - error with {VRF_KEY_FOR_NORNIR_HOSTS} key => check inventory hosts file")

        result = list()

        if VRF_VNI_INTERFACE_KEY_JSON in data.keys():
            for vrf_vni in data[VRF_VNI_INTERFACE_KEY_JSON]:
                result.append({"vni":  vrf_vni['vni'], "vrf": vrf_vni['vrf']})
        
        nornirObj.inventory.hosts[host][VRF_KEY_FOR_NORNIR_HOSTS_BRIEF] = result


# ----------------------------------------------------
#
#
def compare_vrf_vni(topology: dict(), devices: Nornir) -> bool:
    
    for host in devices.inventory.hosts:

        if host in topology[VRF_VNI_YAML_KEY]:
            
            local_file_vrf_vni = list()
            remote_running_vrf_vni = list()

            for vrf_vni in topology[VRF_VNI_YAML_KEY][host]:
                local_file_vrf_vni.append(
                    VRFVNI(vrf=vrf_vni['vrf'], vni=vrf_vni['vni'])
                )

            for vrf_vni_conf in devices.inventory.hosts[host][VRF_KEY_FOR_NORNIR_HOSTS_BRIEF]:
                remote_running_vrf_vni.append(
                    VRFVNI(vrf=vrf_vni_conf['vrf'], vni=vrf_vni_conf['vni'])
                )            

            remote = ListVRFVNI(remote_running_vrf_vni)
            local = ListVRFVNI(local_file_vrf_vni)

            if remote != local:
                print(f"[compare_vrf_vni] The problem is on {host}!!")
                return False   

    return True
