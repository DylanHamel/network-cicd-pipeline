#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-


"""
Description ...

"""

__author__ = "Dylan Hamel"
__maintainer__ = "Dylan Hamel"
__version__ = "1.0"
__email__ = "dylan.hamel@protonmail.com"
__status__ = "Prototype"
__copyright__ = "Copyright 2019"

######################################################
#
# Default value used for exit()
#
EXIT_SUCCESS = 0
EXIT_FAILURE = 1

######################################################
#
# Import Library
#
#try:
#    import pprint
#    PP = pprint.PrettyPrinter(indent=4)
#except ImportError as importError:
#    print("{EXCEPTIONS_HEADER} pprint")
#    print(importError)
#    exit(EXIT_FAILURE)

try:
    from netests.networkCheck.ipCheck import IPv4Address
    from netests.networkCheck.ipCheck import ListIPv4Address
except ImportError as importError:
    print(importError.msg)
    print("Error import [netests] BGPSession")
    print(importError)
    exit(EXIT_FAILURE)

try:
    from nornir.plugins.tasks.networking import netmiko_send_command
    from nornir.core import Nornir
except ImportError as importError:
    print("Error import [bgp.py] nornir")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import json
except ImportError as importError:
    print("Error import [bgp.py] json")
    print(importError)
    exit(EXIT_FAILURE)


######################################################
#
# Constantes
#
IP_ADDRESSES_KEY = "ip_address"
LIST_OF_IP_ADDRESSES_KEY = "allentries"
INTERFACE_INFORMATIONS = "iface_obj"

INTERFACE_KEY_FOR_NORNIR_HOSTS = "int_addr"
INT_IP_FOR_NORNIR_HOSTS_BRIEF = "ip_brief"

CUMULUS_COMMAND = "net show interface all json"

IPV4_YAML_KEY = "ip_addresses"
YAML_GROUPS_KEY = "groups"
YAML_ALL_KEY = "all"
# ====================================================
# IPv4 PART
# ====================================================

def _cumulus_get_interfaces_infos(task):

    if INTERFACE_KEY_FOR_NORNIR_HOSTS not in task.host.keys():
        output = task.run(
            name="net show bgp summary json",
            task=netmiko_send_command,
            command_string=CUMULUS_COMMAND
        )

        task.host[INTERFACE_KEY_FOR_NORNIR_HOSTS] = output.result


# ----------------------------------------------------
#
#
def _cumulus_interfaces_infos_converter(nornirObj):

    for host in nornirObj.inventory.hosts:
        resultIPv4 = dict()

        ## Check that {CUMULUS_COMMAND} has been executed and
        ## Output has been stored in task.host[IPV4_ADDR_KEY_FOR_NORNIR_HOSTS]
        if INTERFACE_KEY_FOR_NORNIR_HOSTS in nornirObj.inventory.hosts[host].keys():
            data = json.loads(
                nornirObj.inventory.hosts[host][INTERFACE_KEY_FOR_NORNIR_HOSTS])
        else:
            raise Exception(
                f"[_cumulus_interfaces_infos_converter] - error with {INTERFACE_KEY_FOR_NORNIR_HOSTS} key => check inventory hosts file")

        for interface in data:
            if INTERFACE_INFORMATIONS in data[interface].keys():
                if IP_ADDRESSES_KEY in data[interface][INTERFACE_INFORMATIONS].keys():
                    if LIST_OF_IP_ADDRESSES_KEY in data[interface][INTERFACE_INFORMATIONS][IP_ADDRESSES_KEY].keys():
                        list_ip = list()
                        for ip_address in data[interface][INTERFACE_INFORMATIONS][IP_ADDRESSES_KEY][LIST_OF_IP_ADDRESSES_KEY]:
                            list_ip.append(ip_address)         
                        if len(list_ip) > 0:
                            resultIPv4[interface] = list_ip
         
        nornirObj.inventory.hosts[host][INT_IP_FOR_NORNIR_HOSTS_BRIEF] = resultIPv4




# ----------------------------------------------------
#
#
def compare_ipv4_adresses(topology: dict(), devices: Nornir) -> bool:

    
    lst_error_host = list()

    # Get running configuration
    for host in devices.inventory.hosts:
        lst_running_ip_addresses = list()
        
        for interface, list_ip in devices.inventory.hosts[host][INT_IP_FOR_NORNIR_HOSTS_BRIEF].items():
                for ip in list_ip:

                    # To remove IPv6 address
                    if "/128" not in ip and "/64" not in ip and "/48" not in ip and "::" not in ip:
                        lst_running_ip_addresses.append(
                            IPv4Address(hostname=host,
                                        interface=interface,
                                        address=ip
                            )
                        )

        list_local_yaml = _hosts_ipv4(topology, devices, host)

        remote = ListIPv4Address(lst_running_ip_addresses)
        local = ListIPv4Address(list_local_yaml)

        if remote != local:
            lst_error_host.append(host)
                
    
    if len(lst_error_host) > 0:
        print(f"[compare_ipv4_adresses] The problem is on {lst_error_host}!!")
        return False

    return True



# ----------------------------------------------------
#
#
def _hosts_ipv4(topology: dict(), devices: Nornir, host: str()) -> list():

    lst_config_ipv4 = list()
    retrieve = False
    
    if YAML_GROUPS_KEY in topology[IPV4_YAML_KEY].keys():
        for file_groups in topology[IPV4_YAML_KEY][YAML_GROUPS_KEY].keys():
            for group in devices.inventory.hosts[host].groups:
                if "," in file_groups:
                    if group in file_groups.split(","):
                        for ipv4 in topology[IPV4_YAML_KEY][YAML_GROUPS_KEY][file_groups]:
                            lst_config_ipv4.append(
                                IPv4Address(hostname=host,
                                            interface=ipv4['name'],
                                            address=ipv4['addr']
                                )
                            )
                else:
                    if group in topology[IPV4_YAML_KEY][YAML_GROUPS_KEY].keys():
                        for ipv4 in topology[IPV4_YAML_KEY][YAML_GROUPS_KEY][group]:
                            lst_config_ipv4.append(
                                IPv4Address(hostname=host,
                                            interface=ipv4['name'],
                                            address=ipv4['addr']
                                )
                            )

    if YAML_ALL_KEY in topology[IPV4_YAML_KEY].keys():
        for ipv4 in topology[IPV4_YAML_KEY][YAML_ALL_KEY]:
            lst_config_ipv4.append(
                IPv4Address(hostname=host,
                            interface=ipv4['name'],
                            address=ipv4['addr']
                )
            )

    if host in topology[IPV4_YAML_KEY].keys():
        for ipv4 in topology[IPV4_YAML_KEY][host]:
            lst_config_ipv4.append(
                IPv4Address(hostname=host,
                            interface=ipv4['name'],
                            address=ipv4['addr']
                            )
            )

    return lst_config_ipv4
