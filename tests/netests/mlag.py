#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-


"""
Description ...

"""

__author__ = "Dylan Hamel"
__maintainer__ = "Dylan Hamel"
__version__ = "1.0"
__email__ = "dylan.hamel@protonmail.com"
__status__ = "Prototype"
__copyright__ = "Copyright 2019"

######################################################
#
# Default value used for exit()
#
EXIT_SUCCESS = 0
EXIT_FAILURE = 1

######################################################
#
# Import Library
#
try:
    import pprint
    PP = pprint.PrettyPrinter(indent=4)
except ImportError as importError:
    print("{EXCEPTIONS_HEADER} pprint")
    print(importError)
    exit(EXIT_FAILURE)

try:
    from netests.networkCheck.mlagCheck import MLAG
except ImportError as importError:
    print(importError.msg)
    print("Error import [netests] BGPSession")
    print(importError)
    exit(EXIT_FAILURE)

try:
    from nornir.plugins.tasks.networking import netmiko_send_command
    from nornir.core import Nornir
    from nornir.plugins.functions.text import print_result
except ImportError as importError:
    print("Error import [bgp.py] nornir")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import json
except ImportError as importError:
    print("Error import [bgp.py] json")
    print(importError)
    exit(EXIT_FAILURE)


######################################################
#
# Constantes
#
MLAG_YAML_KEY = "mlags"

MLAG_KEY_FOR_NORNIR_HOSTS = "mlag_all"
MLAG_KEY_FOR_NORNIR_HOSTS_BRIEF = "mlag_brief"

CLAG_INTERFACE_KEY_JSON = "clagIntfs"
CLAG_STATUS_KEY_JSON = "status"
CLAG_ID_INTERFACE_KEY_JSON = "clagId"

CLAG_PEER_IP_KEY_YAML = "peer_ip"
CLAG_PEER_IF_KEY_YAML = "peer_if"
CLAG_PEER_ALIVE_KEY_YAML = "peer_alive"
CLAG_SYS_MAC_KEY_YAML = "sys_mac"
CLAG_VNIS_KEY_YAML = "vnis"

CUMULUS_COMMAND_MLAG = "net show clag json"
CUMULUS_COMMAND_MLAG_PARAM = "net show clag params json"

# ====================================================
# MLAG PART
# ====================================================


def _cumulus_get_mlag_infos(task):

    if MLAG_KEY_FOR_NORNIR_HOSTS not in task.host.keys():
        output = task.run(
            name=f"{CUMULUS_COMMAND_MLAG}",
            task=netmiko_send_command,
            command_string=CUMULUS_COMMAND_MLAG
        )

        task.host[MLAG_KEY_FOR_NORNIR_HOSTS] = output.result


# ----------------------------------------------------
#
#
def _cumulus_mlag_infos_converter(nornirObj):

    for host in nornirObj.inventory.hosts:
        if MLAG_KEY_FOR_NORNIR_HOSTS in nornirObj.inventory.hosts[host].keys():
            data = json.loads(
                nornirObj.inventory.hosts[host][MLAG_KEY_FOR_NORNIR_HOSTS])
        else:
            raise Exception(
                f"[_cumulus_mlag_infos_converter] - error with {MLAG_KEY_FOR_NORNIR_HOSTS} key => check inventory hosts file")

        result = dict()
        lst_vni = list()

        if CLAG_INTERFACE_KEY_JSON in data.keys():
            for interface, facts in data[CLAG_INTERFACE_KEY_JSON].items():
                if facts[CLAG_ID_INTERFACE_KEY_JSON] == 0:
                    lst_vni.append(interface)

            result[CLAG_VNIS_KEY_YAML] = lst_vni

        if CLAG_STATUS_KEY_JSON in data.keys():
            result[CLAG_PEER_ALIVE_KEY_YAML] = data[CLAG_STATUS_KEY_JSON]['peerAlive']
            result[CLAG_PEER_IP_KEY_YAML] = data[CLAG_STATUS_KEY_JSON]['peerIp']
            result[CLAG_PEER_IF_KEY_YAML] = data[CLAG_STATUS_KEY_JSON]['peerIf']
            result[CLAG_SYS_MAC_KEY_YAML] = data[CLAG_STATUS_KEY_JSON]['sysMac']

        nornirObj.inventory.hosts[host][MLAG_KEY_FOR_NORNIR_HOSTS_BRIEF] = result


# ----------------------------------------------------
#
#
def compare_mlag(topology: dict(), devices: Nornir) -> bool:

    lst_mlag_down = list()

    for host in devices.inventory.hosts:
        if devices.inventory.hosts[host][MLAG_KEY_FOR_NORNIR_HOSTS_BRIEF][CLAG_PEER_ALIVE_KEY_YAML] is not True:
            lst_mlag_down.append(host)
    
    if len(lst_mlag_down) > 0:
        print(f"[mlag - compare_mlag] - Error with {lst_mlag_down} MLAG are down !")
        return False

    for host in devices.inventory.hosts:
        if host in topology[MLAG_YAML_KEY]:
            local_file_mlag = MLAG(
                    vnis=topology[MLAG_YAML_KEY][host][CLAG_VNIS_KEY_YAML],
                    peer_if=topology[MLAG_YAML_KEY][host][CLAG_PEER_IF_KEY_YAML],
                    peer_ip=topology[MLAG_YAML_KEY][host][CLAG_PEER_IP_KEY_YAML],
                    sys_mac=topology[MLAG_YAML_KEY][host][CLAG_SYS_MAC_KEY_YAML])

            remote_running_mlag = MLAG(
                vnis=devices.inventory.hosts[host][MLAG_KEY_FOR_NORNIR_HOSTS_BRIEF][CLAG_VNIS_KEY_YAML],
                peer_if=devices.inventory.hosts[host][MLAG_KEY_FOR_NORNIR_HOSTS_BRIEF][CLAG_PEER_IF_KEY_YAML],
                peer_ip=devices.inventory.hosts[host][MLAG_KEY_FOR_NORNIR_HOSTS_BRIEF][CLAG_PEER_IP_KEY_YAML],
                sys_mac=devices.inventory.hosts[host][MLAG_KEY_FOR_NORNIR_HOSTS_BRIEF][CLAG_SYS_MAC_KEY_YAML])

            if local_file_mlag != remote_running_mlag:
                print(host)
                print(local_file_mlag)
                print(remote_running_mlag)
                return False
    
    return True
