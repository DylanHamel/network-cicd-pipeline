#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-


"""
Description ...

"""

__author__ = "Dylan Hamel"
__maintainer__ = "Dylan Hamel"
__version__ = "1.0"
__email__ = "dylan.hamel@protonmail.com"
__status__ = "Prototype"
__copyright__ = "Copyright 2019"

######################################################
#
# Default value used for exit()
#
EXIT_SUCCESS = 0
EXIT_FAILURE = 1

######################################################
#
# Import Library
#
try:
    from nornir.plugins.tasks.text import template_file
    from nornir.plugins.tasks.commands import remote_command
    from nornir.core.exceptions import NornirSubTaskError
    from nornir.plugins.functions.text import print_result
except ImportError as importError:
    print("Error import [pings.py] nornir")
    print(importError)
    exit(EXIT_FAILURE)

######################################################
#
# Constantes
#
PATH_JINJA2_TEMPLATE = "./tests/templates/jinja2/"
PATH_RESULT_TEMPLATE = "./tests/templates/files/"
RESULT_EXTENSIONS = "_ping.cfg"
PING_TEMPLATE = "ping.j2"
PINGS_KEY_FOR_NORNIR_HOSTS = "pings"

YAML_FILE_KEY = "pings"
YAML_ALL_KEY = "all"
YAML_GROUPS_KEY = "groups"

# ====================================================
# PING PART
# ====================================================

# ----------------------------------------------------
#
#
def _hosts_to_ping(file: dict(), groups: list(), hostname: str()) -> list():

    lst_hosts = list()

    if YAML_GROUPS_KEY in file[YAML_FILE_KEY].keys():
        for file_groups in file[YAML_FILE_KEY][YAML_GROUPS_KEY].keys():
            for group in groups:
                if "," in file_groups:
                    if group in file_groups.split(","):
                        for host in file[YAML_FILE_KEY][YAML_GROUPS_KEY][file_groups]:
                            lst_hosts.append(host)
                else:
                    if group in file[YAML_FILE_KEY][YAML_GROUPS_KEY].keys():
                        for host in file[YAML_FILE_KEY][YAML_GROUPS_KEY][group]:
                            lst_hosts.append(host)
    
    for group in groups:
        if group in file[YAML_FILE_KEY][YAML_GROUPS_KEY].keys():
            for host in file[YAML_FILE_KEY][YAML_GROUPS_KEY][group]:
                lst_hosts.append(host)

    if YAML_ALL_KEY in file[PINGS_KEY_FOR_NORNIR_HOSTS].keys():
        for host in file[PINGS_KEY_FOR_NORNIR_HOSTS][YAML_ALL_KEY]:
            lst_hosts.append(host)

    if str(hostname) in file[PINGS_KEY_FOR_NORNIR_HOSTS].keys():
        for host in file[PINGS_KEY_FOR_NORNIR_HOSTS][str(hostname)]:
            lst_hosts.append(host)

    return lst_hosts

# ----------------------------------------------------
#
#
def _ping_network_devices(task):

    file = open(
        f"{PATH_RESULT_TEMPLATE}{task.host.name}{RESULT_EXTENSIONS}", "r")

    for ping_line in file:
        try:
            output = task.run(
                name=f"Ping network devices",
                task=remote_command,
                command=ping_line
            )
            #print_result(output)

        except NornirSubTaskError as identifier:
            print(f"[PINGS] ERROR WITH {task.host} _> {ping_line}")
            print(f"RESULT = {output.failed}")

        except Exception as e:
            print(e)

# ----------------------------------------------------
#
#
def _generate_ping_command(task, file):

    task.host[PINGS_KEY_FOR_NORNIR_HOSTS] = _hosts_to_ping(
        file, list(task.host.groups), str(task.host))

    output = task.run(
        name=f"Generate templates from {PATH_JINJA2_TEMPLATE}",
        task=template_file,
        template=f"{PING_TEMPLATE}",
        path=f"{PATH_JINJA2_TEMPLATE}"
    )

    file = open(f"{PATH_RESULT_TEMPLATE}{task.host.name}{RESULT_EXTENSIONS}", "w+")
    file.write(output.result)
    file.close()
