#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-


"""
Description ...

"""

__author__ = "Dylan Hamel"
__maintainer__ = "Dylan Hamel"
__version__ = "1.0"
__email__ = "dylan.hamel@protonmail.com"
__status__ = "Prototype"
__copyright__ = "Copyright 2019"

######################################################
#
# Default value used for exit()
#
EXIT_SUCCESS = 0
EXIT_FAILURE = 1

######################################################
#
# Import Library
#
try:
    import pprint
    PP = pprint.PrettyPrinter(indent=4)
except ImportError as importError:
    print("{EXCEPTIONS_HEADER} pprint")
    print(importError)
    exit(EXIT_FAILURE)

try:
    from netests.networkCheck.bgpCheck import BGPSession
    from netests.networkCheck.bgpCheck import ListBGPSessions
except ImportError as importError:
    print(importError.msg)
    print("Error import [netests] BGPSession/ListBGPSessions")
    print(importError)
    exit(EXIT_FAILURE)

try:
    from nornir.plugins.tasks.networking import netmiko_send_command
    from nornir.core import Nornir
except ImportError as importError:
    print("Error import [bgp.py] nornir")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import json
except ImportError as importError:
    print("Error import [bgp.py] json")
    print(importError)
    exit(EXIT_FAILURE)


######################################################
#
# Constantes
#
SPINE_ANSIBLE_GROUP_NAME = "spine"

EVPN_YAML_KEY = "bgp_evpn"
BGP_YAML_KEY = "bgp_sessions"

EVPN_SUM_KEY_FOR_NORNIR_HOSTS = "evpn_sum"
EVPN_VNI_KEY_FOR_NORNIR_HOSTS = "evpn_vni"
EVPN_KEY_FOR_NORNIR_HOSTS_BRIEF = "evpn_brief"

EVPN_VNI_DEFAULT_KEY_JSON = ["advertiseGatewayMacip",
                             "advertiseAllVnis",
                             "flooding",
                             "numVnis",
                             "numL2Vnis",
                             "numL3Vnis"
]

EVPN_KEY_FOR_ASN = "asn"

CUMULUS_COMMAND_SUMMARY = "net show bgp  evpn summary json"
CUMULUS_COMMAND_VNI = "net show bgp evpn vni json"

# ====================================================
# EVPN PART
# ====================================================


# ----------------------------------------------------
#
#
def _cumulus_get_vpn_summary(task):

    output = task.run(
        name=CUMULUS_COMMAND_SUMMARY,
        task=netmiko_send_command,
        command_string=CUMULUS_COMMAND_SUMMARY
    )

    task.host[EVPN_SUM_KEY_FOR_NORNIR_HOSTS] = output.result

    output = task.run(
        name=CUMULUS_COMMAND_VNI,
        task=netmiko_send_command,
        command_string=CUMULUS_COMMAND_VNI
    )

    task.host[EVPN_VNI_KEY_FOR_NORNIR_HOSTS] = output.result


# ----------------------------------------------------
#
#
def _cumulus_evpn_converter(nornirObj):

    for host in nornirObj.inventory.hosts:
        resultBGP = dict()

        if EVPN_SUM_KEY_FOR_NORNIR_HOSTS in nornirObj.inventory.hosts[host].keys() and \
                EVPN_VNI_KEY_FOR_NORNIR_HOSTS in nornirObj.inventory.hosts[host].keys():
            data_sum = json.loads(
                nornirObj.inventory.hosts[host][EVPN_SUM_KEY_FOR_NORNIR_HOSTS])
            data_vni = json.loads(
                nornirObj.inventory.hosts[host][EVPN_VNI_KEY_FOR_NORNIR_HOSTS])
        else:
            raise Exception(
                "[_cumulus_evpn_converter] - error with EVPN key => check inventory hosts file")

        if "routerId" in data_sum.keys():
        
            resultBGP[host] = dict()
            resultBGP[host][EVPN_KEY_FOR_ASN] = data_sum['as']
            resultBGP[host]['routerId'] = data_sum['routerId']
            resultBGP[host]['vrfName'] = data_sum['vrfName']

            resultBGP[host]['interfaces'] = dict()
            for port, facts in data_sum['peers'].items():
                resultBGP[host]['interfaces'][port] = dict()
                resultBGP[host]['interfaces'][port]['state'] = facts['state']
                if 'hostname' in facts.keys():
                    resultBGP[host]['interfaces'][port]['bgp_neighbor'] = facts['hostname']
                else:
                    resultBGP[host]['interfaces'][port]['bgp_neighbor'] = "None"

        if SPINE_ANSIBLE_GROUP_NAME not in nornirObj.inventory.hosts[host].groups:
            if "advertiseGatewayMacip" in data_vni.keys():

                list_remote_evpn_vnis = list()

                for vnis, facts in data_vni.items():
                    if EVPN_KEY_FOR_ASN not in resultBGP[host].keys():
                        resultBGP[host] = dict()

                    if vnis not in EVPN_VNI_DEFAULT_KEY_JSON:
                        list_remote_evpn_vnis.append(
                                                {
                                                "vni": facts["vni"],
                                                "type": facts["type"]
                                                }
                        )
                
                resultBGP[host]['evpn_vnis'] = list(list_remote_evpn_vnis)

        nornirObj.inventory.hosts[host][EVPN_KEY_FOR_NORNIR_HOSTS_BRIEF] = resultBGP[host]


# ----------------------------------------------------
#
#
def compare_bgp_evpn(topology: dict(), devices: Nornir) -> bool:

    for host in devices.inventory.hosts:
        if host in topology[BGP_YAML_KEY]:

            local_file_evpn = list()
            remote_running_evpn = list()

            for evpn_sessions in topology[BGP_YAML_KEY][host]['neighbors']:
                local_file_evpn.append(
                    BGPSession(src_hostname=host,
                               dst_hostname=evpn_sessions['neighbor'],
                               src_interface=evpn_sessions['interface']
                               )
                )

            for port, facts in devices.inventory.hosts[host][EVPN_KEY_FOR_NORNIR_HOSTS_BRIEF]['interfaces'].items():
                remote_running_evpn.append(
                    BGPSession(src_hostname=host,
                               dst_hostname=facts['bgp_neighbor'],
                               src_interface=port
                               )
                )

            remote = ListBGPSessions(remote_running_evpn)
            local = ListBGPSessions(local_file_evpn)

            if remote != local:
                print(
                    f"[compare_bgp_evpn] The problem is on {host}!!")
                return False

    return True


# ----------------------------------------------------
#
#
def compare_evpn(topology: dict(), devices: Nornir, *, neighbors=False) -> bool:

    list_bgp_not_defined = list()
    list_evpn_not_error = list()

    for host in devices.inventory.hosts:
        if host in topology[EVPN_YAML_KEY].keys():
            
            if neighbors:

                local_file_bgp = list()
                remote_running_bgp = list()
                
                
                if "neighbors" in topology[EVPN_YAML_KEY][host].keys():
                    for bgp_session in topology[EVPN_YAML_KEY][host]['neighbors']:
                        local_file_bgp.append(
                            BGPSession(src_hostname=host,
                                    dst_hostname=bgp_session['neighbor'],
                                    src_interface=bgp_session['interface']
                                    )
                        )
                    
                    for port, facts in devices.inventory.hosts[host][EVPN_KEY_FOR_NORNIR_HOSTS_BRIEF]['interfaces'].items():
                        remote_running_bgp.append(
                            BGPSession(src_hostname=host,
                                    dst_hostname=facts['bgp_neighbor'],
                                    src_interface=port
                                    )
                        )

                    remote = ListBGPSessions(remote_running_bgp)
                    local = ListBGPSessions(local_file_bgp)

                    if remote != local:
                        list_bgp_not_defined.append(host)

                else:
                    list_bgp_not_defined.append(host)

            if "vnis" in topology[EVPN_YAML_KEY][host].keys():

                    local_file_evpn_vni= list()
                    remote_running_evpn_vni = list()

                    for evpn_sessions in topology[EVPN_YAML_KEY][host]['vnis']:
                        local_file_evpn_vni.append(evpn_sessions)
                    
                    for evpn_sessions in devices.inventory.hosts[host][EVPN_KEY_FOR_NORNIR_HOSTS_BRIEF]['evpn_vnis']:
                        remote_running_evpn_vni.append(evpn_sessions['vni'])


                    if sorted(local_file_evpn_vni) != sorted(remote_running_evpn_vni):
                        list_evpn_not_error.append(host)
                
    
    return_value = True

    if len(list_bgp_not_defined) > 0:
        print(
            f"[compare_evpn] MP-BGP EVPN neighbors are not defined for {list_bgp_not_defined}!!")
        return_value = False

    if len(list_evpn_not_error) > 0:
        print(
            f"[compare_bgp_evpn] Mp-BGP EVPN VNIS are not correct on {list_evpn_not_error}!!")
        return_value = False


    return True
