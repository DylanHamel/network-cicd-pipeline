#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-


"""
Description ...

"""

__author__ = "Dylan Hamel"
__maintainer__ = "Dylan Hamel"
__version__ = "1.0"
__email__ = "dylan.hamel@protonmail.com"
__status__ = "Prototype"
__copyright__ = "Copyright 2019"

######################################################
#
# Default value used for exit()
#
EXIT_SUCCESS = 0
EXIT_FAILURE = 1

######################################################
#
# Import Library
#
try:
    import pprint
    PP = pprint.PrettyPrinter(indent=4)
except ImportError as importError:
    print("{EXCEPTIONS_HEADER} pprint")
    print(importError)
    exit(EXIT_FAILURE)

try:
    from nornir.plugins.tasks.networking import netmiko_send_command
    from nornir.core import Nornir
    from nornir.plugins.functions.text import print_result
except ImportError as importError:
    print("Error import [bgp.py] nornir")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import json
except ImportError as importError:
    print("Error import [bgp.py] json")
    print(importError)
    exit(EXIT_FAILURE)


######################################################
#
# Constantes
#
L2VNI_YAML_KEY = "vteps"

VTEP_KEY_FOR_NORNIR_HOSTS = "vtep_all"
VTEP_KEY_FOR_NORNIR_HOSTS_BRIEF = "vtep_brief"

CUMULUS_COMMAND_VTEP = "net show evpn vni detail json"

# ====================================================
# VTEP PART
# ====================================================

def _cumulus_get_l2vni(task):

    if VTEP_KEY_FOR_NORNIR_HOSTS not in task.host.keys():
        output = task.run(
            name=f"{CUMULUS_COMMAND_VTEP}",
            task=netmiko_send_command,
            command_string=CUMULUS_COMMAND_VTEP
        )

        task.host[VTEP_KEY_FOR_NORNIR_HOSTS] = output.result


def _cumulus_vtep_converter(nornirObj):

    for host in nornirObj.inventory.hosts:
        if VTEP_KEY_FOR_NORNIR_HOSTS in nornirObj.inventory.hosts[host].keys():
            data = json.loads(
                nornirObj.inventory.hosts[host][VTEP_KEY_FOR_NORNIR_HOSTS])
        else:
            raise Exception(
                f"[_cumulus_vtep_converter] - error with {VTEP_KEY_FOR_NORNIR_HOSTS} key => check inventory hosts file")

        result = result()

        NotImplemented
        # Problem in JSON output
