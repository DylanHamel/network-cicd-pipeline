#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-


"""
Description ...

"""

__author__ = "Dylan Hamel"
__maintainer__ = "Dylan Hamel"
__version__ = "1.0"
__email__ = "dylan.hamel@protonmail.com"
__status__ = "Prototype"
__copyright__ = "Copyright 2019"

######################################################
#
# Default value used for exit()
#
EXIT_SUCCESS = 0
EXIT_FAILURE = 1

######################################################
#
# Import Library
#
try:
    from nornir.plugins.tasks.text import template_file
    from nornir.plugins.tasks.commands import remote_command
    from nornir.core.exceptions import NornirSubTaskError
    from nornir.plugins.functions.text import print_result
except ImportError as importError:
    print("Error import [sockets.py] nornir")
    print(importError)
    exit(EXIT_FAILURE)

######################################################
#
# Constantes
#
PATH_JINJA2_TEMPLATE = "./tests/templates/jinja2/"
PATH_RESULT_TEMPLATE = "./tests/templates/files/"
RESULT_EXTENSIONS = "_socket.cfg"
SOCKET_TEMPLATE = "socket.j2"
SOCKETS_KEY_FOR_NORNIR_HOSTS = "sockets"

YAML_FILE_KEY = "sockets"
YAML_ALL_KEY = "all"
YAML_GROUPS_KEY = "groups"

# ====================================================
# PING PART
# ====================================================

# ----------------------------------------------------
#
#
def _hosts_to_sockets(file: dict(), groups: list(), hostname: str()) -> list():
    
    lst_hosts = list()

    if YAML_GROUPS_KEY in file[YAML_FILE_KEY].keys():
        for file_groups in file[YAML_FILE_KEY][YAML_GROUPS_KEY].keys():
            for group in groups:
                if "," in file_groups:
                    if group in file_groups.split(","):
                        for host in file[YAML_FILE_KEY][YAML_GROUPS_KEY][file_groups]:
                            lst_hosts.append(host)
                else:
                    if group in file[YAML_FILE_KEY][YAML_GROUPS_KEY].keys():
                        for host in file[YAML_FILE_KEY][YAML_GROUPS_KEY][group]:
                            lst_hosts.append(host)

    if YAML_GROUPS_KEY in file[YAML_FILE_KEY].keys():
        for group in groups:
            if group in file[YAML_FILE_KEY][YAML_GROUPS_KEY].keys():
                for host in file[YAML_FILE_KEY][YAML_GROUPS_KEY][group]:
                    lst_hosts.append(host)

    if YAML_ALL_KEY in file[SOCKETS_KEY_FOR_NORNIR_HOSTS].keys():
        for host in file[SOCKETS_KEY_FOR_NORNIR_HOSTS][YAML_ALL_KEY]:
            lst_hosts.append(host)

    if str(hostname) in file[SOCKETS_KEY_FOR_NORNIR_HOSTS].keys():
        for host in file[SOCKETS_KEY_FOR_NORNIR_HOSTS][str(hostname)]:
            lst_hosts.append(host)

    return lst_hosts


# ----------------------------------------------------
#
#
def _socket_network_devices(task):

    file = open(
        f"{PATH_RESULT_TEMPLATE}{task.host.name}{RESULT_EXTENSIONS}", "r")

    for socket_cmd in file:
        try:
            output = task.run(
                name=f"Sockets network devices",
                task=remote_command,
                command=socket_cmd
            )
            #print_result(output)

        except NornirSubTaskError as identifier:
            print(f"[SOCKETS] ERROR WITH {task.host} _> {socket_cmd}")
            print(f"RESULT = {output.failed}")

        except Exception as e:
            print(e)


# ----------------------------------------------------
#
#
def _generate_socket_command(task, file):
    
    task.host[SOCKETS_KEY_FOR_NORNIR_HOSTS] = _hosts_to_sockets(
        file, list(task.host.groups), str(task.host))

    output = task.run(
        name=f"Generate templates from {PATH_JINJA2_TEMPLATE}",
        task=template_file,
        template=f"{SOCKET_TEMPLATE}",
        path=f"{PATH_JINJA2_TEMPLATE}"
    )

    file = open(
        f"{PATH_RESULT_TEMPLATE}{task.host.name}{RESULT_EXTENSIONS}", "w+")
    file.write(output.result)
    file.close()
