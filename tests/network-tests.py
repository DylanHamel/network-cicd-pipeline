#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-


"""
Description ...

"""

__author__ = "Dylan Hamel"
__maintainer__ = "Dylan Hamel"
__version__ = "1.0"
__email__ = "dylan.hamel@protonmail.com"
__status__ = "Prototype"
__copyright__ = "Copyright 2019"

######################################################
#
# Default value used for exit()
#
EXIT_SUCCESS = 0
EXIT_FAILURE = 1

######################################################
#
# Import Library
#
EXCEPTIONS_HEADER = "Error import [network-tests]"
#
# Own libraries
#
try:
    import netests.sockets
except ImportError as importError:
    print(f"{EXCEPTIONS_HEADER} netests.sockets")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import netests.pings
except ImportError as importError:
    print(f"{EXCEPTIONS_HEADER} netests.pings")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import netests.lldp
except ImportError as importError:
    print(f"{EXCEPTIONS_HEADER} netests.lldp")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import netests.bgp
except ImportError as importError:
    print(f"{EXCEPTIONS_HEADER} netests.bgp")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import netests.ipv4
except ImportError as importError:
    print(f"{EXCEPTIONS_HEADER} netests.ipv4")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import netests.vxlan
except ImportError as importError:
    print(f"{EXCEPTIONS_HEADER} netests.vxlan")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import netests.mlag
except ImportError as importError:
    print(f"{EXCEPTIONS_HEADER} netests.mlag")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import netests.vrf
except ImportError as importError:
    print(f"{EXCEPTIONS_HEADER} netests.vrf")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import netests.evpn
except ImportError as importError:
    print(f"{EXCEPTIONS_HEADER} netests.evpn")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import netests.mtu
except ImportError as importError:
    print(f"{EXCEPTIONS_HEADER} netests.mtu")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import netests.l2vni
except ImportError as importError:
    print(f"{EXCEPTIONS_HEADER} netests.l2vni")
    print(importError)
    exit(EXIT_FAILURE)

#
# Data Structure
#
try:
    import json
except ImportError as importError:
    print(f"{EXCEPTIONS_HEADER} json")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import yaml
except ImportError as importError:
    print(f"{EXCEPTIONS_HEADER} yaml")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import textfsm
except ImportError as importError:
    print(f"{EXCEPTIONS_HEADER} textfsm")
    print(importError)
    exit(EXIT_FAILURE)

#
# Network Libraries
#
try:
    import netmiko

except ImportError as importError:
    print("{EXCEPTIONS_HEADER} netmiko")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import pprint
    PP = pprint.PrettyPrinter(indent=4)
except ImportError as importError:
    print("{EXCEPTIONS_HEADER} pprint")
    print(importError)
    exit(EXIT_FAILURE)

try:
    from nornir import InitNornir
    from nornir.core.task import Result
    from nornir.core import Nornir

# To use advanced filters
    from nornir.core.filter import F

# To use HTTP requests
    from nornir.plugins.tasks.apis import http_method

# To execute commands through SSH
    from nornir.plugins.tasks.commands import remote_command

# Netmiko commands
    from nornir.plugins.tasks.networking import netmiko_send_command
    from nornir.plugins.tasks.networking import netmiko_save_config
    from nornir.plugins.tasks.networking import netmiko_send_config

# To use PING
    from nornir.plugins.tasks.networking import tcp_ping

# To print task results
    from nornir.plugins.functions.text import print_result

# To retrieve device datas in a YAML file
    from nornir.plugins.tasks.data import load_yaml

# To generate template from Jinja2
    from nornir.plugins.tasks.text import template_file

# To raise Nornir exceptions
    from nornir.core.exceptions import CommandError
    from nornir.core.exceptions import NornirExecutionError
    from nornir.core.exceptions import NornirSubTaskError

except ImportError as importError:
    print(f"{EXCEPTIONS_HEADER} nornir")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import click
except ImportError as importError:
    print(f"{EXCEPTIONS_HEADER}  click")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import os
except ImportError as importError:
    print(f"{EXCEPTIONS_HEADER} os")
    print(importError)
    exit(EXIT_FAILURE)
######################################################
#
# Constantes
#
EXCEPTIONS_HEADER = "Error import [network-tests]"
PRINT_HEADER = "[network-tests.py - main]"
TEST_TO_EXECUTE_YAML_FILE = "_test_to_execute.yml"
VLAN_VXLAN_TO_CHECK = "vlan.yml"
L2VNI_TO_CHECK = "l2vni.yml"
VTEP_TO_CHECK = "vtep.yml"
MLAG_TO_CHECK = "mlag.yml"
EVPN_TO_CHECK = "evpn.yml"
VRF_VNI_TO_CHECK = "vrf_vni.yml"
IPV4_ADDRESS_TO_CHECK = "ipv4.yml"
LLDP_LINKS_TO_CHECK = "lldp.yml"
PING_HOST_TO_CHECK = "ping.yml"
SOCKET_HOST_TO_CHECK = "socket.yml"
BGP_SESSIONS_TO_CHECK = "bgp.yml"
MTU_TO_CHECK = "mtu.yml"
NORNIR_CONFIG_FILE_VIRTUAL = "./tests/nornir/config_virtual.yml"
NORNIR_CONFIG_FILE_PROD = "./tests/nornir/config_prod.yml"
NORNIR_CONFIG_FILE_PATH = "./tests/nornir/"
NORNIR_LOGS_FILE = "./tests/nornir/nornir.log"
NORNIR_DEBUG_MODE = "debug"
TEXTFSM_TEMPLATE_PATH = "./tests/templates/textfsm/"
TO_EXECUTE_FILE_VALUE = ['INFO', 'TRUE', 'FALSE']
######################################################
#
# Variables
#

######################################################
#
# Functions
#
def check_ping_network_devices(nr: Nornir):
    """
    Check that each devices can reach otherss
    """

    devices = nr.filter(
        F(groups__contains="leaf") | \
        F(groups__contains="spine") | \
        F(groups__contains="exit")
    )  

    if len(devices.inventory.hosts) == 0:
        raise Exception(f"[check_ping_network_devices] no device selected.")

    file = open_file(os.environ['tests_path']+PING_HOST_TO_CHECK)

    data = devices.run(
        task=netests.pings._generate_ping_command,
        file=file,
        num_workers=10
    )
    #print_result(data)

    if data.failed is True:
        return False

    data = devices.run(
        task=netests.pings._ping_network_devices,
        num_workers=10
    )
    #print_result(data)

    return (not data.failed)



def check_cumulus_bgp_established(nr: Nornir):

    devices = nr.filter(
        F(groups__contains="leaf") |
        F(groups__contains="spine") |
        F(groups__contains="exit")
    )

    if len(devices.inventory.hosts) == 0:
        raise Exception(f"[check_cumulus_lldp] no device selected.")

    path_url = os.environ['tests_path']+BGP_SESSIONS_TO_CHECK

    data = devices.run(
        task=netests.bgp._cumulus_get_bgp_summary,
        on_failed=True,
        num_workers=10
    )
    #print_result(data)
    if data.failed is True:
        return False

    netests.bgp._cumulus_bgp_summary_converter(devices)

    content = open_file(path_url)
    content = (content)

    result = dict()

    return (netests.bgp.check_all_bgp_sessions_established(devices))



def check_cumulus_bgp(nr: Nornir):

    devices = nr.filter(
        F(groups__contains="leaf") |
        F(groups__contains="spine") |
        F(groups__contains="exit")
    )

    if len(devices.inventory.hosts) == 0:
        raise Exception(f"[check_cumulus_lldp] no device selected.")

    path_url = os.environ['tests_path']+BGP_SESSIONS_TO_CHECK

    data = devices.run(
        task=netests.bgp._cumulus_get_bgp_summary,
        on_failed=True,
        num_workers=10
    )
    #print_result(data)
    if data.failed is True:
        return False

    netests.bgp._cumulus_bgp_summary_converter(devices)
    content = open_file(path_url)
    content = (content)

    return (netests.bgp.compare_topology_and_bgp_output(content, devices))



def check_cumulus_lldp(nr: Nornir):

    devices = nr.filter(
        F(groups__contains="leaf") |
        F(groups__contains="spine") |
        F(groups__contains="exit")
    )

    if len(devices.inventory.hosts) == 0:
        raise Exception(f"[check_cumulus_lldp] no device selected.")

    path_url = os.environ['tests_path']+LLDP_LINKS_TO_CHECK

    data = devices.run(
        task=netests.lldp._cumulus_get_lldp_neighbors,
        on_failed=True,
        num_workers=10
    )
    #print_result(data)
    if data.failed is True:
        return False

    netests.lldp._cumulus_lldp_converter(devices)
    content = open_file(path_url)
    content = netests.lldp.retrieve_topology_from_architecture(content)

    return (netests.lldp.compare_topology_and_lldp_output(content, devices))



def check_cumulus_ipv4_addresses(nr: Nornir):

    devices = nr.filter(
        F(groups__contains="leaf") |
        F(groups__contains="spine") |
        F(groups__contains="exit")
    )

    if len(devices.inventory.hosts) == 0:
        raise Exception(f"[check_cumulus_ipv4_addresses] no device selected.")

    path_url = os.environ['tests_path']+IPV4_ADDRESS_TO_CHECK

    data = devices.run(
        task=netests.ipv4._cumulus_get_interfaces_infos,
        on_failed=True,
        num_workers=10
    )
    #print_result(data)
    if data.failed is True:
        return False

    netests.ipv4._cumulus_interfaces_infos_converter(devices)
    content = open_file(path_url)

    return (netests.ipv4.compare_ipv4_adresses(content, devices))



def check_cumulus_vlan_vxlan(nr: Nornir):

    devices = nr.filter(
        F(groups__contains="leaf") |
        F(groups__contains="spine") |
        F(groups__contains="exit")
    )

    if len(devices.inventory.hosts) == 0:
        raise Exception(f"[check_cumulus_vlan_vxlan] no device selected.")

    path_url = os.environ['tests_path']+VLAN_VXLAN_TO_CHECK

    data = devices.run(
        task=netests.vxlan._cumulus_get_interfaces_infos,
        on_failed=True,
        num_workers=10
    )
    #print_result(data)
    if data.failed is True:
        return False

    netests.vxlan._cumulus_vlan_vxlan_infos_converter(devices)
    content = open_file(path_url)

    return (netests.vxlan.compare_vxlan_vlan(content, devices))


def check_cumulus_mlag(nr: Nornir):

    # No MLAG ON SPINE !!!
    devices = nr.filter(
        F(groups__contains="leaf") |
        F(groups__contains="exit")
    )

    if len(devices.inventory.hosts) == 0:
        raise Exception(f"[check_cumulus_mlag] no device selected.")

    path_url = os.environ['tests_path']+MLAG_TO_CHECK

    data = devices.run(
        task=netests.mlag._cumulus_get_mlag_infos,
        on_failed=True,
        num_workers=10
    )
    #print_result(data)
    if data.failed is True:
        return False

    netests.mlag._cumulus_mlag_infos_converter(devices)
    content = open_file(path_url)

    return (netests.mlag.compare_mlag(content, devices))


def check_cumulus_vrf_vni(nr: Nornir):

    # No VRF ON SPINE - only routing !!!
    devices = nr.filter(
        F(groups__contains="leaf") |
        F(groups__contains="exit")
    )

    if len(devices.inventory.hosts) == 0:
        raise Exception(f"[check_cumulus_vrf_vni] no device selected.")

    path_url = os.environ['tests_path']+VRF_VNI_TO_CHECK

    data = devices.run(
        task=netests.vrf._cumulus_get_vrf_vni_infos,
        on_failed=True,
        num_workers=10
    )
    #print_result(data)
    if data.failed is True:
        return False

    netests.vrf._cumulus_vrf_vni_infos_converter(devices)
    content = open_file(path_url)

    return (netests.vrf.compare_vrf_vni(content, devices))


def check_cumulus_evpn(nr: Nornir):

    devices = nr.filter(
        F(groups__contains="leaf") |
        F(groups__contains="spine") |
        F(groups__contains="exit")
    )

    if len(devices.inventory.hosts) == 0:
        raise Exception(f"[check_cumulus_evpn] no device selected.")

    path_url = os.environ['tests_path']+EVPN_TO_CHECK

    data = devices.run(
        task=netests.evpn._cumulus_get_vpn_summary,
        on_failed=True,
        num_workers=10
    )
    #print_result(data)
    if data.failed is True:
        return False

    netests.evpn._cumulus_evpn_converter(devices)
    content = open_file(path_url)

    if "path_bgp" in content['bgp_evpn'].keys():
        bgp_sessions = open_file(content['bgp_evpn']['path_bgp'])
        bgp_side = netests.evpn.compare_bgp_evpn(bgp_sessions, devices)
        neighbors = False
    else:
        bgp_side = True
        neighbors = True

    evpn_side = netests.evpn.compare_evpn(content, devices, neighbors=neighbors)

    return bgp_side and evpn_side


def check_cumulus_interfaces_mtu(nr: Nornir):

    devices = nr.filter(
        F(groups__contains="leaf") |
        F(groups__contains="spine") |
        F(groups__contains="exit")
    )

    path_url = os.environ['tests_path']+MTU_TO_CHECK
    content = open_file(path_url)

    if len(devices.inventory.hosts) == 0:
        raise Exception(f"[check_cumulus_interfaces_mtu] no device selected.")

    
    data = devices.run(
        task=netests.mtu._cumulus_get_interfaces_mtu,
        on_failed=True,
        num_workers=10
    )
    #print_result(data)
    if data.failed is True:
        return False

    netests.mtu._cumulus_interfaces_mtu_converter(devices)

    return (netests.mtu.compare_interfaces_mtu(content, devices))


def check_cumulus_l2vni(nr: Nornir):

    devices = nr.filter(
        F(groups__contains="leaf") |
        F(groups__contains="exit")
    )

    path_url = os.environ['tests_path']+L2VNI_TO_CHECK
    content = open_file(path_url)

    if len(devices.inventory.hosts) == 0:
        raise Exception(f"[check_cumulus_l2vni] no device selected.")

    data = devices.run(
        task=netests.l2vni._cumulus_get_l2vni,
        on_failed=True,
        num_workers=10
    )
    #print_result(data)
    if data.failed is True:
        return False

    netests.l2vni._cumulus_l2vni_converter(devices)

    return (netests.l2vni.compare_l2vni(content, devices))


def check_sockets(nr: Nornir):

    devices = nr.filter(
        F(groups__contains="leaf") |
        F(groups__contains="spine") |
        F(groups__contains="exit")
    )

    if len(devices.inventory.hosts) == 0:
        raise Exception(f"[check_sockets] no device selected.")

    file = open_file(os.environ['tests_path']+SOCKET_HOST_TO_CHECK)

    data = devices.run(
        task=netests.sockets._generate_socket_command,
        file=file,
        num_workers=10
    )
    #print_result(data)

    if data.failed is True:
        return False

    data = devices.run(
        task=netests.sockets._socket_network_devices,
        num_workers=10
    )
    #print_result(data)

    return (not data.failed)


######################################################
#
# MAIN Functions
#
@click.command()
@click.option('--production', default="#", help='Specify if Nornir will use config_prod.yml or not.')
@click.option('--tests-changed', default="#", help='Specify if Nornir will use config_prod.yml or not.')
def main(production, tests_changed):

    if production == "#":
        config_file = NORNIR_CONFIG_FILE_VIRTUAL
    else:
        config_file = NORNIR_CONFIG_FILE_PROD
        
    if tests_changed == "#":
        os.environ['tests_path'] = "./tests/topology/"
    else:
        os.environ['tests_path'] = "./tests/topology/backup/"

    print(f"Nornir config file uses {config_file}")
    print(f"Path to YAML file use for tests {os.environ['tests_path']}")

    nr = InitNornir(
        config_file=config_file,
        logging={"file": NORNIR_LOGS_FILE, "level": NORNIR_DEBUG_MODE}
    )

    test_to_execute = open_file(os.environ['tests_path']+TEST_TO_EXECUTE_YAML_FILE)
    PP.pprint(test_to_execute)
    if check_test_to_execute_file(test_to_execute) is False:
        print(f"{PRINT_HEADER} Please check your {os.environ['tests_path']+TEST_TO_EXECUTE_YAML_FILE} file")
        exit(EXIT_FAILURE)
    
    
    exit_value = True

    # ''''''''''''''''''''''''''''''''''''''''''''
    # 1. Check all BGP sessions established
    # ''''''''''''''''''''''''''''''''''''''''''''
    if test_to_execute['bgp_up'] is not False:
        all_bgp_session_established = check_cumulus_bgp_established(nr)
        print(f"{PRINT_HEADER} All BGP sessions are established => {all_bgp_session_established}")
        if test_to_execute['bgp_up'] is True and all_bgp_session_established is False:
            exit_value = False
    else:
        print(f"{PRINT_HEADER} BGP_ETABLISHED tests are not executed !!")
    
    # ''''''''''''''''''''''''''''''''''''''''''''
    # 2. Check BGP sessions 
    # ''''''''''''''''''''''''''''''''''''''''''''
    if test_to_execute['bgp'] is not False:
        bgp_session_up = check_cumulus_bgp(nr)
        print(f"{PRINT_HEADER} All BGP sessions are UP regarding YAML file ({BGP_SESSIONS_TO_CHECK}) => {bgp_session_up}")
        if test_to_execute['bgp'] is True and all_bgp_session_established is False:
            exit_value = False
    else:
        print(f"{PRINT_HEADER} BGP_SESSIONS tests are not executed !!")
    
    # ''''''''''''''''''''''''''''''''''''''''''''
    # 3. Check LLDP neighbors
    # ''''''''''''''''''''''''''''''''''''''''''''
    if test_to_execute['lldp'] is not False:
        if production == "#":
            lldp_neighbors = check_cumulus_lldp(nr)
            print(f"{PRINT_HEADER} All LLDP neighbors are UP regarding YAML file ({LLDP_LINKS_TO_CHECK}) {lldp_neighbors}")
        if test_to_execute['lldp'] is True and lldp_neighbors is False:
            exit_value = False
    else:
        print(f"{PRINT_HEADER} LLDP tests are not executed !!")
    
    # ''''''''''''''''''''''''''''''''''''''''''''
    # 4. Check interfaces IP adress
    # ''''''''''''''''''''''''''''''''''''''''''''
    if test_to_execute['ipv4'] is not False:
        ipv4_adresses = check_cumulus_ipv4_addresses(nr)
        print(f"{PRINT_HEADER} All interfaces IPv4 addresses are correct regarding YAML file ({IPV4_ADDRESS_TO_CHECK})  {ipv4_adresses}")
        if test_to_execute['ipv4'] is True and ipv4_adresses is False:
            exit_value = False
    else:
        print(f"{PRINT_HEADER} IPv4 tests are not executed !!")
       
    # ''''''''''''''''''''''''''''''''''''''''''''
    # 5. Check PING
    # ''''''''''''''''''''''''''''''''''''''''''''
    if test_to_execute['ping'] is not False:
        ping_works = check_ping_network_devices(nr)
        print(f"{PRINT_HEADER} All PINGS work regarding YAML file ({PING_HOST_TO_CHECK}) {ping_works}")
        if test_to_execute['ping'] is True and ping_works is False:
            exit_value = False
    else:
        print(f"{PRINT_HEADER} PING tests are not executed !!")

    # ''''''''''''''''''''''''''''''''''''''''''''
    # 6. Check if VLAN / VXLAN exist
    # ''''''''''''''''''''''''''''''''''''''''''''
    if test_to_execute['vlan'] is not False:
        vlan_vxlan_exist = check_cumulus_vlan_vxlan(nr)
        print(f"{PRINT_HEADER} All VLAN and VXLAN are present regarding YAML file ({VLAN_VXLAN_TO_CHECK}) {vlan_vxlan_exist}")
        if test_to_execute['vlan'] is True and vlan_vxlan_exist is False:
            exit_value = False
    else:
        print(f"{PRINT_HEADER} VLAN tests are not executed !!")
    
    # ''''''''''''''''''''''''''''''''''''''''''''
    # 7. Check MLAG
    # ''''''''''''''''''''''''''''''''''''''''''''
    if test_to_execute['mlag'] is not False:
        mlag_works = check_cumulus_mlag(nr)
        print(f"{PRINT_HEADER} All MLAG are UP regarding YAML file ({MLAG_TO_CHECK}) {mlag_works}")
        if test_to_execute['mlag'] is True and mlag_works is False:
            exit_value = False
    else:
        print(f"{PRINT_HEADER} MLAG tests are not executed !!")
    
    # ''''''''''''''''''''''''''''''''''''''''''''
    # 8. Check VRF - VNI 
    # ''''''''''''''''''''''''''''''''''''''''''''
    if test_to_execute['vrf_vni'] is not False:
        vrf_vni_works = check_cumulus_vrf_vni(nr)
        print(f"{PRINT_HEADER} All VRF VNI are present regarding YAML file ({VLAN_VXLAN_TO_CHECK}) {vrf_vni_works}")
        if test_to_execute['vrf_vni'] is True and vrf_vni_works is False:
            exit_value = False
    else:
        print(f"{PRINT_HEADER} VRF_VNI tests are not executed !!")
    
    # ''''''''''''''''''''''''''''''''''''''''''''
    # 9. Check EVPN
    # ''''''''''''''''''''''''''''''''''''''''''''
    if test_to_execute['evpn'] is not False:
        evpn_works = check_cumulus_evpn(nr)
        print(f"{PRINT_HEADER} All EVPN (VNI) sessions are UP regarding YAML file ({EVPN_TO_CHECK}) {evpn_works}")
        if test_to_execute['evpn'] is True and evpn_works is False:
            exit_value = False
    else:
        print(f"{PRINT_HEADER} EVPN tests are not executed !!")

    # ''''''''''''''''''''''''''''''''''''''''''''
    # 10. L2VNI
    # ''''''''''''''''''''''''''''''''''''''''''''
    if test_to_execute['l2vni'] is not False:
        l2vni_works = check_cumulus_l2vni(nr)
        print(f"{PRINT_HEADER} All L2VNI are present regarding YAML file ({L2VNI_TO_CHECK}) {l2vni_works}")
        if test_to_execute['l2vni'] is True and l2vni_works is False:
            exit_value = False
    else:
        print(f"{PRINT_HEADER} L2VNI tests are not executed !!")

    # ''''''''''''''''''''''''''''''''''''''''''''
    # 11. VTEP
    # ''''''''''''''''''''''''''''''''''''''''''''
    if test_to_execute['vtep'] is not False:
        vtep_works = check_cumulus_vtep(nr)
        print(f"{PRINT_HEADER} All VTEP are correct for each devices regarding YAML file ({VTEP_TO_CHECK}) {vtep_works}")
        if test_to_execute['vtep'] is True and vtep_works is False:
            exit_value = False
    else:
        print(f"{PRINT_HEADER} VTEP tests are not executed !!")
    
    # ''''''''''''''''''''''''''''''''''''''''''''
    # 12. MTU
    # ''''''''''''''''''''''''''''''''''''''''''''
    if test_to_execute['mtu'] is not False:
        mtu_works = check_cumulus_interfaces_mtu(nr)
        print(f"{PRINT_HEADER} All swpxx interface have the MTU define in ({MTU_TO_CHECK}) {mtu_works}")
        if test_to_execute['mtu'] is True and mtu_works is False:
            exit_value = False
    else:
        print(f"{PRINT_HEADER} MTU tests are not executed !!")

    # ''''''''''''''''''''''''''''''''''''''''''''
    # 13. SOCKETS
    # ''''''''''''''''''''''''''''''''''''''''''''
    if test_to_execute['socket'] is not False:
        socket_works = check_sockets(nr)
        print(f"{PRINT_HEADER} All SOCKETS work regarding YAML file ({SOCKET_HOST_TO_CHECK}) {socket_works}")
        if test_to_execute['socket'] is True and socket_works is False:
            exit_value = False
    else:
        print(f"{PRINT_HEADER} SOCKET tests are not executed !!")

    if exit_value:
        exit(EXIT_SUCCESS)
    else:
        exit(EXIT_FAILURE)
    
######################################################
#
# Other functions
#
def open_file(path: str()) -> dict():
    """
    This function  will open a YAML file and return is data

    Args:
        param1 (str): Path to the YAML file

    Returns:
        str: Node name
    """

    with open(path, 'r') as yamlFile:
        try:
            data = yaml.load(yamlFile)
        except yaml.yamlError as exc:
            print(exc)
            print("[network-tests - open_file] Error with YAML file - Check if your syntax and your path are correct")
            return None
    return data


def check_test_to_execute_file(data: dict()) -> bool:

    return_value = True

    for key, value in data.items():
        if str(value).upper() not in TO_EXECUTE_FILE_VALUE:
            return_value = False
            print(f"[network-tests - check_test_to_execute_file] Value for {key} is not valid ({str(value).upper()})")
            print(f"[network-tests - check_test_to_execute_file] Value has to be in {TO_EXECUTE_FILE_VALUE}")
        
    return return_value
# -----------------------------------------------------------------------------------------------------------------------------
#
#
if __name__ == "__main__":
    main()  
